<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of project
 *
 * @author rotichmordecai
 */
class Project extends MY_Controller {

    var $data;

    function __construct() {

        parent::__construct();

        $this->load->library(array('template', 'form_validation', 'imaging'));
        $this->load->helper(array('form', 'url', 'galashia'));

        $this->config->load('imaging');

        $this->load->model(array('user_profile_model',
            'user_model',
            'category_model',
            'article_model',
            'analytics_model',
            'comment_model',
            'message_thread_model',
            'message_model',
            'review_model',
            'follow_model',
            'image_model',
            'project_model',
            'project_image_model'
        ));

        if ($this->uri->segment(2) != 'login') {
            $this->view_path = 'project/';
            $this->data = array();
            $this->front_assets();
        }
        $this->data = array();

        $this->config->set_item('crumb_divider', '');
        $this->config->set_item('tag_open', '<ul>');
        $this->config->set_item('tag_close', '</ul>');
        $this->config->set_item('crumb_open', '<li>');
        $this->config->set_item('crumb_close', '</li>');
        $this->config->set_item('crumb_last_open', '<li>');

        $this->breadcrumbs->settings();

        $this->data["categories"] = $categories = $this->category_model->select();
    }

    function index() {


        $this->load->library('pagination');


        $criteria = array(
            'project.user_id' => $this->tank_auth->get_user_id()
        );


        $group_by = array(
            'project.id'
        );

        $config = array();

        $config["base_url"] = site_url('project/' . $this->uri->segment(2));
        $config["total_rows"] = $this->project_model->fetch_count($criteria, $group_by);
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;

        if (count($_GET) > 0)
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->data["results"] = $this->project_model->fetch($config["per_page"], $page, $criteria, $group_by);
        $this->data["pagination"] = $this->pagination->create_links();

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Project', '/project/index');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : Projects';
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function view($project_id) {

        $criteria = array(
            'project.id' => $project_id
        );
        $project = $this->project_model->select($criteria);

        if (isset($project[0])) {

            $this->data['project'] = $project = $project[0];


            $criteria = array(
                'project_image.project_id' => $project_id
            );
            $this->data['project_image'] = $this->project_image_model->select($criteria);

            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Project', '/project/index');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->data['title'] = 'Galashia : View project';
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else {
            redirect('');
        }
    }

    function post() {


        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('start_date', 'start date', 'required|trim|xss_clean');
        $this->form_validation->set_rules('end_date', 'end date', 'required|trim|xss_clean');


        if ($this->form_validation->run() == FALSE) {
            
        } else {

            $this->project_model->set_user_id(set_value('user_id'));
            $this->project_model->set_title(set_value('title'));
            $this->project_model->set_description(set_value('description'));
            $this->project_model->set_start_date(date('Y-m-d'));
            $this->project_model->set_end_date(date('Y-m-d'));
            $this->project_model->set_status(1);

            if ($this->project_model->insert() > 0) {
                $this->message->set('success', 'Success! project Added');
            } else {
                $this->message->set('error', 'Error! project could not be added');
            }
        }

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Post Project', '/project/post');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : Post Project';
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function edit($project_id) {

        if (isset($_FILES["image"])) {

            $this->imaging->upload($_FILES["image"]);
            $this->imaging->file_overwrite = TRUE;

            $image = '';
            $thumb = '';

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->process($this->config->item('project_image_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->imaging->allowed = array('image/*');

                if ($this->imaging->image_src_x > $this->config->item('thumb_size') || $this->imaging->image_src_y > $this->config->item('thumb_size')) {
                    $this->imaging->image_resize = true;
                    $this->imaging->image_x = $this->config->item('thumb_size');
                    $this->imaging->image_ratio_y = true;
                }

                $this->imaging->file_overwrite = TRUE;
                $this->imaging->process($this->config->item('project_thumb_path'));

                if ($this->imaging->processed == true) {
                    $thumb = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->project_image_model->set_project_id($project_id);
                $this->project_image_model->set_image($image);
                $this->project_image_model->set_thumb($thumb);
                $this->project_image_model->set_status(1);

                if ($this->project_image_model->insert() > 0) {
                    $this->message->set('success', 'Success! Image Added');
                } else {
                    $this->message->set('error', 'Error! Image could not be added');
                }

                exit();
            } else {
                $error = $this->imaging->error;
            }
        }

        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('start_date', 'startdate', 'required|trim|xss_clean');
        $this->form_validation->set_rules('end_date', 'end date', 'required|trim|xss_clean');


        if ($this->form_validation->run() == FALSE) {
            
        } else {

            $this->project_model->set_user_id(set_value('user_id'));
            $this->project_model->set_title(set_value('title'));
            $this->project_model->set_description(set_value('description'));
            $this->project_model->set_start_date(date('Y-m-d'));
            $this->project_model->set_end_date(date('Y-m-d'));
            $this->project_model->set_status(1);

            if ($this->project_model->insert() > 0) {
                $this->message->set('success', 'Success! project Added');
            } else {
                $this->message->set('error', 'Error! project could not be added');
            }
        }

        $criteria = array(
            'project.id' => $project_id
        );
        $project = $this->project_model->select($criteria);

        if (isset($project[0])) {

            $this->data['project'] = $project = $project[0];

            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Edit Project', '/project/post');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->data['title'] = 'Galashia : Edit Project';
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        }
    }

}
