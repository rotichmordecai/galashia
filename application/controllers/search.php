<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author rotichmordecai
 */
class Search extends MY_Controller {

    var $data;

    function __construct() {
        parent::__construct();

        $this->load->library(array('template', 'form_validation'));
        $this->load->helper(array('form', 'url', 'galashia'));
        $this->load->model(array('user_model', 'category_model', 'article_model', 'analytics_model', 'comment_model'));

        if ($this->uri->segment(2) != 'login') {
            $this->view_path = 'search/';
            $this->data = array();
            $this->front_assets();
        }
        $this->data = array();

        $this->config->set_item('crumb_divider', '');
        $this->config->set_item('tag_open', '<ul>');
        $this->config->set_item('tag_close', '</ul>');
        $this->config->set_item('crumb_open', '<li>');
        $this->config->set_item('crumb_close', '</li>');
        $this->config->set_item('crumb_last_open', '<li>');

        $this->breadcrumbs->settings();
        
        $this->data["categories"] = $categories = $this->category_model->select();
        
    }

    function index() {


        $search = $this->input->get('search');

        if (empty($search)) {
            redirect('articles/view');
        } else {

            $this->load->library('pagination');

            $category_id = $this->input->get('category_id');

            $this->data['title'] = 'Galashia : Search Articles';
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Search Articles', '/articles');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();

            $config = array();

            $config["base_url"] = site_url('search/index');
            $config["total_rows"] = $this->article_model->search_count($search);
            $config["per_page"] = 5;
            $config["uri_segment"] = 3;

            if (count($_GET) > 0)
                $config['suffix'] = '?' . http_build_query($_GET, '', "&");

            $this->pagination->initialize($config);

            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            $this->data["results"] = $this->article_model->search($search, $config["per_page"], $page);
            $this->data["pagination"] = $this->pagination->create_links();

            $this->data["categories"] = $this->category_model->select();


            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        }
    }

}
