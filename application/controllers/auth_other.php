<?php

class auth_other extends MY_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('tank_auth/users');
        $this->load->helper('url');
        // for google open id
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }

    function facebook_signin() {

        $this->load->library('facebook'); // Automatically picks appId and secret from config
        $fb_user = $this->facebook->get_user();

        if (isset($fb_user)) {
            $this->session->set_userdata('facebook_id', $fb_user['id']);
            $user = $this->user_model->get_user_by_sm(array('facebook_id' => $fb_user['id']), 'facebook_id');

            if (sizeof($user) == 0) {
                redirect('auth_other/fill_user_info', 'refresh');
            } else {
                // simulate what happens in the tank auth
                $this->session->set_userdata(array('user_id' => $user[0]->id, 'username' => $user[0]->username,
                    'status' => ($user[0]->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED));
                $this->tank_auth->clear_login_attempts($user[0]->email);
                $this->users->update_login_info($user[0]->id, $this->config->item('login_record_ip', 'tank_auth'), $this->config->item('login_record_time', 'tank_auth'));
                $redirect = $this->session->userdata('redirect');
                if (strlen($redirect) > 0) {
                    redirect($redirect, 'refresh');
                }
                redirect('auth', 'refresh');
            }
        } else {
            echo 'cannot find the Facebook user';
        }
    }

// function to allow users to log in via twitter
    function twitter_signin() {

        if ($this->input->get('oauth_token') && $this->session->userdata('request_token') !== $this->input->get('oauth_token')) {
            $this->reset_session();
            redirect(base_url('/auth_other/twitter_signin'));
        } else {
            $access_token = $this->connection->getAccessToken($this->input->get('oauth_verifier'));

            if ($this->connection->http_code == 200) {
                $this->session->set_userdata('access_token', $access_token['oauth_token']);
                $this->session->set_userdata('access_token_secret', $access_token['oauth_token_secret']);
                $this->session->set_userdata('twitter_user_id', $access_token['user_id']);
                $this->session->set_userdata('twitter_screen_name', $access_token['screen_name']);
                $this->session->unset_userdata('request_token');
                $this->session->unset_userdata('request_token_secret');
                $criteria = array(
                    'user_profile.twitter_id' => $access_token['user_id']
                );
                $user = $this->user_model->select($criteria);
                if (count($user) != 1) {
                    redirect('auth_other/fill_user_info');
                } else {
                    // simulate what happens in the tank auth
                    $this->session->set_userdata(array('user_id' => $user[0]->id, 'username' => $user[0]->username,
                        'status' => ($user[0]->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED));
                    $this->users->update_login_info($user[0]->id, $this->config->item('login_record_ip', 'tank_auth'), $this->config->item('login_record_time', 'tank_auth'));
                    redirect('', 'refresh');
                }
            } else {

                if ($this->session->userdata('access_token') && $this->session->userdata('access_token_secret')) {
                    // User is already authenticated. Add your user notification code here.
                } else {

                    // Making a request for request_token
                    $request_token = $this->connection->getRequestToken(base_url('/auth_other/twitter_signin'));
                    $this->session->set_userdata('request_token', $request_token['oauth_token']);
                    $this->session->set_userdata('request_token_secret', $request_token['oauth_token_secret']);

                    if ($this->connection->http_code == 200) {
                        $url = $this->connection->getAuthorizeURL($request_token);
                        redirect($url);
                    } else {
                        // An error occured. Make sure to put your error notification code here.
                        redirect(base_url('/'));
                    }
                }
            }
        }
    }

    // handle when users log in using google friend connect
    function gfc_signin() {
        // we passed the user id in the URL, let's get it!
        $gfc_id = $this->uri->segment(3);
        if (!is_null($gfc_id)) {
            $this->session->set_userdata('gfc_id', $gfc_id);
            $user = $this->user_model->get_user_by_sm(array('gfc_id' => $gfc_id), 'gfc_id');
            if (sizeof($user) == 0) {
                redirect('auth_other/fill_user_info', 'refresh');
            } else {
                // simulate what happens in the tank auth
                $this->session->set_userdata(array('user_id' => $user[0]->id, 'username' => $user[0]->username,
                    'status' => ($user[0]->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED));
                $this->users->update_login_info($user[0]->id, $this->config->item('login_record_ip', 'tank_auth'), $this->config->item('login_record_time', 'tank_auth'));
                redirect('auth', 'refresh');
            }
        } else {
            echo 'cannot find the Google Friend Connect ID!';
        }
    }

    // function for logging in via google open id
    function google_openid_signin() {
        //$this->load->library('lightopenid'); // loaded in aotoload.php
        // these are some of the attributes we can get from open id
        // refer to 
        // http://code.google.com/p/lightopenid/wiki/GettingMoreInformation
        // for more
        $required_attr = array('namePerson/friendly', 'contact/email',
            'namePerson/first', 'namePerson/last',
            'contact/country/home', 'contact/email', 'pref/language');
        try {
            if (!isset($_GET['openid_mode'])) {
                $lightopenid = new Lightopenid;
                $lightopenid->identity = 'https://www.google.com/accounts/o8/id';
                $lightopenid->required = $required_attr;
                redirect($lightopenid->authUrl(), 'refresh');
            } elseif ($_GET['openid_mode'] == 'cancel') {
                echo 'User has cancelled authentication!';
            } else {
                $lightopenid = new Lightopenid;
                $lightopenid->required = $required_attr;
                if ($lightopenid->validate()) {
                    #Here goes the code that gets interpreted after successful login!!!
                    //print_r($lightopenid);
                    //echo '<br/>';
                    //print_r($lightopenid->identity);
                    //print_r($lightopenid->getAttributes());


                    $google_open_id = $lightopenid->identity;
                    $this->session->set_userdata('google_open_id', $google_open_id);

                    // does this user exist?
                    $user = $this->user_model->get_user_by_sm(array('google_open_id' => $google_open_id), 'google_open_id');
                    if (sizeof($user) == 0) {
                        redirect('auth_other/fill_user_info', 'refresh');
                    } else {
                        // simulate what happens in the tank auth
                        $this->session->set_userdata(array('user_id' => $user[0]->id, 'username' => $user[0]->username,
                            'status' => ($user[0]->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED));
                        //$this->tank_auth->clear_login_attempts($user[0]->email); can't run this when doing twitter
                        redirect('auth', 'refresh');
                    }
                } else {
                    echo 'User has not logged in.';
                }
            }
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }
    }

    // function for logging in via google open id
    function yahoo_openid_signin() {
        //$this->load->library('lightopenid'); // loaded in aotoload.php
        // these are some of the attributes we can get from open id
        // refer to 
        // http://code.google.com/p/lightopenid/wiki/GettingMoreInformation
        // for more
        $required_attr = array('namePerson/friendly', 'contact/email',
            'namePerson/first', 'namePerson/last',
            'contact/country/home', 'contact/email', 'pref/language');
        try {
            if (!isset($_GET['openid_mode'])) {
                $lightopenid = new Lightopenid;
                $lightopenid->identity = 'http://me.yahoo.com';
                $lightopenid->required = $required_attr;
                redirect($lightopenid->authUrl(), 'refresh');
            } elseif ($_GET['openid_mode'] == 'cancel') {
                echo 'User has cancelled authentication!';
            } else {
                $lightopenid = new Lightopenid;
                $lightopenid->required = $required_attr;
                if ($lightopenid->validate()) {
                    #Here goes the code that gets interpreted after successful login!!!
                    //print_r($lightopenid);
                    //echo '<br/>';
                    //print_r($lightopenid->identity);
                    //print_r($lightopenid->getAttributes());

                    $yahoo_open_id = $lightopenid->identity;
                    $this->session->set_userdata('yahoo_open_id', $yahoo_open_id);

                    // does this user exist?
                    $user = $this->user_model->get_user_by_sm(array('yahoo_open_id' => $yahoo_open_id), 'yahoo_open_id');
                    if (sizeof($user) == 0) {
                        redirect('auth_other/fill_user_info', 'refresh');
                    } else {
                        // simulate what happens in the tank auth
                        $this->session->set_userdata(array('user_id' => $user[0]->id, 'username' => $user[0]->username,
                            'status' => ($user[0]->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED));
                        //$this->tank_auth->clear_login_attempts($user[0]->email); can't run this when doing twitter
                        redirect('auth', 'refresh');
                    }
                } else {
                    echo 'User has not logged in.';
                }
            }
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }
    }

    // called when user logs in via facebook/twitter for the first time
    function fill_user_info() {

        // load validation library and rules
        $this->load->config('tank_auth', TRUE);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length') . ']');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length') . ']|max_length[' . $this->config->item('password_max_length') . ']|alpha_dash');

        // Run the validation
        if ($this->form_validation->run() == false) {
            $this->load->view('auth_other/fill_user_info');
        } else {

            $username = mysql_real_escape_string($this->input->post('username'));
            $email = mysql_real_escape_string($this->input->post('email'));
            $password = mysql_real_escape_string($this->input->post('password'));

            $criteria = array(
                'user.email' => $email
            );
            $user = $this->user_model->select($criteria);

            if (count($user) != 1) {
                $password = $this->generate_password(9, 8);
                $data = $this->tank_auth->create_user($username, $email, $password, false);
                $user_id = $data['user_id'];
            } else {
                $user_id = $user[0]->user_id;
            }

            /*
             * We now must create a new user in tank auth with a random password in order
             * to insert this user and also into user profile table with tank auth id
             */

            if ($this->session->userdata('facebook_id')) {
                $this->user_model->update_user_profile($user_id, array('facebook_id' => $this->session->userdata('facebook_id')));
            } else if ($this->session->userdata('twitter_user_id')) {
                $this->user_model->update_user_profile($user_id, array('twitter_id' => $this->session->userdata('twitter_user_id')));
            } else if ($this->session->userdata('gfc_id')) {
                $this->user_model->update_user_profile($user_id, array('gfc_id' => $this->session->userdata('gfc_id')));
            } else if ($this->session->userdata('google_open_id')) {
                $this->user_model->update_user_profile($user_id, array('google_open_id' => $this->session->userdata('google_open_id')));
            } else if ($this->session->userdata('yahoo_open_id')) {
                $this->user_model->update_user_profile($user_id, array('yahoo_open_id' => $this->session->userdata('yahoo_open_id')));
            }
            // let the user login via tank auth
            $this->tank_auth->login($email, $password, false, false, true);
            redirect('auth', 'refresh');
        }
    }

    // a logout function for 3rd party
    function logout() {

        $redirect = site_url('auth/logout');
        if ($this->session->userdata('gfc_id') && $this->session->userdata('gfc_id') != '') {
            $redirect = null;
        }

        // set all user data to empty
        $this->session->set_userdata(array('facebook_id' => '',
            'twitter_id' => '',
            'gfc_id' => '',
            'google_open_id' => '',
            'yahoo_open_id' => ''));
        if ($redirect) {
            redirect($redirect, 'refresh');
        } else {
            $this->load->view('gfc_logout');
        }
    }

    // function to validate the email input field
    function email_check($email) {
        $user = $this->users->get_user_by_email($email);
        if (sizeof($user) > 0) {
            $this->form_validation->set_message('email_check', 'This %s is already registered.');
            return false;
        } else {
            return true;
        }
    }

    function username_check($username) {
        $user = $this->users->get_user_by_username($username);
        if (sizeof($user) > 0) {
            $this->form_validation->set_message('username_check', 'This %s is already registered.');
            return false;
        } else {
            return true;
        }
    }

    // generates a random password for the user
    function generate_password($length = 9, $strength = 0) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

}

/* End of file main.php */
/* Location: ./freally_app/controllers/main.php */