<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Article
 *
 * @author rotichmordecai
 */
class Articles extends MY_Controller {

    var $data;

    function __construct() {
        parent::__construct();

        $this->load->library(array('template', 'form_validation'));
        $this->load->helper(array('form', 'url', 'galashia'));
        $this->load->model(array('user_model', 'category_model', 'comment_model', 'article_model', 'analytics_model'));

        if ($this->uri->segment(2) != 'login') {
            $this->view_path = 'articles/';
            $this->data = array();
            $this->front_assets();
        }
        $this->data = array();

        $this->config->set_item('crumb_divider', '');
        $this->config->set_item('tag_open', '<ul>');
        $this->config->set_item('tag_close', '</ul>');
        $this->config->set_item('crumb_open', '<li>');
        $this->config->set_item('crumb_close', '</li>');
        $this->config->set_item('crumb_last_open', '<li>');

        $this->breadcrumbs->settings();

        $this->data["categories"] = $categories = $this->category_model->select();
    }

    function index() {
        redirect('articles/view');
    }

    function view($_category = NULL) {

        $this->load->library('pagination');

        $re1 = '(c)'; # Any Single Word Character (Not Whitespace) 1
        $re2 = '(_)'; # Any Single Character 1
        $re3 = '(\\d+)'; # Integer Number 1

        $category_id = $this->input->get('category_id');

        if ($c = preg_match_all("/" . $re1 . $re2 . $re3 . "/is", $_category, $matches)) {
            $w1 = $matches[1][0];
            $c1 = $matches[2][0];
            $category_id = $matches[3][0];
        }

        $this->data["categories"] = $categories = $this->category_model->select();
        $category_ids = array();
        category_ids($category_ids, $categories, $category_id);
        $this->data['title'] = 'Galashia : Articles';
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Articles', '/articles');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $criteria = NULL;

        if ($category_id > 0) {
            $criteria = array(
                'article.category_id' => $category_ids
            );
        }

        $criteria['article.status'] = array(1);

        $group_by = array(
            'article.id'
        );

        $order_by = array(
            'article.id' => 'desc'
        );

        $config = array();

        $config["base_url"] = site_url('articles/' . $this->uri->segment(2));
        $config["total_rows"] = $this->article_model->fetch_count($criteria, $group_by, $order_by);
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;

        if (count($_GET) > 0)
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->data["results"] = $this->article_model->fetch($config["per_page"], $page, $criteria, $group_by, $order_by);
        $this->data["pagination"] = $this->pagination->create_links();

        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function browse() {
        
    }

}
