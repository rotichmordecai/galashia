<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of profile
 *
 * @author rotichmordecai
 */
class Profile extends MY_Controller {

    var $data;

    function __construct() {

        parent::__construct();

        $this->load->library(array('template', 'form_validation', 'imaging'));
        $this->load->helper(array('form', 'url', 'galashia'));

        $this->load->model(array('user_profile_model',
            'user_model',
            'category_model',
            'article_model',
            'analytics_model',
            'comment_model',
            'message_thread_model',
            'message_model',
            'review_model',
            'follow_model',
            'image_model'
        ));

        $this->config->load('imaging');

        if ($this->uri->segment(2) != 'login') {
            $this->view_path = 'profile/';
            $this->data = array();
            $this->front_assets();
        }
        $this->data = array();

        $this->config->set_item('crumb_divider', '');
        $this->config->set_item('tag_open', '<ul>');
        $this->config->set_item('tag_close', '</ul>');
        $this->config->set_item('crumb_open', '<li>');
        $this->config->set_item('crumb_close', '</li>');
        $this->config->set_item('crumb_last_open', '<li>');

        $this->breadcrumbs->settings();

        $this->data["categories"] = $categories = $this->category_model->select();
    }

    function index($username) {

        $criteria = array(
            'username' => $username
        );

        $this->data["username"] = $username;
        $profile = $this->user_model->select($criteria);

        if (isset($profile[0])) {

            $profile = $profile[0];
            $this->data["profile"] = $profile;
            $criteria = array(
                'user_id' => $profile->id
            );
            $this->data['results'] = $this->article_model->select($criteria);

            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Profile - ' . $profile->username, '/p/' . $profile->username);
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->data['title'] = 'Galashia : ' . $profile->username;
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        }
    }

    function images() {
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Upload Images', '/project/upload_images');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : Upload Images';
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function upload_image() {

        if (isset($_FILES["image"])) {

            $this->imaging->upload($_FILES["image"]);
            $this->imaging->file_overwrite = TRUE;

            $image = '';
            $thumb = '';

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->process($this->config->item('user_image_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->imaging->allowed = array('image/*');

                if ($this->imaging->image_src_x > $this->config->item('thumb_size') || $this->imaging->image_src_y > $this->config->item('thumb_size')) {
                    $this->imaging->image_resize = true;
                    $this->imaging->image_x = $this->config->item('thumb_size');
                    $this->imaging->image_ratio_y = true;
                }

                $this->imaging->file_overwrite = TRUE;
                $this->imaging->process($this->config->item('user_thumb_path'));

                if ($this->imaging->processed == true) {
                    $thumb = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->image_model->set_image($image);
                $this->image_model->set_thumb($thumb);
                $this->image_model->set_user_id($this->tank_auth->get_user_id());
                $this->image_model->set_status(1);

                if ($this->image_model->insert() > 0) {
                    $this->message->set('success', 'Success! Image Added');
                } else {
                    $this->message->set('error', 'Error! Image could not be added');
                }

                exit();
            } else {
                $error = $this->imaging->error;
            }
        }
    }

    function action_follow_user() {

        $this->form_validation->set_rules('follow_id', 'follow_id', 'required|trim|xss_clean');

        if ($this->form_validation->run() != FALSE) {

            $criteria = array(
                'follow.user_id_1' => $this->tank_auth->get_user_id(),
                'follow.user_id_2' => set_value('follow_id')
            );
            $follow = $this->follow_model->select($criteria);

            if (count($follow)) {

                $criteria = array(
                    'follow.user_id_1' => $this->tank_auth->get_user_id(),
                    'follow.user_id_2' => set_value('follow_id')
                );
                $delete = $this->follow_model->delete($criteria);

                if ($delete) {
                    $return['success'] = TRUE;
                    $return['response'] = 'Success! unfollow successful';
                } else {
                    $return['success'] = FALSE;
                    $return['response'] = 'Error! unfollow unsuccessful';
                }
            } else {

                $this->follow_model->set_user_id_1($this->tank_auth->get_user_id());
                $this->follow_model->set_user_id_2(set_value('follow_id'));
                $this->follow_model->set_status(1);

                if ($this->follow_model->insert() > 0) {
                    $return['success'] = TRUE;
                    $return['response'] = 'Success! follow successful';
                } else {
                    $return['success'] = FALSE;
                    $return['response'] = 'Error! follow unsuccessful';
                }
            }
        }
        echo json_encode($return);
    }

    function action_submit_review() {

        $return = array();

        $this->form_validation->set_rules('recipient_id', 'Recipient', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user_rating', 'User rating', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user_review', 'User review', 'required|trim|xss_clean');

        if ($this->form_validation->run() != FALSE) {

            $this->review_model->set_user_id1($this->tank_auth->get_user_id());
            $this->review_model->set_user_id2(set_value('recipient_id'));
            $this->review_model->set_rating(set_value('user_rating'));
            $this->review_model->set_review(set_value('user_review'));
            $this->review_model->set_status(1);

            if ($this->review_model->insert() > 0) {
                $return['success'] = TRUE;
                $return['response'] = 'Success! review Added';
            } else {
                $return['success'] = FALSE;
                $return['response'] = 'Error! review could not be added';
            }
        }
        echo json_encode($return);
    }

    function action_submit_message() {

        $return = array();

        $this->form_validation->set_rules('recipient_id', 'recipient_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('message_title', 'message_title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('message_text', 'message_text', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else {

            $criteria = array(
                'message_thread.title' => set_value('message_title')
            );
            $message_thread = $this->message_thread_model->select($criteria);

            $message_thread_id = NULL;

            if (count($message_thread)) {
                $message_thread_id = $message_thread[0]->id;
            } else {
                $this->message_thread_model->set_title(set_value('message_title'));
                $message_thread_id = $this->message_thread_model->insert();
            }

            $this->message_model->set_user_id1($this->tank_auth->get_user_id());
            $this->message_model->set_user_id2(set_value('recipient_id'));
            $this->message_model->set_message_thread_id($message_thread_id);
            $this->message_model->set_message_title(set_value('message_title'));
            $this->message_model->set_message_text(set_value('message_text'));
            $this->message_model->set_status(1);

            if ($this->message_model->insert() > 0) {
                $return['success'] = TRUE;
                $return['response'] = 'Success! message sent';
            } else {
                $return['success'] = FALSE;
                $return['response'] = 'Error! message could not be sent';
            }
        }

        echo json_encode($return);
    }

    function submit_follow($username) {
        $criteria = array(
            'user.username' => $username
        );
        $user = $this->user_model->select($criteria);
        $this->data['user'] = isset($user[0]) ? $user[0] : NULL;
        $this->load->view($this->view_path . __FUNCTION__, $this->data);
    }

    function submit_review($username) {
        $criteria = array(
            'user.username' => $username
        );
        $user = $this->user_model->select($criteria);
        $this->data['user'] = isset($user[0]) ? $user[0] : NULL;
        $this->load->view($this->view_path . __FUNCTION__, $this->data);
    }

    function submit_message($username) {
        $criteria = array(
            'user.username' => $username
        );
        $user = $this->user_model->select($criteria);
        $this->data['user'] = isset($user[0]) ? $user[0] : NULL;
        $this->load->view($this->view_path . __FUNCTION__, $this->data);
    }

    function update() {

        if (!$this->tank_auth->is_logged_in()) {
            redirect('');
        }

        $criteria = array(
            'username' => $this->tank_auth->get_username()
        );

        $profile = $this->user_model->select($criteria);

        if (isset($profile[0])) {


            $this->form_validation->set_rules('user_name', 'user_name', 'required|trim|xss_clean');
            $this->form_validation->set_rules('country', 'country', 'required|trim|xss_clean');
            $this->form_validation->set_rules('website', 'website', 'required|trim|xss_clean');
            $this->form_validation->set_rules('user_bio', 'user bio', 'required|trim|xss_clean');
            $this->form_validation->set_rules('facebook', 'facebook profile', 'trim|xss_clean');
            $this->form_validation->set_rules('twitter', 'twitter profile', 'trim|xss_clean');
            $this->form_validation->set_rules('linkedin', 'linkedin profile', 'trim|xss_clean');
            $this->form_validation->set_rules('googleplus', 'google plus profile', 'trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                
            } else {

                $this->user_profile_model->set_id($this->tank_auth->get_user_id());
                $this->user_profile_model->set_user_name(set_value('user_name'));
                $this->user_profile_model->set_country(set_value('country'));
                $this->user_profile_model->set_website(set_value('website'));
                $this->user_profile_model->set_user_bio(set_value('user_bio'));

                if (strpos(set_value('facebook'), 'username') == false) {
                    $this->user_profile_model->set_facebook(set_value('facebook'));
                }

                if (strpos(set_value('twitter'), 'username') == false) {
                    $this->user_profile_model->set_twitter(set_value('twitter'));
                }

                if (strpos(set_value('linkedin'), 'username') == false) {
                    $this->user_profile_model->set_linkedin(set_value('linkedin'));
                }

                if (strpos(set_value('googleplus'), 'username') == false) {
                    $this->user_profile_model->set_googleplus(set_value('googleplus'));
                }

                if ($this->user_profile_model->update() > 0) {
                    $this->message->set('success', 'Success! your profile has been updated');
                } else {
                    $this->message->set('error', 'Error! profile could not be updated');
                }
            }


            $this->data["profile"] = $profile = $profile[0];

            $criteria = array(
                'user_id' => $profile->id
            );

            $user_profile = $this->user_profile_model->select($criteria);
            $this->data["user_profile"] = $user_profile = $user_profile[0];


            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Profile - ' . $profile->username, '/p/' . $profile->username);
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->data['title'] = 'Galashia : ' . $profile->username;

            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        }
    }

    function pagination_timeline() {

        echo '<li>
                <div class="timeline-badge default"><i class="glyphicon glyphicon-arrow-left"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">Left Event</h4>
                    </div>
                    <div class="timeline-body">
                        <p>Add more progress events and milestones to the left or right side of the timeline. Each event can be tagged with a date and given a beautiful icon to symbolize it spectacular meaning.</p>
                    </div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-badge success"><i class="glyphicon glyphicon-thumbs-up"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">Oldest event</h4>
                    </div>
                    <div class="timeline-body">
                        <p>Add more progress events and milestones to the left or right side of the timeline. Each event can be tagged with a date and given a beautiful icon to symbolize it spectacular meaning.</p>
                    </div>
                </div>
            </li>';
    }

    function pagination_articles() {

        $username = $this->input->post('username');

        $criteria = array(
            'username' => $username
        );

        $profile = $this->user_model->select($criteria);

        if (isset($profile[0])) {

            $this->data["profile"] = $profile = $profile[0];
            $criteria = array(
                'user_id' => $profile->id
            );

            $this->data['results'] = $this->article_model->select($criteria);
            $this->load->view($this->view_path . __FUNCTION__, $this->data);
        }
    }

    function pagination_images() {
        
    }

    function articles() {

        if (!$this->tank_auth->is_logged_in()) {
            redirect('');
        }

        $criteria = array(
            'username' => $this->tank_auth->get_username()
        );

        $profile = $this->user_model->select($criteria);

        if (isset($profile[0])) {

            $this->data["profile"] = $profile = $profile[0];

            $criteria = array(
                'user_id' => $profile->id
            );
            $this->data['results'] = $this->article_model->select($criteria);


            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Profile - ' . $profile->username, '/p/' . $profile->username);
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->data['title'] = 'Galashia : ' . $profile->username;

            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        }
    }

}
