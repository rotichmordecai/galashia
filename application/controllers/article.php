<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Article
 *
 * @author rotichmordecai
 */
class Article extends MY_Controller {

    var $data;

    function __construct() {
        parent::__construct();

        $this->load->library(array('template', 'form_validation', 'imaging'));
        $this->load->helper(array('form', 'url', 'galashia'));
        $this->load->model(array('user_model', 'category_model', 'comment_model', 'article_model', 'analytics_model'));
        $this->config->load('imaging');

        if ($this->uri->segment(2) != 'login') {
            $this->view_path = 'article/';
            $this->data = array();
            $this->front_assets();
        }
        $this->data = array();
        $this->data['title'] = 'Galashia';
        $this->data['breadcrumbs'] = '';
        $this->data["categories"] = $this->category_model->select();

        $this->config->set_item('crumb_divider', '');
        $this->config->set_item('tag_open', '<ul>');
        $this->config->set_item('tag_close', '</ul>');
        $this->config->set_item('crumb_open', '<li>');
        $this->config->set_item('crumb_close', '</li>');
        $this->config->set_item('crumb_last_open', '<li>');

        $this->breadcrumbs->settings();

        $this->data["categories"] = $categories = $this->category_model->select();
    }

    function index() {

        $this->data['title'] = 'Galashia : Articles';
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Articles', '/articles');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function view($article_id) {

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('View Article', '/article/view/' . $article_id);
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : View Article';

        $this->form_validation->set_rules('article_id', 'article_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user_id', 'user_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('textcomment', 'textcomment', 'required|trim|xss_clean');


        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            
        } else { // passed validation proceed to post success logic
            // build array for the model
            $this->comment_model->set_article_id(set_value('article_id'));
            $this->comment_model->set_user_id(set_value('user_id'));
            $this->comment_model->set_textcomment(set_value('textcomment'));
            $this->comment_model->set_status(0);

            if ($this->comment_model->insert() == TRUE) {
                
            } else {
                
            }
        }

        $this->data['articles'] = $article = $this->article_model->select(array('id' => $article_id, 'status' => 1));

        if (!is_array($article) || count($article) == 0) {
            redirect('/articles/view');
        }

        $analytics = $this->analytics_model->select(array('article_id' => $article_id));
        if (count($analytics) == 1) {
            $analytics = $analytics[0];
            $this->analytics_model->set_id($analytics->id);
            $this->analytics_model->set_article_id($article_id);
            $this->analytics_model->set_view($analytics->view + 1);
            $this->analytics_model->set_status(1);
            $this->analytics_model->update();
        } else {
            $this->analytics_model->set_article_id($article_id);
            $this->analytics_model->set_view(1);
            $this->analytics_model->set_status(1);
            $this->analytics_model->insert();
        }

        $this->data['analytics'] = isset($analytics) ? $analytics : NULL;
        $this->data['comments'] = $comments = $this->comment_model->select(array('article_id' => $article_id));
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function grammer() {
        $content = $this->input->post('content');
        $grammer = $this->curl->simple_get('https://languagetool.org', array('language' => 'en-US', 'text' => $content), array(CURLOPT_PORT => 8081, CURLOPT_SSL_VERIFYPEER => FALSE));
        $grammer = simplexml_load_string($grammer);

        foreach ($grammer->error as $key => $error) {

            $error_msg = '<h5><strong>Error #' . $error['category'] . ':</strong></h5>';
            $error_msg .= '<strong>Context :</strong>' . $error['context'] . '<br />';
            $error_msg .= '<strong>Replacements :</strong>' . $error['replacements'] . '<br />';
            $error_msg .= '<strong>Error message :</strong>' . $error['msg'] . '<br />';
            $error_msg .= '<strong>Issue :</strong>' . $error['locqualityissuetype'] . '<br />';

            $this->message->set('error', $error_msg);
        }

        echo $this->message->display();
    }

    function post() {

        if (!$this->tank_auth->is_logged_in()) {
            $this->message->set('error', 'Error! Please login to post an Artilcle');
            redirect('auth/login');
        }

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Post Article', '/article/post');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : Post Article';

        $this->form_validation->set_rules('category_id', 'category_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content', 'content', 'required|trim|xss_clean');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->data['category_options'] = result_to_select($this->category_model->select(), 'id', 'title', 'Select Category');
            $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $image = "";
            $error = "";
            $this->imaging->upload($_FILES["image"]);

            $this->imaging->file_overwrite = TRUE;

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));
                $this->imaging->process($this->config->item('image_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));

                if ($this->imaging->image_src_x > $this->config->item('thumb_size') || $this->imaging->image_src_y > $this->config->item('thumb_size')) {
                    $this->imaging->image_resize = true;
                    $this->imaging->image_x = $this->config->item('thumb_size');
                    $this->imaging->image_ratio_y = true;
                }

                $this->imaging->file_overwrite = TRUE;
                $this->imaging->process($this->config->item('thumb_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }
            } else {
                $error = $this->imaging->error;
            }

            $user_id = $this->tank_auth->get_user_id();

            $this->article_model->set_user_id($user_id);
            $this->article_model->set_category_id(set_value('category_id'));
            $this->article_model->set_title(set_value('title'));
            $this->article_model->set_image($image);
            $this->article_model->set_content(set_value('content'));
            $this->article_model->set_status(0);

            // run insert model to write data to db
            if ($this->article_model->insert() > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! Article added');
            } else {
                $this->message->set('error', 'Error! Article could not be added');
            }
            redirect('articles');
        }
    }

    function edit($article_id) {

        if (!$this->tank_auth->is_logged_in()) {
            $this->message->set('error', 'Error! Please login to edit an Artilcle');
            redirect('auth/login');
        }

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Edit Article', '/article/edit/' . $article_id);
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : Edit Article';

        $this->data['articles'] = $article = $this->article_model->select(array('id' => $article_id));

        $article = isset($article[0]) ? $article[0] : NULL;

        $this->form_validation->set_rules('category_id', 'category_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content', 'content', 'required|trim|xss_clean');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->data['category_options'] = result_to_select($this->category_model->select(), 'id', 'title', 'Select Parent');
            $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $image = ($article != NULL) ? $article->image : "";
            $error = "";
            $this->imaging->upload($_FILES["image"]);

            $this->imaging->file_overwrite = TRUE;

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));
                $this->imaging->process($this->config->item('image_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));

                if ($this->imaging->image_src_x > $this->config->item('thumb_size') || $this->imaging->image_src_y > $this->config->item('thumb_size')) {
                    $this->imaging->image_resize = true;
                    $this->imaging->image_x = $this->config->item('thumb_size');
                    $this->imaging->image_ratio_y = true;
                }

                $this->imaging->file_overwrite = TRUE;
                $this->imaging->process($this->config->item('thumb_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }
            } else {
                $error = $this->imaging->error;
            }

            $user_id = $this->tank_auth->get_user_id();

            $this->article_model->set_id($article_id);
            $this->article_model->set_user_id($user_id);
            $this->article_model->set_category_id(set_value('category_id'));
            $this->article_model->set_title(set_value('title'));
            $this->article_model->set_image($image);
            $this->article_model->set_content(set_value('content'));
            $this->article_model->set_status(0);

            // run insert model to write data to db
            if ($this->article_model->update() > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! Article updated');
            } else {
                $this->message->set('error', 'Error! Article could not be updated');
            }
            redirect('articles');
        }
    }

}
