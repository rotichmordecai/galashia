<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends MY_Controller {

    var $data;

    function __construct() {

        parent::__construct();

        $this->load->library(array('template', 'form_validation'));
        $this->load->helper(array('form', 'url', 'galashia'));
        $this->load->model(array('user_model', 'category_model', 'article_model'));

        if ($this->uri->segment(2) != 'login') {
            $this->view_path = 'welcome/';
            $this->data = array();
            $this->front_assets();
        }
        $this->data = array();
        $criteria = array(
            'category.status' => 1
        );
        $this->data["categories"] = $categories = $this->category_model->select($criteria);
    }
    
    public function index() {
        $this->load->view($this->view_path . __FUNCTION__, $this->data);
    }

    public function welcome() {
        $criteria = array(
            'article.status' => 1
        );

        $group_by = array(
            'article.id'
        );

        $order_by = array(
            'article.id' => 'desc'
        );

        $this->data["results"] = $this->article_model->fetch(3, 0, $criteria, $group_by, $order_by);
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */