<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author rotichmordecai
 */
class Page extends MY_Controller {

    var $data;

    function __construct() {
        parent::__construct();

        $this->load->library(array('template', 'form_validation'));
        $this->load->helper(array('form', 'url', 'galashia'));
        $this->load->model(array('user_model', 'category_model', 'article_model'));

        if ($this->uri->segment(2) != 'login') {
            $this->view_path = 'page/';
            $this->data = array();
            $this->front_assets();
        }
        $this->data = array();
        
        $this->config->set_item('crumb_divider', '');
        $this->config->set_item('tag_open', '<ul>');
        $this->config->set_item('tag_close', '</ul>');
        $this->config->set_item('crumb_open', '<li>');
        $this->config->set_item('crumb_close', '</li>');
        $this->config->set_item('crumb_last_open', '<li>');

        $this->breadcrumbs->settings();
        
        $this->data["categories"] = $categories = $this->category_model->select();
        
    }

    function contactus() {
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Contact Us', '/page/contactus');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : Contact Us';
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }
    
    function about() {
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('About Us', '/page/aboutus');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : About Us';
        $this->data["categories"] = $this->category_model->select();
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }
    
    function services() {
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Services', '/page/services');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : Our Services';
        $this->data["categories"] = $this->category_model->select();
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }
	
	function pricing() {
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Services', '/page/services');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['title'] = 'Galashia : Our Pricing';
        $this->data["categories"] = $this->category_model->select();
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

}
