<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author rotichmordecai
 */
class Admin extends Administrator {

    var $data;
    var $twitter_content;

    function __construct() {

        parent::__construct();

        $this->load->library(array('template', 'form_validation', 'imaging', 'facebook'));
        $this->load->helper(array('form', 'url', 'galashia'));
        $this->load->model(array('user_model', 'category_model', 'article_model', 'analytics_model', 'client_model', 'site_settings_model'));
        $this->config->load('imaging');

        if ($this->uri->segment(2) != 'login') {
            $this->template->set_template('admin');
            $this->view_path = 'admin/';
            $this->data = array();
            $this->assets();
            if (!$this->tank_auth->is_logged_in()) {
                redirect('admin/login');
            }

            if (!in_array($this->tank_auth->get_user_id(), array(1, 2))) {
                redirect('');
            }
        }
        $this->data = array();

        $this->data['fb_login_url'] = $this->facebook->login_url();

        $fb_user = $this->facebook->get_user();
        $this->data['fb_session'] = FALSE;

        $this->twitter_content = NULL;

        if ($this->session->userdata('access_token') && $this->session->userdata('access_token_secret')) {
            $this->twitter_content = $this->data['tw_account'] = $this->connection->get('account/verify_credentials');
            if (isset($this->twitter_content->errors)) {
                $this->reset_session();
                $this->data['tw_session'] = FALSE;
            } else {
                $this->data['tw_session'] = TRUE;
            }
        } else {
            $this->data['tw_session'] = FALSE;
        }


        if (isset($fb_user['id'])) {
            $this->data['fb_session'] = TRUE;
        }

        $this->session->set_userdata('redirect', $this->uri->uri_string());
    }

    function assets() {

        //load css files
        $this->template->add_css('assets/admin/css/bootstrap.css');
        $this->template->add_css('assets/admin/css/sb-admin.css');
        $this->template->add_css('assets/admin/font-awesome/css/font-awesome.min.css');

        // load js files
        $this->template->add_js('assets/admin/js/jquery-1.10.2.js');
        $this->template->add_js('assets/admin/js/bootstrap.js');
        $this->template->add_js('assets/admin/js/morris/chart-data-morris.js');
        $this->template->add_js('assets/admin/js/tablesorter/jquery.tablesorter.js');
        $this->template->add_js('assets/admin/js/tablesorter/tables.js');
        $this->template->add_js('assets/front/assets/js/jquery.prettyPhoto.js');
        $this->template->add_js('assets/tinymce/jquery.tinymce.min.js');
    }

    function index() {
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function login() {
        $this->template->set_template('login');
        $this->view_path = 'admin/';
        $this->assets();
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function logout() {
        
    }

    function add_client() {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Clients', '/admin/view_clients');
        $this->breadcrumbs->push('Add Client', '/admin/add_client');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else {

            $logo = "";
            $error = "";
            $this->imaging->upload($_FILES["logo"]);

            $this->imaging->file_overwrite = TRUE;

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));
                $this->imaging->process($this->config->item('image_path'));

                if ($this->imaging->processed == true) {
                    $logo = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));

                if ($this->imaging->image_src_x > $this->config->item('thumb_size') || $this->imaging->image_src_y > $this->config->item('thumb_size')) {
                    $this->imaging->image_resize = true;
                    $this->imaging->image_x = $this->config->item('thumb_size');
                    $this->imaging->image_ratio_y = true;
                }

                $this->imaging->file_overwrite = TRUE;
                $this->imaging->process($this->config->item('thumb_path'));

                if ($this->imaging->processed == true) {
                    $logo = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }
            } else {
                $error = $this->imaging->error;
            }

            $this->client_model->set_title(set_value('title'));
            $this->client_model->set_logo($logo);
            $this->client_model->set_description(set_value('description'));
            $this->client_model->set_status(set_value('status'));

            // run insert model to write data to db
            if ($this->client_model->insert() > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! Client added');
            } else {
                $this->message->set('error', 'Error! Client could not be added');
            }
            redirect('admin/view_clients');
        }
    }

    function edit_client($client_id) {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Clients', '/admin/view_clients');
        $this->breadcrumbs->push('Edit Client', '/admin/edit_client');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->data['clients'] = $client = $this->client_model->select(array('id' => $client_id));
        $client = isset($client[0]) ? $client[0] : NULL;

        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else {

            $logo = ($client != NULL) ? $client->logo : "";
            $error = "";
            $this->imaging->upload($_FILES["logo"]);

            $this->imaging->file_overwrite = TRUE;

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));
                $this->imaging->process($this->config->item('image_path'));

                if ($this->imaging->processed == true) {
                    $logo = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));

                if ($this->imaging->image_src_x > $this->config->item('thumb_size') || $this->imaging->image_src_y > $this->config->item('thumb_size')) {
                    $this->imaging->image_resize = true;
                    $this->imaging->image_x = $this->config->item('thumb_size');
                    $this->imaging->image_ratio_y = true;
                }

                $this->imaging->file_overwrite = TRUE;
                $this->imaging->process($this->config->item('thumb_path'));

                if ($this->imaging->processed == true) {
                    $logo = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }
            } else {
                $error = $this->imaging->error;
            }

            $this->client_model->set_id($client_id);
            $this->client_model->set_title(set_value('title'));
            $this->client_model->set_logo($logo);
            $this->client_model->set_description(set_value('description'));
            $this->client_model->set_status(set_value('status'));

            // run insert model to write data to db
            if ($this->client_model->update() > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! Client updated');
            } else {
                $this->message->set('error', 'Error! Client could not be updated');
            }
            redirect('admin/view_clients');
        }
    }

    function add_user() {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Users', '/admin/view_users');
        $this->breadcrumbs->push('Add new User', '/admin/add_user');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $use_username = $this->config->item('use_username', 'tank_auth');
        if ($use_username) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']|alpha_dash');
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]');


        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $email_activation = $this->config->item('email_activation', 'tank_auth');


            if (!is_null($data = $this->tank_auth->create_user($use_username ? $this->form_validation->set_value('username') : '', $this->form_validation->set_value('email'), $this->form_validation->set_value('password'), $email_activation))) {         // success
                $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                if ($email_activation) {         // send "activate" email
                    $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;
                    $this->_send_email('activate', $data['email'], $data);
                    unset($data['password']); // Clear password (just for any case)
                } else {
                    if ($this->config->item('email_account_details', 'tank_auth')) { // send "welcome" email
                        $this->_send_email('welcome', $data['email'], $data);
                    }
                    unset($data['password']); // Clear password (just for any case)
                }

                $this->message->set('success', 'Success! User account created');
            } else {
                $this->message->set('error', 'Error! User account cound not be created');
            }

            redirect('admin/view_users');
        }
    }

    function view_user($user_id) {
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function delete_user($user_id) {

        $this->user_model->set_id($user_id);

        if ($this->user_model->delete() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! User deleted');
        } else {
            $this->message->set('error', 'Error! User could not be deleted');
        }

        redirect('admin/view_users');
    }

    function set_user_admin($user_id, $is_admin = NULL) {


        $this->user_model->set_id($user_id);
        $this->user_model->set_is_admin($is_admin);

        if ($this->user_model->update() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! User set as admin');
        } else {
            $this->message->set('error', 'Error! User could not be set as admin');
        }

        redirect('admin/view_users');
    }

    function delete_article($article_id) {

        $this->article_model->set_id($article_id);

        if ($this->article_model->delete() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! Article deleted');
        } else {
            $this->message->set('error', 'Error! Article could not be deleted');
        }

        redirect('admin/view_articles');
    }

    function delete_client($client_id) {

        $this->client_model->set_id($client_id);

        if ($this->client_model->delete() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! Client deleted');
        } else {
            $this->message->set('error', 'Error! Client could not be deleted');
        }

        redirect('admin/view_clients');
    }

    function delete_category($category_id) {

        $this->category_model->set_id($category_id);

        if ($this->category_model->delete() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! Category deleted');
        } else {
            $this->message->set('error', 'Error! Category could not be deleted');
        }

        redirect('admin/view_categories');
    }

    function block_user($user_id) {

        $this->user_model->set_id($user_id);
        $this->user_model->set_activated(0);
        $this->user_model->set_banned(1);

        if ($this->user_model->update() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! User blocked');
        } else {
            $this->message->set('error', 'Error! User could not be blocked');
        }

        redirect('admin/view_users');
    }

    function block_category($category_id) {

        $this->category_model->set_id($category_id);
        $this->category_model->set_status(0);

        if ($this->category_model->update() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! Category blocked');
        } else {
            $this->message->set('error', 'Error! Category could not be blocked');
        }

        redirect('admin/view_categories');
    }

    function block_article($article_id) {

        $this->article_model->set_id($article_id);
        $this->article_model->set_status(0);

        if ($this->article_model->update() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! Article blocked');
        } else {
            $this->message->set('error', 'Error! Article could not be blocked');
        }

        redirect('admin/view_articles');
    }

    function block_client($client_id) {

        $this->client_model->set_id($client_id);
        $this->client_model->set_status(0);

        if ($this->client_model->update() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! Client blocked');
        } else {
            $this->message->set('error', 'Error! Client could not be blocked');
        }

        redirect('admin/view_articles');
    }

    function activate_user($user_id) {

        $this->user_model->set_id($user_id);
        $this->user_model->set_activated(1);
        $this->user_model->set_banned(0);

        // run insert model to write data to db
        if ($this->user_model->update() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! User activated');
        } else {
            $this->message->set('error', 'Error! User could not be activated');
        }

        redirect('admin/view_users');
    }

    function activate_category($user_id) {

        $this->category_model->set_id($user_id);
        $this->category_model->set_status(1);

        // run insert model to write data to db
        if ($this->category_model->update() > 0) { // the information has therefore been successfully saved in the db    
            $this->message->set('success', 'Success! Category activated');
        } else {
            $this->message->set('error', 'Error! Category could not be activated');
        }

        redirect('admin/view_categories');
    }

    function activate_article($article_id) {

        $this->article_model->set_id($article_id);
        $this->article_model->set_status(1);

        // run insert model to write data to db
        if ($this->article_model->update() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! Article activated');
        } else {
            $this->message->set('error', 'Error! Article could not be activated');
        }
        redirect('admin/view_articles');
    }

    function activate_client($client_id) {

        $this->client_model->set_id($client_id);
        $this->client_model->set_status(1);

        // run insert model to write data to db
        if ($this->client_model->update() > 0) { // the information has therefore been successfully saved in the db
            $this->message->set('success', 'Success! Client activated');
        } else {
            $this->message->set('error', 'Error! Client could not be activated');
        }
        redirect('admin/view_clients');
    }

    function edit_user($user_id) {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Users', '/admin/view_users');
        $this->breadcrumbs->push('Add new User', '/admin/add_user');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['users'] = $this->user_model->select(array('id' => $user_id));

        $use_username = $this->config->item('use_username', 'tank_auth');
        if ($use_username) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']|alpha_dash');
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|xss_clean|matches[password]');


        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            // build array for the model
            $hasher = new PasswordHash(
                    $this->config->item('phpass_hash_strength', 'tank_auth'), $this->config->item('phpass_hash_portable', 'tank_auth'));
            $hashed_password = $hasher->HashPassword(set_value('password'));

            $this->user_model->set_id($user_id);
            $this->user_model->set_username(set_value('username'));
            $this->user_model->set_email(set_value('email'));
            if (!empty($_POST['password']))
                $this->user_model->set_password($hashed_password);

            // run insert model to write data to db
            if ($this->user_model->update() > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! User updated');
            } else {
                $this->message->set('error', 'Error! User could not be updated');
            }
            redirect('admin/view_users');
        }
    }

    function view_users() {

        $this->load->library('pagination');

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Users', '/admin/view_users');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $config = array();
        $config["base_url"] = site_url('admin/view_users');
        $config["total_rows"] = $this->user_model->count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->data["results"] = $this->user_model->fetch($config["per_page"], $page);
        $this->data["pagination"] = $this->pagination->create_links();

        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function add_category() {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Categories', '/admin/view_categories');
        $this->breadcrumbs->push('Add New Category', '/admin/add_category');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->form_validation->set_rules('category_id', 'category_id', 'trim|xss_clean');
        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->data['category_options'] = result_to_select($this->category_model->select(), 'id', 'title', 'Select Parent');
            $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            // build array for the model
            $form_data = array(
                'category_id' => set_value('category_id'),
                'title' => set_value('title'),
                'status' => set_value('status'),
            );

            $this->category_model->set_category_id(set_value('category_id'));
            $this->category_model->set_title(set_value('title'));
            $this->category_model->set_status(set_value('status'));

            // run insert model to write data to db
            if ($this->category_model->insert() > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! Category added');
            } else {
                $this->message->set('error', 'Error! Category could not be added');
            }
            redirect('admin/view_categories');
        }
    }

    function view_category($article_id) {
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function edit_category($category_id) {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Categories', '/admin/view_categories');
        $this->breadcrumbs->push('Add New Category', '/admin/add_category');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['categories'] = $this->category_model->select(array('category.id' => $category_id));

        $this->form_validation->set_rules('category_id', 'category_id', 'trim|xss_clean');
        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->data['category_options'] = result_to_select($this->category_model->select(), 'id', 'title', 'Select Parent');
            $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            // build array for the model
            $this->category_model->set_id($category_id);
            $this->category_model->set_category_id(set_value('category_id'));
            $this->category_model->set_title(set_value('title'));
            $this->category_model->set_status(set_value('status'));

            // run insert model to write data to db
            if ($this->category_model->update() > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! Category updated');
            } else {
                $this->message->set('error', 'Error! Category could not be updated');
            }
            redirect('admin/view_categories');
        }
    }

    function view_categories() {

        $this->load->library('pagination');

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Categories', '/admin/view_categories');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $config = array();
        $config["base_url"] = site_url('admin/view_categories');
        $config["total_rows"] = $this->category_model->count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->data["results"] = $this->category_model->fetch($config["per_page"], $page);
        $this->data["pagination"] = $this->pagination->create_links();

        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function add_article() {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Articles', '/admin/view_articles');
        $this->breadcrumbs->push('Add New Article', '/admin/add_article');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->form_validation->set_rules('category_id', 'category_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content', 'content', 'required|trim|xss_clean');
        $this->form_validation->set_rules('fb_msg', 'fb_msg', 'trim|xss_clean');
        $this->form_validation->set_rules('tw_msg', 'tw_msg', 'trim|xss_clean');
        $this->form_validation->set_rules('li_msg', 'li_msg', 'trim|xss_clean');
        $this->form_validation->set_rules('gp_msg', 'gp_msg', 'trim|xss_clean');
        $this->form_validation->set_rules('tagline', 'tagline', 'trim|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'trim|xss_clean');
        $this->form_validation->set_rules('keywords', 'keywords', 'trim|xss_clean');
        $this->form_validation->set_rules('author', 'author', 'trim|xss_clean');
        $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        $this->data['fb_account'] = $fb_account = $this->facebook->get_user_accounts();
        $this->data['fb_user'] = $this->facebook->get_user();


        if ($this->form_validation->run() == FALSE) {
            $this->data['category_options'] = result_to_select($this->category_model->select(), 'id', 'title', 'Select Category');
            $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $image = "";
            $error = "";
            $this->imaging->upload($_FILES["image"]);

            $this->imaging->file_overwrite = TRUE;

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));
                $this->imaging->process($this->config->item('image_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));

                if ($this->imaging->image_src_x > $this->config->item('thumb_size') || $this->imaging->image_src_y > $this->config->item('thumb_size')) {
                    $this->imaging->image_resize = true;
                    $this->imaging->image_x = $this->config->item('thumb_size');
                    $this->imaging->image_ratio_y = true;
                }

                $this->imaging->file_overwrite = TRUE;
                $this->imaging->process($this->config->item('thumb_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }
            } else {
                $error = $this->imaging->error;
            }

            $user_id = $this->tank_auth->get_user_id();

            $this->article_model->set_user_id($user_id);
            $this->article_model->set_category_id(set_value('category_id'));
            $this->article_model->set_title(set_value('title'));
            $this->article_model->set_image($image);
            $this->article_model->set_content(set_value('content'));
            $this->article_model->set_fb_msg(set_value('fb_msg'));
            $this->article_model->set_tw_msg(set_value('tw_msg'));
            $this->article_model->set_li_msg(set_value('li_msg'));
            $this->article_model->set_gp_msg(set_value('gp_msg'));
            $this->article_model->set_tagline(set_value('tagline'));
            $this->article_model->set_description(set_value('description'));
            $this->article_model->set_keywords(set_value('keywords'));
            $this->article_model->set_author(set_value('author'));
            $this->article_model->set_status(set_value('status'));

            // run insert model to write data to db
            $insert_id = $this->article_model->insert();
            if ($insert_id > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! Article added');

                $post_accounts = $this->input->post('twaccount');

                if (is_array($post_accounts)) {
                    foreach ($post_accounts as $post_account) {
                        if ($this->twitter_content != NULL) {
                            if ($this->twitter_content->id == $post_account) {
                                $data = array(
                                    'status' => 'Just testing'
                                );
                                $result = $this->connection->post('statuses/update', $data);
                                if (!isset($result->errors)) {
                                    
                                } else {
                                    
                                }
                            }
                        }
                    }
                }


                $post_accounts = $this->input->post('fbaccount');

                if (is_array($post_accounts)) {
                    foreach ($post_accounts as $post_account) {
                        foreach ($fb_account['data'] as $account) {
                            if ($post_account == 'me') {
                                $param = array(
                                    'link' => site_url('article/view/' . $insert_id . '_' . url_title(set_value('title'))),
                                    'message' => 'Galashia - ' . set_value('title')
                                );
                                $this->facebook->post_to_account($post_account, $param);
                            } else {
                                if ($account->id == $post_account) {
                                    $param = array(
                                        'access_token' => $account->access_token,
                                        'link' => site_url('article/view/' . $insert_id . '_' . url_title(set_value('title'))),
                                        'message' => 'Galashia - ' . set_value('title')
                                    );
                                    $this->facebook->post_to_account($post_account, $param);
                                }
                            }
                        }
                    }
                }
            } else {
                $this->message->set('error', 'Error! Article could not be added');
            }
            redirect('admin/view_articles');
        }
    }

    function view_article($article_id) {
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function edit_article($article_id) {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Articles', '/admin/view_articles');
        $this->breadcrumbs->push('Add New Article', '/admin/add_article');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->data['articles'] = $article = $this->article_model->select(array('id' => $article_id));

        $article = isset($article[0]) ? $article[0] : NULL;

        $this->form_validation->set_rules('category_id', 'category_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content', 'content', 'required|trim|xss_clean');
        $this->form_validation->set_rules('fb_msg', 'fb_msg', 'trim|xss_clean');
        $this->form_validation->set_rules('tw_msg', 'tw_msg', 'trim|xss_clean');
        $this->form_validation->set_rules('li_msg', 'li_msg', 'trim|xss_clean');
        $this->form_validation->set_rules('gp_msg', 'gp_msg', 'trim|xss_clean');
        $this->form_validation->set_rules('tagline', 'tagline', 'trim|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'trim|xss_clean');
        $this->form_validation->set_rules('keywords', 'keywords', 'trim|xss_clean');
        $this->form_validation->set_rules('author', 'author', 'trim|xss_clean');
        $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->data['category_options'] = result_to_select($this->category_model->select(), 'id', 'title', 'Select Parent');
            $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $image = ($article != NULL) ? $article->image : "";
            $error = "";
            $this->imaging->upload($_FILES["image"]);

            $this->imaging->file_overwrite = TRUE;

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));
                $this->imaging->process($this->config->item('image_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = url_title(set_value('title'));

                if ($this->imaging->image_src_x > $this->config->item('thumb_size') || $this->imaging->image_src_y > $this->config->item('thumb_size')) {
                    $this->imaging->image_resize = true;
                    $this->imaging->image_x = $this->config->item('thumb_size');
                    $this->imaging->image_ratio_y = true;
                }

                $this->imaging->file_overwrite = TRUE;
                $this->imaging->process($this->config->item('thumb_path'));

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $error = $this->imaging->error;
                }
            } else {
                $error = $this->imaging->error;
            }

            $user_id = $this->tank_auth->get_user_id();

            $this->article_model->set_id($article_id);
            $this->article_model->set_user_id($user_id);
            $this->article_model->set_category_id(set_value('category_id'));
            $this->article_model->set_title(set_value('title'));
            $this->article_model->set_image($image);
            $this->article_model->set_content(set_value('content'));
            $this->article_model->set_fb_msg(set_value('fb_msg'));
            $this->article_model->set_tw_msg(set_value('tw_msg'));
            $this->article_model->set_li_msg(set_value('li_msg'));
            $this->article_model->set_gp_msg(set_value('gp_msg'));
            $this->article_model->set_tagline(set_value('tagline'));
            $this->article_model->set_description(set_value('description'));
            $this->article_model->set_keywords(set_value('keywords'));
            $this->article_model->set_author(set_value('author'));
            $this->article_model->set_status(set_value('status'));

            // run insert model to write data to db
            if ($this->article_model->update() > 0) { // the information has therefore been successfully saved in the db
                $this->message->set('success', 'Success! Article updated');
            } else {
                $this->message->set('error', 'Error! Article could not be updated');
            }
            redirect('admin/view_articles');
        }
    }

    function view_articles() {
        $this->load->library('pagination');

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Articles', '/admin/view_articles');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $criteria = null;
        $group_by = null;
        $order_by = null;

        $config = array();
        $config["base_url"] = site_url('admin/view_articles');
        $config["total_rows"] = $this->article_model->count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->data["results"] = $this->article_model->fetch($config["per_page"], $page, $criteria, $group_by, $order_by);
        $this->data["pagination"] = $this->pagination->create_links();

        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function view_clients() {
        $this->load->library('pagination');

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Articles', '/admin/view_articles');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $config = array();
        $config["base_url"] = site_url('admin/view_clients');
        $config["total_rows"] = $this->client_model->count();
        $config["per_page"] = 1;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->data["results"] = $this->client_model->fetch(array(), $config["per_page"], $page);
        $this->data["pagination"] = $this->pagination->create_links();

        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function settings() {

        $this->breadcrumbs->push('Home', '/admin');
        $this->breadcrumbs->push('Settings', '/admin/settings');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->data['settings'] = $article = $this->site_settings_model->select(array('id' => 1));

        $this->form_validation->set_rules('id', 'id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('name', 'name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('tagline', 'tagline', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('keywords', 'keywords', 'required|trim|xss_clean');
        $this->form_validation->set_rules('author', 'author', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            
        } else {

            $this->site_settings_model->set_id(set_value('id'));
            $this->site_settings_model->set_name(set_value('name'));
            $this->site_settings_model->set_title(set_value('title'));
            $this->site_settings_model->set_tagline(set_value('tagline'));
            $this->site_settings_model->set_description(set_value('description'));
            $this->site_settings_model->set_keywords(set_value('keywords'));
            $this->site_settings_model->set_author(set_value('author'));

            if ($this->site_settings_model->update() > 0) {
                $this->message->set('success', 'Success! Updated site settings');
            } else {
                $this->message->set('error', 'Error! Site settings could not be updated');
            }
        }

        $this->data['status_options'] = array(1 => 'active', 0 => 'Not Active');
        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

}
