<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


if (!function_exists('result_to_select')) {

    function result_to_select($results, $value = 'id', $key = 'title', $default_text = '') {
        // Converts objects to arrays
        if (is_object($results))
            $results = get_object_vars($results);

        $options = array();

        if ($default_text && $default_text != '') {
            $options[''] = $default_text;
        }
        // Will only run if results is an array, not a string, int, etc.
        if (is_array($results)) {
            foreach ($results as $result) {
                if (is_object($result))
                    $result = get_object_vars($result);
                // Get the two rows specified
                $options[$result[$value]] = $result[$key];
            }
        }

        return $options;
    }

}


if (!function_exists('show_tree_view')) {

    function show_tree_view($categories, $current_parent, $current_level = 0, $previous_level = -1) {


        foreach ($categories as $category) {

            if ($current_parent == $category->category_id) {
                if ($current_level > $previous_level)
                    echo " <ul class='widget'> ";

                if ($current_level == $previous_level)
                    echo " </li> ";

                echo '<li><span class="icon-angle-right"></span><a href="' . site_url('articles/view_' . $category->id . '_' . url_title($category->title)) . '">' . str_repeat(" - ", $current_level) . $category->title . '</a></li>';

                if ($current_level > $previous_level) {
                    $previous_level = $current_level;
                }

                $current_level++;

                show_tree_view($categories, $category->id, $current_level, $previous_level);

                $current_level--;
            }
        }

        if ($current_level == $previous_level)
            echo " </li>  </ul> ";
    }

}


if (!function_exists('show_menu_view')) {

    function show_menu_view($categories, $current_parent, $current_level = 0, $previous_level = -1) {


        foreach ($categories as $category) {

            if ($current_parent == $category->category_id) {
                if ($current_level > $previous_level)
                    echo " <ul class='dropdown-menu'> ";

                if ($current_level == $previous_level)
                    echo " </li> ";

                echo '<li><a href="' . site_url('articles/view_' . $category->id . '_' . url_title($category->title)) . '">' . str_repeat(" - ", $current_level) . $category->title . '</a></li>';

                if ($current_level > $previous_level) {
                    $previous_level = $current_level;
                }
            }
        }

        if ($current_level == $previous_level)
            echo " </li>  </ul> ";
    }

}


if (!function_exists('category_ids')) {

    function category_ids(&$category_ids, $categories, $current_parent) {

        $_categories = array_filter($categories, function($category) use ($current_parent) {
            return $category->category_id == $current_parent;
        });

        array_push($category_ids, $current_parent);

        foreach ($_categories as $category) {
            category_ids($category_ids, $categories, $category->id);
        }
    }

}

if (!function_exists('show_user_image')) {

    function show_user_image() {
        return site_url("assets/front/assets/img/user/user.png");
    }

}


if (!function_exists('show_post_image')) {

    function show_post_image($image) {
        if (file_exists(FCPATH . "uploads/thumb/$image")) {
            return site_url('uploads/thumb/' . $image);
        } else {
            return site_url("assets/front/assets/img/blog/document.png");
        }
    }

}