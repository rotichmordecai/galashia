<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<form action="<?php echo site_url('search/index'); ?>">
    <div class="search-bar">
        <div class='input-append'>
            <input id="search" name="search" placeholder="Search..." />
            <button type="submit" class='btn add-on'>
                <span class="icon-search"></span>
            </button>
        </div>
    </div>
</form><!--search bar-->