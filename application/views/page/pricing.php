<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!--=========================================
                               Bread crumb
 =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>




<!--===========================
        Blog
  ==============================-->
<section id="contact">
    <div class="container">
        <div class="pricing-plan-1">
            <h3>Our Plans</h3>
            <div class="pricing-table">
                <div class="pricing-head">
                    <div class="plan-title">Bronze</div>
                    <div class="plan-price"></div>
                </div>
                <!--pricing-head-->
                <div class="pricing-detail">
                    <ul>
                        <li>Initial setup and social media analysis</li>
                        <li>Social media sites management</li>
                        <li>Dialogue engagement and monitoring customer  issues</li>
                        <li>Client content</li>
                        <li>1 week of social media content development</li>
                        <li>2 social media networks</li>
                        <li></li>
                    </ul>
                    <div class="select-plan"> <a href="page/contactus">Contact Us</a> </div>
                </div>
                <!--pricing-detail-->
            </div>
            <!--pricing-table-->
            <div class="pricing-table">
                <div class="pricing-head">
                    <div class="plan-title">Silver</div>
                    <div class="plan-price"></div>
                </div>
                <!--pricing-head-->
                <div class="pricing-detail">
                    <ul>
                        <li>Initial setup and social media analysis</li>
                        <li>Social media sites management</li>
                        <li>Dialogue engagement and monitoring customer  issues</li>
                        <li>Client content</li>
                        <li>2 weeks of social media content development</li>
                        <li>3 social media networks</li>
                        <li>Website/blog</li>
                        <li>Development of brand awareness</li>
                        <li>Reputation management</li>
                        <li>Generation of inbound traffic</li>
                        <li>Cultivation of leads and sales</li>
                        <li></li>
                    </ul>
                    <div class="select-plan"><a href="page/contactus">Contact Us</a></div>
                </div>
                <!--pricing-detail-->
            </div>
            <!--pricing-table-->
            <div class="pricing-table">
                <div class="pricing-head">
                    <div class="plan-title">Gold</div>
                    <div class="plan-price"></div>
                </div>
                <!--pricing-head-->
                <div class="pricing-detail">
                    <ul>
                        <li>Initial setup and social media analysis</li>
                        <li>Social media sites management</li>
                        <li>Dialogue engagement and monitoring customer  issues</li>
                        <li>Client content</li>
                        <li>3 weeks of social media content development</li>
                        <li>4 social media networks</li>
                        <li>Website/blog</li>
                        <li>Development of brand awareness</li>
                        <li>Reputation management</li>
                        <li>Generation of inbound traffic</li>
                        <li>Cultivation of leads and sales</li>
                        <li>Development and Implementation of social media  strategy</li>
                        <li>Monitoring trends in social media</li>
                        <li>Implementation of social media campaigns</li>
                        <li>Management of social media campaigns</li>
                        <li>Social networking analysis</li>
                        <li>Monitoring the internet for brand related topics  of conversation</li>
                        <li>Providing feedback to higher ups</li>
                        <li>Promotion of social media within the  organization</li>
                        <li></li>
                    </ul>
                    <div class="select-plan"><a href="page/contactus">Contact Us</a></div>
                </div>
                <!--pricing-detail-->
            </div>
            <!--pricing-table-->
            <div class="pricing-table"><!--pricing-head--><!--pricing-detail-->
            </div>
            <!--pricing-table-->
        </div>
        <div class="clearfix" ></div>
        <p>&nbsp;</p>
        <p>For any queries and clarifications, kindly call or drop us an email</p>
        <p>&nbsp;</p>    
        <h3>Content development</h3>
        <p>&nbsp;</p>
        <table border="0" cellspacing="0" cellpadding="0" width="" class="table table-bordered">
            <tbody>
                <tr>
                    <td width="139" valign="top"><p><strong>TYPES OF CONTENT</strong></p></td>
                    <td width="492" colspan="4" valign="top"><p><strong>COVERAGE</strong></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Social media articles</strong></p></td>
                    <td width="132" valign="top"><p><em>Topic ideas</em></p></td>
                    <td width="150" valign="top"><p><em>Keyword/topic research</em></p></td>
                    <td width="138" valign="top"><p><em>500 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Blog post </strong></p></td>
                    <td width="132" valign="top"><p><em>Topic ideas</em></p></td>
                    <td width="150" valign="top"><p><em>Keyword/Topic research</em></p></td>
                    <td width="138" valign="top"><p><em>500 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Mini blog posts</strong></p></td>
                    <td width="132" valign="top"><p><em>Topic ideas</em></p></td>
                    <td width="150" valign="top"><p><em>Keyword/Topic research</em></p></td>
                    <td width="138" valign="top"><p><em>300 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Viral articles</strong></p></td>
                    <td width="132" valign="top"><p><em>Topic ideas</em></p></td>
                    <td width="150" valign="top"><p><em>Keyword/Topic research</em></p></td>
                    <td width="138" valign="top"><p><em>300 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Website pages</strong></p></td>
                    <td width="132" valign="top"><p><em>Keyword research</em></p></td>
                    <td width="150" valign="top"><p><em>Optimization and meta tags</em></p></td>
                    <td width="138" valign="top"><p><em>350 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>E-books</strong></p></td>
                    <td width="132" valign="top"><p><em>Topic ideas</em></p></td>
                    <td width="150" valign="top"><p><em>Industry &amp; topic research</em></p></td>
                    <td width="138" valign="top"><p><em>2500 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Landing pages</strong></p></td>
                    <td width="132" valign="top"><p><em>Keyword research</em></p></td>
                    <td width="150" valign="top"><p><em>Optimization and meta tags</em></p></td>
                    <td width="138" valign="top"><p><em>200-300 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisio<a name="_GoBack" id="_GoBack"></a>ns</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Workflow emails</strong></p></td>
                    <td width="132" valign="top"><p><em>Project consulting call</em></p></td>
                    <td width="150" valign="top"><p><em>Email topic ideas</em></p></td>
                    <td width="138" valign="top"><p><em>300-500 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Auto responders</strong></p></td>
                    <td width="132" valign="top"><p><em>Project consulting call</em></p></td>
                    <td width="150" valign="top"><p><em>Email topic ideas</em></p></td>
                    <td width="138" valign="top"><p><em>300-500 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Press releases</strong></p></td>
                    <td width="132" valign="top"><p><em>Topic ideas</em></p></td>
                    <td width="150" valign="top"><p><em>Keyword/Topic research</em></p></td>
                    <td width="138" valign="top"><p><em>300 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Case study</strong></p></td>
                    <td width="132" valign="top"><p><em>Client interview</em></p></td>
                    <td width="150" valign="top"><p><em>Keyword/Topic research</em></p></td>
                    <td width="138" valign="top"><p><em>1000 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Feature length articles</strong></p></td>
                    <td width="132" valign="top"><p><em>Topic ideas</em></p></td>
                    <td width="150" valign="top"><p><em>Keyword/Topic research</em></p></td>
                    <td width="138" valign="top"><p><em>1500 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Sales letters</strong></p></td>
                    <td width="132" valign="top"><p><em>Project consulting call</em></p></td>
                    <td width="150" valign="top"><p><em>Product/Service research</em></p></td>
                    <td width="138" valign="top"><p><em>300 words of copy</em></p></td>
                    <td width="72" valign="top"><p><em>2 revisions</em></p></td>
                </tr>
                <tr>
                    <td width="139" valign="top"><p><strong>Content strategy consulting</strong></p></td>
                    <td width="132" valign="top"><p><em>Content discovery-audit</em></p></td>
                    <td width="150" valign="top"><p><em>Team interviews</em></p></td>
                    <td width="138" valign="top"><p><em>Content recommendations</em></p></td>
                    <td width="72" valign="top"><p><em>Strategy report</em></p></td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>    
        <div class="clearfix" ></div>
    </div>
    <!--container-->
</section>
