<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!--=========================================
                               Bread crumb
 =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>




<!--===========================
        Blog
  ==============================-->   
<section id="blog-single">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8">
              <h3>Who we are</h3>
              <p>&nbsp;</p>
                  <p>Galashia offers professional social media management and writing services for individuals, print media, </p>
                  <p>businesses, companies, corporations and the Web.</p>
                  <p>Our role in your company is to implement the Company's Social Media Strategy, create content, develop brand </p>
                  <p>awareness, and generate inbound traffic and encourage product adoption. We shall coordinate with the internal </p>
                  <p>Marketing and PR teams to support your respective missions, ensuring consistency in voice and cultivating a </p>
              <p>social media referral network through our rich content.</p>
                  <h4>&nbsp;</h4>
                  <h4>What makes us unique</h4>
                  <p>&nbsp;</p>
                  <p>Unlike other companies and individuals, we have experts with broad skills in;</p>
                  <p>• Content strategy and development </p>
                  <p>• Project management skills</p>
                  <p>• In-depth knowledge and understanding of social media platforms</p>
                  <p>• Blogging ecosystem</p>
                  <p>• English/grammar and excellent oral communication skills</p>
                  <p>• Teamwork </p>
                  <p>• Technical understanding and easy to adopt </p>
                  <p>• Communication and relationship building skills </p>
                  <p>• Public relations, branding, sales community management and marketing</p>
              <p>• Social media writing and S.E.O </p>
                  <p>• Research</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
              <div class="clearfix"></div>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="sidebar">
                    <div class="search-bar">
                        <div class='input-append'>
                            <input id="search" placeholder="Search..." />
                            <button class='btn add-on'>
                                <span class="icon-search"></span>
                            </button>
                        </div>
                    </div><!--search bar-->

                    <h5>Categories</h5>

                    <?php show_tree_view($categories, 0); ?>

                </div><!--sidebar-->

            </div>

        </div>
    </div>
</section>
