<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!--=========================================
                               Bread crumb
 =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>




<!--===========================
        Blog
  ==============================-->   
<section id="blog-single">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8">
              <h3>Social media management</h3>
                  <p>Under social media management, we do the following.</p>
              <ul>
                    <li>Development and Implementation of social media  strategy</li>
                    <li>Social media sites management</li>
                    <li>Development of brand awareness</li>
                    <li>Reputation management</li>
                    <li>Generation of inbound traffic </li>
                    <li>Cultivation of leads and sales</li>
                    <li>Dialogue engagement and monitoring customer  issues</li>
                    <li>Monitoring trends in social media </li>
                    <li>Social media content development</li>
                    <li>Implementation of social media campaigns</li>
                    <li>Management of social media campaigns </li>
                    <li>Social networking analysis </li>
                    <li>Monitoring the internet for brand related topics  of conversation</li>
                    <li>Providing feedback to higher ups</li>
                    <li>Promotion of social media within the  organization</li>
                  </ul>
                  <h3>Why us </h3>
                  <ul>
                    <li>Social Media is about people, conversations and  deriving leads and sales from those relationships, it’s important that one has  a well-coordinated social media team like galashia to achieve this goals. </li>
                    <li>Galashia has experts with all the necessary  skills to achieve the defined goals.</li>
                    <li>As two is better than one, a company is better  than an individual because we have a broad range of resources and personnel to deliver </li>
                    <li>We are more reliable as tasks are assigned to  specific experts who are closely monitored by project managers. </li>
                    <li>We always meet the deadlines </li>
                    <li>Our work is quality assured </li>
              </ul>
                  <h3>Benefits</h3>
                  <p>With Galashia on board, be sure to have;</p>
              <ul>
                    <li>A more prominent brand </li>
                    <li>Search engine optimized content </li>
                    <li>Increased customer loyalty</li>
                    <li>Lead generation</li>
                    <li>Marketing </li>
                    <li>Increased quality engagement</li>
                    <li>Growth of email database</li>
                    <li>Sign up users or customer for a new product or  service launch</li>
                    <li>Profit generation </li>
                    <li>Improved customer service</li>
                    <li>An increase in the amount of traffic on your  website/blog</li>
                    <li>More followers and likes  </li>
                    <li>Increase in sales </li>
                    <li>Increased visibility and value in the eyes of  customers</li>
                    <li>Authenticity and transparency </li>
                    <li>Happier customers, </li>
                  </ul>
                  <h3>Content development (why out-source) </h3>
                  <p>(www.contentlaunch.com)</p>
                  <h3>Statistics </h3>
                  <ul>
                    <li>Content marketing costs 62%  less than traditional marketing and generates about 3 times as many leads. (<a href="http://www.demandmetric.com/content/content-marketing-infographic" target="_blank" title="Demand Metric">Demand Metric</a>)</li>
                    <li>62% of companies outsource  content marketing. (<a href="http://www.inboundwriter.com/content-marketing/infographic-the-content-marketing-explosion/" target="_blank" title="Mashable and Inbound Writer">Mashable &amp; Inbound Writer</a>)</li>
                    <li>72% of large companies and  33% of small companies use a combination of in-house and outsourced resources  for content creation. (<a href="http://contentmarketinginstitute.com/wp-content/uploads/2013/10/B2B_Research_2014_CMI.pdf" target="_blank" title="Content Marketing Institute">Content Marketing Institute</a>)</li>
                    <li>When B2B marketers outsource,  64% outsource writing, 54% outsource design, 30% outsource distribution, and  22% outsource editing, (<a href="http://contentmarketinginstitute.com/wp-content/uploads/2013/10/B2B_Research_2014_CMI.pdf" target="_blank">Content Marketing Institute</a>)</li>
              </ul>
                  <h3>Services </h3>
                  <ul>
                    <li>Content strategy </li>
                    <li>Content marketing </li>
                    <li>Content writing </li>
              </ul>
                  <h3>Types of content </h3>
                  <ul>
                    <li>Social media articles </li>
                    <li>Blog posts</li>
                    <li>E-books</li>
                    <li>News paper articles </li>
                    <li>Magazine content </li>
                    <li>case studies</li>
                    <li>landing pages</li>
                    <li>workflow emails</li>
                    <li>info graphics</li>
                    <li>video scripts</li>
                    <li>presentation content </li>
                    <li>site content</li>
                    <li>press releases</li>
              </ul>
                  <h3>Why us </h3>
                  <p>Content, it’s what powers the connection you have with your  target market. Integrated with an inbound marketing program and it's a dynamic  force. But who's supposed to create all of this content? Your marketing team is  too busy and doesn't have the writing expertise. You work hard on strategizing  and running your marketing programs and outsource the content writing.<br>
              A crowd sourced content provider is not the answer. The  quality simply isn't there. Freelancers are unreliable and can't write for  every industry.</p>
                  <h3>Why galashia?</h3>
                  <ul>
                    <li>We develop high quality, search engine  optimized, sharable content that converts</li>
                    <li>We work with expert content writers </li>
                    <li>We can write for every industry  </li>
                    <li>No crowdsourced, offshore freelancers – only  dedicated content experts</li>
                    <li>Affordable pricing (includes all research, meta  data writing &amp; revisions)</li>
                    <li>We assist with content strategy, planning and  distribution </li>
                    <li>Quality is guaranteed </li>
                    <li>We can deliver content rapidly if you need it  urgent </li>
                    <li>We can handle an extensive list in a short time </li>
                    <li>We are always available to deliver </li>
                    <li>We are cost effective </li>
                    <li>We always meet our deadlines </li>
                    <li>We’re passionate about content marketing and  want to help your brand or agency shine!</li>
              </ul>
                  <h3>Benefits</h3>
                  <p>With Galashia on board, be sure to have;</p>
              <ul>
                    <li>A more prominent brand </li>
                    <li>Search engine optimized content </li>
                    <li>Increased customer loyalty</li>
                    <li>Lead generation</li>
                    <li>Marketing </li>
                    <li>Increased quality engagement</li>
                    <li>Growth of email database</li>
                    <li>Sign up users or customer for a new product or  service launch</li>
                    <li>Profit generation </li>
                    <li>Improved customer service</li>
                    <li>An increase in the amount of traffic on your  website/blog</li>
                    <li>More followers and likes  </li>
                    <li>Increase in sales </li>
                    <li>Increased visibility and value in the eyes of  customers</li>
                    <li>Authenticity and transparency </li>
                    <li>Happier customers,              </li>
              </ul>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
<div class="clearfix"></div>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="sidebar">
                    <div class="search-bar">
                        <div class='input-append'>
                            <input id="search" placeholder="Search..." />
                            <button class='btn add-on'>
                                <span class="icon-search"></span>
                            </button>
                        </div>
                    </div><!--search bar-->

                    <h5>Categories</h5>

                    <?php show_tree_view($categories, 0); ?>

                </div><!--sidebar-->

            </div>

        </div>
    </div>
</section>
