<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!--=========================================
                               Bread crumb
 =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>



<!--===========================
       contact
 ==============================-->   
<section id="contact">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="contact-map" data-address="Nairobi" data-zoomlvl="13" data-maptype="HYBRID"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="contact_form">
            <form class="contact_form">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
                        <label for="name_contact">Name<span>*</span></label>
                        <input id="name_contact" name="name_contact" type="text" />

                        <label for="email_contact">Email<span>*</span></label>
                        <input id="email_contact" name="email_contact" type="text" />

                        <label for="subj_contact">Subject<span>*</span></label>
                        <input id="subj_contact" name="subj_contact" type="text" />
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">    
                        <label for="msg_contact">Your Message<span>*</span></label>
                        <textarea id="msg_contact" name="msg_contact" type="text"></textarea>
                        <button class="btn btn-default" type="submit">SEND MESSAGE</button>
                    </div>
                </div>       	
            </form><!--contact form-->

        </div><!--contact_form-->

    </div><!--container-->


</section> <!--contact-->