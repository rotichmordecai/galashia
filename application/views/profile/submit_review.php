<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Your review & rating for <?php echo $user->user_name; ?></h4>
        </div>
        <div class="modal-body">
            <form id="submit_review" class="form-horizontal" action="<?php echo site_url('profile/action_submit_review'); ?>">
                <fieldset>

                    <div id="message"></div>

                    <input type="hidden" id="recipient_id" name="recipient_id" value="<?php echo $user->id; ?>" />

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="">Your rating</label>  
                        <div class="col-md-8">
                            <input type="number" name="user_rating" id="user_rating" class="rating" data-clearable="remove"/>
                        </div>
                    </div>

                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textarea">Your review</label>
                        <div class="col-md-8">                     
                            <textarea class="form-control" rows="3" id="user_review" name="user_review" placeholder="Write your message here ..."></textarea>
                        </div>
                    </div>

                </fieldset>
            </form>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 0px; margin-top: 0px;">Close</button>
            <button id="button_submit_reviews" type="button" class="btn btn-primary" style="margin-bottom: 0px; margin-top: 0px;">Save changes</button>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<script type="text/javascript">

    // prepare the form when the DOM is ready 
    $(document).ready(function () {

        $('input.rating').rating();
        $("#button_submit_reviews").click(function () {

            var recipient_id = $("input#recipient_id").val();
            var user_rating = $("input#user_rating").val();
            var user_review = $("textarea#user_review").val();
            if (user_rating != '' && user_review != '') {

                var action = $("form#submit_review").attr('action')

                var request = $.ajax({
                    url: action,
                    type: "POST",
                    data: {recipient_id: recipient_id, user_rating: user_rating, user_review: user_review},
                    dataType: "json"
                });
                request.done(function (json) {
                    if (json.success == 'FALSE') {
                        var html = " <div class=\"alert alert-success\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" + json.response + "</div> ";
                        $("div#message").html(html);
                    } else {
                        var html = " <div class=\"alert alert-warning\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" + json.response + "</div> ";
                        $("div#message").html(html);
                    }

                    $("textarea#user_review").val('');

                    setTimeout(function () {
                        $('.modal').modal('hide');
                    }, 3000);

                });
                request.fail(function (jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });
            }
        });
    });

</script>
