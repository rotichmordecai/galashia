<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php
if (!isset($user_profile->facebook)) {
    $user_profile->facebook = 'https://www.facebook.com/username';
}

if (!isset($user_profile->twitter)) {
    $user_profile->twitter = 'https://twitter.com/username';
}

if (!isset($user_profile->linkedin)) {
    $user_profile->linkedin = 'https://www.linkedin.com/in/username';
}

if (!isset($user_profile->googleplus)) {
    $user_profile->googleplus = 'https://plus.google.com/+username';
}
?>

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>

<section id="shortcode">
    <div class="container">

        <div class="">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div id="action_images">

                        <div id="actions" class="row">

                            <div class="col-lg-7">
                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Add files...</span>
                                </span>
                                <button type="submit" class="btn btn-primary start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Start upload</span>
                                </button>
                                <button type="reset" class="btn btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Cancel upload</span>
                                </button>
                            </div>

                            <div class="col-lg-5">
                                <!-- The global file processing state -->
                                <span class="fileupload-process">
                                    <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                    </div>
                                </span>
                            </div>

                        </div>


                        <div class="table table-striped" class="files" id="previews">

                            <div id="template" class="file-row">
                                <!-- This is used as the file preview template -->
                                <div>
                                    <span class="preview"><img data-dz-thumbnail /></span>
                                </div>
                                <div>
                                    <p class="name" data-dz-name></p>
                                    <strong class="error text-danger" data-dz-errormessage></strong>
                                </div>
                                <div>
                                    <p class="size" data-dz-size></p>
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                    </div>
                                </div>
                                <div>
                                    <button class="btn btn-primary start">
                                        <i class="glyphicon glyphicon-upload"></i>
                                        <span>Start</span>
                                    </button>
                                    <button data-dz-remove class="btn btn-warning cancel">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                        <span>Cancel</span>
                                    </button>
                                    <button data-dz-remove class="btn btn-danger delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Delete</span>
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>

                    <?php
                    $attributes = array('class' => 'contact_form', 'id' => 'article-form', 'role' => 'form');
                    echo form_open_multipart(current_url(), $attributes);
                    ?>
                    <div class="form-group <?php echo strlen(form_error('user_name')) > 0 ? 'has-error' : '' ?>">
                        <label>Names</label>
                        <input class="form-control" id="user_name" name="user_name" value="<?php echo set_value('user_name', $user_profile->user_name); ?>"  placeholder="<?php echo set_value('user_name'); ?>">
                        <p class="help-block"><?php echo form_error('user_name'); ?></p>
                    </div>

                    <div class="form-group <?php echo strlen(form_error('country')) > 0 ? 'has-error' : '' ?>">
                        <label>Country or Address</label>
                        <input class="form-control" id="country" name="country" value="<?php echo set_value('country', $user_profile->country); ?>"  placeholder="<?php echo set_value('country'); ?>">
                        <p class="help-block"><?php echo form_error('country'); ?></p>
                    </div>

                    <div class="form-group <?php echo strlen(form_error('website')) > 0 ? 'has-error' : '' ?>">
                        <label>Website</label>
                        <input class="form-control" id="website" name="website" value="<?php echo set_value('website', $user_profile->website); ?>"  placeholder="<?php echo set_value('website'); ?>">
                        <p class="help-block"><?php echo form_error('website'); ?></p>
                    </div>

                    <div class="form-group <?php echo strlen(form_error('facebook')) > 0 ? 'has-error' : '' ?>">
                        <label>Facebook profile</label>
                        <input class="form-control" id="facebook" name="facebook" value="<?php echo set_value('facebook', $user_profile->facebook); ?>"  placeholder="<?php echo set_value('facebook'); ?>">
                        <p class="help-block"><?php echo form_error('facebook'); ?></p>
                    </div>

                    <div class="form-group <?php echo strlen(form_error('twitter')) > 0 ? 'has-error' : '' ?>">
                        <label>Twitter profile</label>
                        <input class="form-control" id="twitter" name="twitter" value="<?php echo set_value('twitter', $user_profile->twitter); ?>"  placeholder="<?php echo set_value('twitter'); ?>">
                        <p class="help-block"><?php echo form_error('twitter'); ?></p>
                    </div>

                    <div class="form-group <?php echo strlen(form_error('googleplus')) > 0 ? 'has-error' : '' ?>">
                        <label>Google plus profile</label>
                        <input class="form-control" id="googleplus" name="googleplus" value="<?php echo set_value('googleplus', $user_profile->googleplus); ?>"  placeholder="<?php echo set_value('googleplus'); ?>">
                        <p class="help-block"><?php echo form_error('googleplus'); ?></p>
                    </div>

                    <div class="form-group <?php echo strlen(form_error('linkedin')) > 0 ? 'has-error' : '' ?>">
                        <label>Linkedin profile</label>
                        <input class="form-control" id="linkedin" name="linkedin" value="<?php echo set_value('linkedin', $user_profile->linkedin); ?>"  placeholder="<?php echo set_value('linkedin'); ?>">
                        <p class="help-block"><?php echo form_error('linkedin'); ?></p>
                    </div>

                    <div class="form-group <?php echo strlen(form_error('user_bio')) > 0 ? 'has-error' : '' ?>">
                        <label>Bio</label>
                        <textarea class="form-control" id="user_bio" name="user_bio" rows="5"><?php echo set_value('user_bio', htmlspecialchars_decode(html_entity_decode($user_profile->user_bio))); ?></textarea>
                        <p class="help-block"><?php echo form_error('user_bio'); ?></p>
                    </div>

                    <div id="grammer_response">
                    </div>

                    <button type="submit" class="btn btn-default">Update Profile</button>

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!--contact_form-->
    </div><!--container-->
</section> <!--contact-->
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js"></script>
<script type="text/javascript">
    var input = document.getElementById('country');
    var options = {};
    new google.maps.places.Autocomplete(input, options);
</script>

<script type="text/javascript">
    $(document).ready(function () {

        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone("div#action_images", {// Make the whole body a dropzone
            url: "<?php echo site_url('profile/upload_image'); ?>", // Set the url
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            acceptedFiles: ".jpg,.jpeg,.png,.gif",
            dictInvalidFileType: "Some files are not added, accepted file types are jpg/jpeg, png and gif",
            paramName: "image",
            previewTemplate: previewTemplate,
            autoQueue: false,
            previewsContainer: "#previews", // Define the container to display the previews
            clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
        });

        myDropzone.on("sending", function (file, xhr, formData) {
            formData.append("active_tab", 'images');
            $("#property_image_result_container").hide();
        });

        myDropzone.on("success", function (file, response) {
            var obj = $.parseJSON(response);
            $(file.previewTemplate).find('.delete').data('imageid', obj.images[0].image_id);
            $(file.previewTemplate).on('click', 'button.delete', delete_button_func);
        });

        myDropzone.on("addedfile", function (file) {
            file.previewElement.querySelector(".start").onclick = function () {
                myDropzone.enqueueFile(file);
            };
        });

        myDropzone.on("error", function (file, errorMessage, xhr) {
            myDropzone.removeFile(file);
            $('#property_image_result').text(errorMessage)
            $('#property_image_result_container').addClass("alert-error").removeClass('alert-success');
            $('#property_image_result_container').show();
        });

        myDropzone.on("totaluploadprogress", function (progress) {
            document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
        });

        myDropzone.on("sending", function (file) {
            document.querySelector("#total-progress").style.opacity = "1";
            file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
        });

        myDropzone.on("queuecomplete", function (progress) {
            document.querySelector("#total-progress").style.opacity = "0";
        });

        document.querySelector("#actions .start").onclick = function () {
            myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
            $("#property_image_result_container").hide();
        };

        $('.file-row').on('click', 'button.delete', delete_button_func);

        function delete_button_func() {

            var image_id = $(this).data("image_id");
            var image_image = $(this).data("image_image");
            var button = $(this);
            //remove text in form result div
            $("#property_image_result").html("");
            $("#property_image_result_container").hide();

            bootbox.confirm("Image will be permanently deleted and cannot be recovered. Are you sure?", function (result) {
                if (result === true) {
                    ajax_delete_property_image(image_id, image_image, button);
                }
            });
        }
        ;


    });
</script>
