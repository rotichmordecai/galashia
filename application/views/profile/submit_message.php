<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Send message to <?php echo $user->user_name; ?></h4>
        </div>
        <div class="modal-body">
            <form id="submit_message" class="form-horizontal" action="<?php echo site_url('profile/action_submit_message'); ?>">
                <fieldset>

                    <div id="message"></div>

                    <input type="hidden" id="recipient_id" name="recipient_id" value="<?php echo $user->id; ?>" />

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Message title</label>  
                        <div class="col-md-8">
                            <input id="message_title" name="message_title" type="text" placeholder="Message title" class="form-control input-md"> 
                        </div>
                    </div>

                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textarea">Your message</label>
                        <div class="col-md-8">                     
                            <textarea class="form-control" id="message_text" name="message_text" placeholder="Write your message here ..."></textarea>
                        </div>
                    </div>

                </fieldset>
            </form>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 0px; margin-top: 0px;">Close</button>
            <button id="button_submit_message"  type="button" class="btn btn-primary" style="margin-bottom: 0px; margin-top: 0px;">Send message</button>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<script type="text/javascript">

    $(document).ready(function () {

        $('input.rating').rating();

        $("#button_submit_message").click(function () {

            var recipient_id = $("input#recipient_id").val();
            var message_title = $("input#message_title").val();
            var message_text = $("textarea#message_text").val();

            if (message_title != '' && message_text != '') {

                var action = $("form#submit_message").attr('action')

                var request = $.ajax({
                    url: action,
                    type: "POST",
                    data: {recipient_id: recipient_id, message_title: message_title, message_text: message_text},
                    dataType: "json"
                });

                request.done(function (json) {

                    if (json.success == 'FALSE') {
                        var html = " <div class=\"alert alert-success\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" + json.response + "</div> ";
                        $("div#message").html(html);
                    } else {
                        var html = " <div class=\"alert alert-warning\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" + json.response + "</div> ";
                        $("div#message").html(html);
                    }

                    $("input#message_title").val('');
                    $("textarea#message_text").val('');

                    setTimeout(function () {
                        $('.modal').modal('hide');
                    }, 3000);


                });

                request.fail(function (jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });
            }
        });
    });

</script>


