<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Follow <?php echo $user->user_name; ?></h4>
        </div>
        <div class="modal-body">
            <form id="submit_message" class="form-horizontal" action="<?php echo site_url('profile/action_follow_user'); ?>">
                <div id="message"></div>
                <fieldset>                    
                    <h3>
                        Do you wish to follow <?php echo $user->user_name; ?>?
                        <input type="hidden" id="follow_id" name="follow_id" value="<?php echo $user->id; ?>" />
                    </h3>
                </fieldset>
            </form>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 0px; margin-top: 0px;">Close</button>
            <button id="button_submit_message"  type="button" class="btn btn-primary" style="margin-bottom: 0px; margin-top: 0px;"><span class="fa fa-plus-circle"> Follow</button>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<script type="text/javascript">

    $(document).ready(function () {

        $('input.rating').rating();

        $("#button_submit_message").click(function () {

            var follow_id = $("input#follow_id").val();

            if (follow_id != '') {

                var action = $("form#submit_message").attr('action')

                var request = $.ajax({
                    url: action,
                    type: "POST",
                    data: {follow_id: follow_id},
                    dataType: "json"
                });

                request.done(function (json) {

                    if (json.success == 'FALSE') {
                        var html = " <div class=\"alert alert-success\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" + json.response + "</div> ";
                        $("div#message").html(html);
                    } else {
                        var html = " <div class=\"alert alert-warning\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" + json.response + "</div> ";
                        $("div#message").html(html);
                    }

                    setTimeout(function () {
                        $('.modal').modal('hide');
                        location.reload();
                    }, 3000);

                });

                request.fail(function (jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });
            }
        });
    });

</script>


