<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php
echo form_hidden('username', $username);
?>

<!--=========================================
                                Bread crumb
  =========================================-->   

<style type="text/css">

    @import url(//fonts.googleapis.com/css?family=Lato:400,900);
    @import url(//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);

    .social {
        list-style: none;
        padding: 0px;
        margin-top: 25px;
        text-align: center;
    }

    .social a {
        position: relative;
        display: inline-block;
        min-width: 40px;
        padding: 10px 0px;
        margin: 0px 5px;
        overflow: hidden;
        text-align: center;
        background-color: rgb(215, 215, 215);
        border-radius: 40px;
    }

    a:hover {
        text-decoration: none;    
    }

    a:valid {
        text-decoration: none;    
    }

    a:active {
        text-decoration: none;    
    }

    a:visited {
        text-decoration: none;    
    }

    a.social-icon {
        text-decoration: none !important;
        box-shadow: 0px 0px 1px rgb(51, 51, 51);
        box-shadow: 0px 0px 1px rgba(51, 51, 51, 0.7);
    }
    a.social-icon:hover {
        color: rgb(255, 255, 255) !important;
    }
    a.facebook {
        color: rgb(59, 90, 154) !important;
    }
    a.facebook:hover {		
        background-color: rgb(59, 90, 154) !important;
    }
    a.twitter {
        color: rgb(45, 168, 225) !important;
    }
    a.twitter:hover {
        background-color: rgb(45, 168, 225) !important;
    }
    a.github {
        color: rgb(51, 51, 51) !important;
    }
    a.github:hover {
        background-color: rgb(51, 51, 51) !important;
    }
    a.google-plus {
        color: rgb(244, 94, 75) !important;
    }
    a.google-plus:hover {
        background-color: rgb(244, 94, 75) !important;
    }
    a.linkedin {
        color: rgb(1, 116, 179) !important;
    }
    a.linkedin:hover {
        background-color: rgb(1, 116, 179) !important;
    }

    .portfolio img:hover {
        cursor: pointer;
        -webkit-transform: scale(1.4, 1.4);
        -ms-transform: scale(1.4, 1.4);
        transform: scale(1.4, 1.4);
        transition-duration: 0.5s;
        -webkit-transition-duration: 0.5s; /* Safari */
        z-index: 1;
    }

    .timeline {
        list-style: none;
        padding: 20px 0 20px;
        position: relative;
    }

    .timeline > li {
        margin-bottom: 20px;
        position: relative;
    }

    .timeline > li:before,
    .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li:after {
        clear: both;
    }

    .timeline > li:before,
    .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li:after {
        clear: both;
    }

    .timeline > li > .timeline-panel {
        width: 86%;
        float: left;
        border: 1px solid #d4d4d4;
        border-radius: 2px;
        padding: 20px;
        position: relative;
        -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline > li > .timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -15px;
        display: inline-block;
        border-top: 15px solid transparent;
        border-left: 15px solid #ccc;
        border-right: 0 solid #ccc;
        border-bottom: 15px solid transparent;
        content: " ";
    }

    .timeline > li > .timeline-panel:after {
        position: absolute;
        top: 27px;
        right: -14px;
        display: inline-block;
        border-top: 14px solid transparent;
        border-left: 14px solid #fff;
        border-right: 0 solid #fff;
        border-bottom: 14px solid transparent;
        content: " ";
    }

    .timeline > li > .timeline-badge {
        color: #fff;
        width: 50px;
        height: 50px;
        line-height: 50px;
        font-size: 1.4em;
        text-align: center;
        position: absolute;
        top: 16px;
        left: 95%;
        margin-left: -25px;
        background-color: #999999;
        z-index: 100;
        border-top-right-radius: 50%;
        border-top-left-radius: 50%;
        border-bottom-right-radius: 50%;
        border-bottom-left-radius: 50%;
    }

    .timeline > li.timeline-inverted > .timeline-badge { 
        left: 5%;
    }


    .timeline > li.timeline-inverted > .timeline-panel {
        float: right;
    }

    .timeline > li.timeline-inverted > .timeline-panel:before {
        border-left-width: 0;
        border-right-width: 15px;
        left: -15px;
        right: auto;
    }

    .timeline > li.timeline-inverted > .timeline-panel:after {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }

    .timeline-badge.primary {
        background-color: #2e6da4 !important;
    }

    .timeline-badge.success {
        background-color: #3f903f !important;
    }

    .timeline-badge.warning {
        background-color: #f0ad4e !important;
    }

    .timeline-badge.danger {
        background-color: #d9534f !important;
    }

    .timeline-badge.info {
        background-color: #5bc0de !important;
    }

    .timeline-title {
        margin-top: 0;
        color: inherit;
    }

    .timeline-body > p,
    .timeline-body > ul {
        margin-bottom: 0;
    }

    .timeline-body > p + p {
        margin-top: 5px;
    }

    @media (max-width: 767px) {
        ul.timeline:before {
            left: 40px;
        }

        ul.timeline > li > .timeline-panel {
            width: calc(100% - 90px);
            width: -moz-calc(100% - 90px);
            width: -webkit-calc(100% - 90px);
        }

        ul.timeline > li > .timeline-badge {
            left: 15px;
            margin-left: 0;
            top: 16px;
        }

        ul.timeline > li > .timeline-panel {
            float: right;
        }

        ul.timeline > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
    }

    .gallery
    {
        display: inline-block;
        margin-top: 20px;
    }

    .gallery img {
        filter: gray; /* IE6-9 */
        -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
        -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
        box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
        margin-bottom:10px;
    }

    .gallery img:hover {
        filter: none; /* IE6-9 */
        -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */

    }

    .gallery .thumbnail
    {
        display: inline-block;
        margin-top: 20px;
    }

    #user-map  {
        margin: 0;
        padding: 0;
        height: 100%;
    }

    #user-map {
        width: 100%;
        height:400px;
    }

</style>

<script type="text/javascript">

    galashia.controller('ProfileController', function ($scope) {
        $scope.profile = {
            show_error: false,
            error_message: 'hahaha'
        };
    });

</script>





<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>

<!-- Modal -->
<div class="modal fade" id="photoModal" tabindex="-1" role="dialog" aria-labelledby="photoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </p>
            </div>
            <div class="modal-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="userMapModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Location - <?php echo $profile->country; ?></h4>

            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div id="user-map" data-address="<?php echo $profile->country; ?>" data-zoomlvl="13" data-maptype="HYBRID"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="followModal" tabindex="-1" role="dialog" aria-labelledby="followModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Loading &hellip;</p>
            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="reviewModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Loading &hellip;</p>
            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Loading &hellip;</p>
            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<section ng-controller="ProfileController" id="blog-medium">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8">

                <div class="row-fluid user-infos cyruxx">
                    <div class="span10 offset1">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Profile - <?php echo $profile->user_name; ?> </h3>
                            </div>
                            <div class="panel-body">
                                <div class="row-fluid">
                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                        <br />
                                        <img class="img-rounded" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=150" alt="User Pic">
                                        <br />
                                        <br />
                                        <br />
                                        <figcaption class="ratings">
                                            <p>Ratings
                                                <a href="#">
                                                    <span class="fa fa-star"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="fa fa-star"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="fa fa-star"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="fa fa-star"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="fa fa-star-o"></span>
                                                </a> 
                                            </p>
                                        </figcaption>
                                    </div>
                                    <div class="col-lg-9  col-md-9 col-sm-9"> 
                                        <br />
                                        <blockquote>
                                            <p><?php echo $profile->user_name; ?></p>
                                            <a href="#userMapModal" class="" data-toggle="modal">
                                                <small><cite title="Source Title"><?php echo $profile->country; ?>  <i class="icon-map-marker"></i></cite></small>
                                            </a>                                            
                                        </blockquote>
                                        <div class="social">
                                            <a href="<?php echo $profile->facebook; ?>" target="_blank" class="[ social-icon facebook ] animate"><span class="fa fa-facebook"></span></a>

                                            <a href="<?php echo $profile->twitter; ?>" target="_blank" class="[ social-icon twitter ] animate"><span class="fa fa-twitter"></span></a>

                                            <a href="<?php echo $profile->googleplus; ?>" target="_blank" class="[ social-icon google-plus ] animate"><span class="fa fa-google-plus"></span></a>

                                            <a href="<?php echo $profile->linkedin; ?>" target="_blank" class="[ social-icon linkedin ] animate"><span class="fa fa-linkedin"></span></a>
                                        </div>
                                        <table class="table table-condensed table-responsive table-user-information">
                                            <tbody>
                                                <tr>
                                                    <td>User level:</td>
                                                    <td><?php echo $profile->is_admin == 1 ? 'Administrator' : 'Content developer'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Member since:</td>
                                                    <td><?php echo $profile->created; ?></td>
                                                </tr>  
                                                <tr>
                                                    <td>Last online:</td>
                                                    <td><?php echo $profile->modified; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div>
                                    <div class="accordion" id="accordion2">

                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseBio">
                                                    <span class="label label-info" style="margin-left: 15px;">Show Bio</span> 
                                                </a>
                                            </div>
                                            <div id="collapseBio" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <p>&nbsp;</p>
                                                    <?php echo html_entity_decode($profile->user_bio); ?>                                                    
                                                </div>
                                            </div>                                            
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button class="btn  btn-primary" type="button" data-toggle="tooltip" data-original-title="Send message to user">
                                    <i class="icon-envelope icon-white"></i>
                                </button>
                                <span class="pull-right">                                    

                                    <span class="label label-info">888 photos</span> 
                                    <span class="label label-warning">150 followers</span>
                                    <button class="btn btn-warning" type="button" data-toggle="tooltip" data-original-title="Edit this user">
                                        <i class="icon-edit icon-white"></i>
                                    </button>
                                    <button class="btn btn-danger" type="button" data-toggle="tooltip" data-original-title="Remove this user">
                                        <i class="icon-remove icon-white"></i>
                                    </button>                                    
                                </span>
                            </div>
                            <div>
                                <div class="col-xs-12 col-sm-4">
                                    <h2><strong> 20,7K </strong></h2>                    
                                    <p><small>Followers</small></p>
                                    <a class="btn btn-success btn-block" data-toggle="modal" href="<?php echo site_url('profile/submit_follow/' . $profile->username); ?>" data-target="#followModal">
                                        <span class="fa fa-plus-circle"></span> Follow 
                                    </a>
                                </div><!--/col-->
                                <div class="col-xs-12 col-sm-4">
                                    <h2><strong>245</strong></h2>                    
                                    <p><small>Following</small></p>
                                    <a class="btn btn-info btn-block" data-toggle="modal" href="<?php echo site_url('profile/submit_review/' . $profile->username); ?>" data-target="#reviewModal">
                                        <span class="fa fa-comment"></span> Submit Review 
                                    </a>                                    
                                </div><!--/col-->
                                <div class="col-xs-12 col-sm-4">
                                    <h2><strong>43</strong></h2>                    
                                    <p><small>Snippets</small></p>
                                    <a class="btn btn-primary btn-block" data-toggle="modal" href="<?php echo site_url('profile/submit_message/' . $profile->username); ?>" data-target="#messageModal">
                                        <span class="fa fa-envelope"></span> Send Message 
                                    </a>
                                </div><!--/col-->
                            </div>
                        </div>
                    </div>
                </div>   

                <h3>My Articles</h3>
                <div class="tex-widget">
                    <p>Find a list of my recent articles below:</p>
                </div>             

                <ul class="nav nav-tabs three" id="ornatTabs">
                    <li class="active"><a href="#timeline">Timeline</a></li>
                    <li class=""><a href="#articles">Articles</a></li>
                    <li class=""><a href="#photos">Photos</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="timeline">
                        <div class="container">
                            <ul class="timeline">

                                <li class="timeline-inverted">
                                    <div class="timeline-badge info"><i class="glyphicon glyphicon-hand-right"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Bootstrap released</h4>
                                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> August 2011</small></p>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
                                                Quisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis 
                                                dolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. 
                                                Aliquam in felis sit amet augue.</p>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="timeline-badge danger"><i class="glyphicon glyphicon-eye-open"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Left Event</h4>
                                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 3 years ago</small></p>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Add more progress events and milestones to the left or right side of the timeline. Each event can be tagged with a date and given a beautiful icon to symbolize it's spectacular meaning.</p>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="timeline-badge default"><i class="glyphicon glyphicon-home"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Left Event</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Add more progress events and milestones to the left or right side of the timeline. Each event can be tagged with a date and given a beautiful icon to symbolize it's spectacular meaning.</p>

                                            <hr>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                    <i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li id="ajaxloader" style="display:none;">
                                <center>
                                    <img src="<?php echo site_url('assets/front/assets/img/ajax-loader.gif'); ?>" />
                                </center>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="tab-pane fade in" id="articles">

                        <?php if (is_array($results)): ?>
                            <?php foreach ($results as $result): ?>

                                <div class="latest-post" style="clear: both;">
                                    <img src="<?php echo show_post_image($result->image); ?>" alt=""/>
                                    <div class="detail">
                                        <a href="<?php echo site_url('article/view/' . $result->id . '_' . url_title($result->title)); ?>"><?php echo $result->title; ?></a>
                                        <div class="date-stamp"><?php echo date("M jS, Y", strtotime($result->updated)); ?></div>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>

                    <div class="tab-pane fade in" id="photos">

                        <div class="container">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a href="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" rel="group" class="thumbnail" data-toggle="modal" data-remote-image="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" data-target="#photoModal">
                                            <img src="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_6.jpg" width="306" height="306" id="629883748977189610_34341255" alt="Mount Snow Skiing/Snowboarding Picture" title="Mount Snow" data-latitude="42.964855128" data-longitude="-72.892956734">
                                            <div class='text-right'>
                                                <small class='text-muted'>5 days ago</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a href="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" rel="group" class="thumbnail" data-toggle="modal" data-remote-image="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" data-target="#photoModal">
                                            <img src="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_6.jpg" width="306" height="306" id="629883748977189610_34341255" alt="Mount Snow Skiing/Snowboarding Picture" title="Mount Snow" data-latitude="42.964855128" data-longitude="-72.892956734">
                                            <div class='text-right'>
                                                <small class='text-muted'>5 days ago</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a href="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" rel="group" class="thumbnail" data-toggle="modal" data-remote-image="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" data-target="#photoModal">
                                            <img src="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_6.jpg" width="306" height="306" id="629883748977189610_34341255" alt="Mount Snow Skiing/Snowboarding Picture" title="Mount Snow" data-latitude="42.964855128" data-longitude="-72.892956734">
                                            <div class='text-right'>
                                                <small class='text-muted'>5 days ago</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a href="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" rel="group" class="thumbnail" data-toggle="modal" data-remote-image="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" data-target="#photoModal">
                                            <img src="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_6.jpg" width="306" height="306" id="629883748977189610_34341255" alt="Mount Snow Skiing/Snowboarding Picture" title="Mount Snow" data-latitude="42.964855128" data-longitude="-72.892956734">
                                            <div class='text-right'>
                                                <small class='text-muted'>5 days ago</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a href="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" rel="group" class="thumbnail" data-toggle="modal" data-remote-image="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" data-target="#photoModal">
                                            <img src="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_6.jpg" width="306" height="306" id="629883748977189610_34341255" alt="Mount Snow Skiing/Snowboarding Picture" title="Mount Snow" data-latitude="42.964855128" data-longitude="-72.892956734">
                                            <div class='text-right'>
                                                <small class='text-muted'>5 days ago</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a href="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" rel="group" class="thumbnail" data-toggle="modal" data-remote-image="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" data-target="#photoModal">
                                            <img src="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_6.jpg" width="306" height="306" id="629883748977189610_34341255" alt="Mount Snow Skiing/Snowboarding Picture" title="Mount Snow" data-latitude="42.964855128" data-longitude="-72.892956734">
                                            <div class='text-right'>
                                                <small class='text-muted'>5 days ago</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a href="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" rel="group" class="thumbnail" data-toggle="modal" data-remote-image="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" data-target="#photoModal">
                                            <img src="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_6.jpg" width="306" height="306" id="629883748977189610_34341255" alt="Mount Snow Skiing/Snowboarding Picture" title="Mount Snow" data-latitude="42.964855128" data-longitude="-72.892956734">
                                            <div class='text-right'>
                                                <small class='text-muted'>5 days ago</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a href="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" rel="group" class="thumbnail" data-toggle="modal" data-remote-image="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_8.jpg" data-target="#photoModal">
                                            <img src="http://distilleryimage10.s3.amazonaws.com/49cd80a2798011e3839612581855eb4e_6.jpg" width="306" height="306" id="629883748977189610_34341255" alt="Mount Snow Skiing/Snowboarding Picture" title="Mount Snow" data-latitude="42.964855128" data-longitude="-72.892956734">
                                            <div class='text-right'>
                                                <small class='text-muted'>5 days ago</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                </div> <!-- list-group / end -->
                            </div> <!-- row / end -->
                        </div> <!-- container / end -->

                    </div>

                </div>    

                <h3>My Projects</h3>

                <div class="row portfolio">
                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+" alt="..." class="img-rounded">
                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+" alt="..." class="img-rounded">
                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+" alt="..." class="img-rounded">
                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+" alt="..." class="img-rounded">
                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+" alt="..." class="img-rounded">
                </div>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="sidebar">
                    <?php $this->load->view('partial/search_form'); ?>
                    <h5>Categories</h5>
                    <?php show_tree_view($categories, 0); ?>
                </div><!--sidebar-->

            </div>

        </div>
    </div>
</section>

<script type="text/javascript">

    var username = $("input[name*='username']").val();

    $(document).ready(function () {

        $('#userMapModal').on('show.bs.modal', function () {

            if ($('#user-map').length)
            {
                var contact_map = 'user-map';
                var mapAddress = $('#user-map').data('address');
                var mapType = $('#user-map').data('maptype');
                var zoomLvl = $('#user-map').data('zoomlvl');
                xv_user_maps(contact_map, mapAddress, mapType, zoomLvl);
            }

        });

        function xv_user_maps(selector, address, type, zoom_lvl) {
            var map = new google.maps.Map(document.getElementById(selector), {
                mapTypeId: google.maps.MapTypeId.type,
                scrollwheel: false,
                draggable: false,
                zoom: zoom_lvl
            });

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'address': address
            },
            function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map,
                    });
                    map.setCenter(results[0].geometry.location);
                    var center = map.getCenter();
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                }
            });
        }

        var scroll_pagination = function (ajaxloader, ajaxurl, wrapper) {

            $(window).scroll(function ()
            {
                if ($(window).scrollTop() == $(document).height() - $(window).height())
                {

                    $(ajaxloader).show();

                    var request = $.ajax({
                        url: ajaxurl,
                        type: "POST",
                        data: {username: username},
                        dataType: "html"
                    });

                    request.done(function (html) {
                        if (html)
                        {
                            //$(wrapper).append(html);
                            //$(ajaxloader).hide();
                        }
                        else
                        {
                            $(ajaxloader).html('<center>No more posts to show.</center>');
                        }
                    });

                    request.fail(function (jqXHR, textStatus) {
                        $(ajaxloader).html('<center>No more posts to show.</center>');
                    });

                }
            });
        };

        $('#photoModal').on('show.bs.modal', function (e) {
            console.log(e);
            $('<img class="img-responsive" src="' + e.relatedTarget.dataset.remoteImage + '">').load(function () {
                $("#photoModal .modal-body").empty();
                $(this).appendTo($('#photoModal .modal-body'));
            });
        });

        var ajaxloader = null;
        var ajaxurl = null;
        var wrapper = null;

        $("ul.nav-tabs li a").click(function () {
            $.cookie('nav_tab', $(this).attr("href"), {expires: 7});

            switch (nav_tab) {
                case '#timeline':

                    ajaxloader = 'div#loadmoreajaxloader';
                    ajaxurl = site_url('profile/pagination_timeline');
                    wrapper = "#postswrapper";

                    break;
                case '#articles':

                    ajaxloader = 'div#ajaxloader';
                    ajaxurl = site_url('profile/pagination_articles');
                    wrapper = "#postswrapper";

                    break;
                case '#photos':

                    ajaxloader = 'div#loadmoreajaxloader';
                    ajaxurl = site_url('profile/pagination_timeline');
                    wrapper = "#postswrapper";

                    break;
                default:

                    ajaxloader = 'li#ajaxloader';
                    ajaxurl = site_url('profile/pagination_timeline');
                    wrapper = "ul.timeline";

                    break;
            }

        });
        var nav_tab = $.cookie('nav_tab');
        $('.nav-tabs a[href="' + nav_tab + '"]').tab('show');

        switch (nav_tab) {
            case '#timeline':

                ajaxloader = 'li#ajaxloader';
                ajaxurl = site_url('profile/pagination_timeline');
                wrapper = "ul.timeline";

                break;
            case '#articles':

                ajaxloader = 'div#ajaxloader';
                ajaxurl = site_url('profile/pagination_articles');
                wrapper = "#postswrapper";

                break;
            case '#photos':

                ajaxloader = 'div#loadmoreajaxloader';
                ajaxurl = site_url('profile/pagination_timeline');
                wrapper = "#postswrapper";

                break;
            default:

                ajaxloader = 'li#ajaxloader';
                ajaxurl = site_url('profile/pagination_timeline');
                wrapper = "ul.timeline";

                break;
        }

        scroll_pagination(ajaxloader, ajaxurl, wrapper);

    });

</script>


