<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--=========================================
                                Bread crumb
  =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>


<!--===========================
        Blog
  ==============================-->   

<section id="blog-medium">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8">

                <h3>My Articles</h3>
                <div class="tex-widget">
                    <p>Recent work!</p>
                </div>


                <ul class="nav nav-tabs three" id="ornatTabs">
                    <li class="active"><a href="#published">Published</a></li>
                    <li class=""><a href="#pending">Pending</a></li>
                    <li class=""><a href="#working">Working</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="published">

                        <?php if (is_array($results)): ?>
                            <?php foreach ($results as $result): ?>

                                <div class="latest-post" style="clear: both;">
                                    <img src="<?php echo show_post_image($result->image); ?>" alt=""/>
                                    <div class="detail">
                                        <a href="<?php echo site_url('article/view/' . $result->id . '_' . url_title($result->title)); ?>"><?php echo $result->title; ?></a>
                                        <div class="date-stamp"><?php echo date("M jS, Y", strtotime($result->updated)); ?></div>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                    <div class="tab-pane fade in" id="pending">

                        <?php if (is_array($results)): ?>
                            <?php foreach ($results as $result): ?>

                                <div class="latest-post" style="clear: both;">
                                    <img src="<?php echo show_post_image($result->image); ?>" alt=""/>
                                    <div class="detail">
                                        <a href="<?php echo site_url('article/view/' . $result->id . '_' . url_title($result->title)); ?>"><?php echo $result->title; ?></a>
                                        <div class="date-stamp"><?php echo date("M jS, Y", strtotime($result->updated)); ?></div>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                    <div class="tab-pane fade in" id="working">

                        <?php if (is_array($results)): ?>
                            <?php foreach ($results as $result): ?>

                                <div class="latest-post" style="clear: both;">
                                    <img src="<?php echo show_post_image($result->image); ?>" alt=""/>
                                    <div class="detail">
                                        <a href="<?php echo site_url('article/view/' . $result->id . '_' . url_title($result->title)); ?>"><?php echo $result->title; ?></a>
                                        <div class="date-stamp"><?php echo date("M jS, Y", strtotime($result->updated)); ?></div>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </div>                

            </div>

            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="sidebar">
                    <?php $this->load->view('partial/search_form'); ?>
                    <h5>Categories</h5>
                    <?php show_tree_view($categories, 0); ?>
                </div><!--sidebar-->

            </div>

        </div>
    </div>
</section>
