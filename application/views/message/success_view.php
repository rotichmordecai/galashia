<div class="alert alert-dismissable alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <ul>
        <?php foreach ($messages as $message): ?>
            <li><?php echo $message ?></li>
        <?php endforeach ?>
    </ul>
</div>