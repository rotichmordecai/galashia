<div class="row">
    <div class="col-lg-12">
        <h1>Articles <small>Add new Article</small></h1>
        <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
    </div>
</div><!-- /.row -->

<div class="row"> 
    <?php
    $attributes = array('class' => '', 'id' => 'article-form', 'role' => 'form');
    echo form_open_multipart(current_url(), $attributes);
    ?>
    <div class="col-lg-6">        

        <div class="form-group <?php echo strlen(form_error('category_id')) > 0 ? 'has-error' : '' ?>">
            <label>Parent</label>
            <?php $addition = 'id="category_id" class="form-control" onChange=""'; ?>
            <?php echo form_dropdown('category_id', $category_options, set_value('category_id'), $addition); ?>
            <p class="help-block"><?php echo form_error('category_id'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
            <label>title</label>
            <input class="form-control" id="title" name="title" value="<?php echo set_value('title'); ?>"  placeholder="<?php echo set_value('title'); ?>">
            <p class="help-block"><?php echo form_error('title'); ?></p>
        </div>

        <div class="form-group  <?php echo strlen(form_error('image')) > 0 ? 'has-error' : '' ?>">
            <label>Image</label>
            <input type="file" id="image" name="image" />
            <p class="help-block"><?php echo form_error('image'); ?></p>
        </div>


        <div class="form-group <?php echo strlen(form_error('content')) > 0 ? 'has-error' : '' ?>">
            <label>Content</label>
            <textarea class="form-control" id="content" name="content" rows="5"><?php echo set_value('content'); ?></textarea>
            <p class="help-block"><?php echo form_error('content'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('fb_msg')) > 0 ? 'has-error' : '' ?>">
            <label>Facebook Update</label>
            <input class="form-control" id="fb_msg" name="fb_msg" value="<?php echo set_value('fb_msg'); ?>"  placeholder="<?php echo set_value('fb_msg'); ?>">
            <p class="help-block"><?php echo form_error('fb_msg'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('tw_msg')) > 0 ? 'has-error' : '' ?>">
            <label>Twitter Update</label>
            <input class="form-control" id="tw_msg" name="tw_msg" value="<?php echo set_value('tw_msg'); ?>"  placeholder="<?php echo set_value('tw_msg'); ?>">
            <p class="help-block"><?php echo form_error('tw_msg'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('li_msg')) > 0 ? 'has-error' : '' ?>">
            <label>LinkedIn Update</label>
            <input class="form-control" id="li_msg" name="li_msg" value="<?php echo set_value('li_msg'); ?>"  placeholder="<?php echo set_value('li_msg'); ?>">
            <p class="help-block"><?php echo form_error('li_msg'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('gp_msg')) > 0 ? 'has-error' : '' ?>">
            <label>Google+ Update</label>
            <input class="form-control" id="gp_msg" name="gp_msg" value="<?php echo set_value('gp_msg'); ?>"  placeholder="<?php echo set_value('gp_msg'); ?>">
            <p class="help-block"><?php echo form_error('gp_msg'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('tagline')) > 0 ? 'has-error' : '' ?>">
            <label>SEO Tagline</label>
            <input class="form-control" id="tagline" name="tagline" value="<?php echo set_value('tagline'); ?>"  placeholder="<?php echo set_value('tagline'); ?>">
            <p class="help-block"><?php echo form_error('tagline'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('description')) > 0 ? 'has-error' : '' ?>">
            <label>SEO Description</label>
            <input class="form-control" id="description" name="description" value="<?php echo set_value('description'); ?>"  placeholder="<?php echo set_value('description'); ?>">
            <p class="help-block"><?php echo form_error('description'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('keywords')) > 0 ? 'has-error' : '' ?>">
            <label>SEO Keywords</label>
            <input class="form-control" id="keywords" name="keywords" value="<?php echo set_value('keywords'); ?>"  placeholder="<?php echo set_value('keywords'); ?>">
            <p class="help-block"><?php echo form_error('keywords'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('author')) > 0 ? 'has-error' : '' ?>">
            <label>SEO Author</label>
            <input class="form-control" id="author" name="author" value="<?php echo set_value('author'); ?>"  placeholder="<?php echo set_value('author'); ?>">
            <p class="help-block"><?php echo form_error('author'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
            <label>Status</label>
            <?php $addition = 'id="status" class="form-control" onChange=""'; ?>
            <?php echo form_dropdown('status', $status_options, set_value('status'), $addition); ?>
            <p class="help-block"><?php echo form_error('status'); ?></p>
        </div>

        <button type="submit" class="btn btn-default">Submit Button</button> 
        <button type="reset" class="btn btn-default">Reset Button</button>

    </div>
    <div class="col-lg-6">
        <?php if (!$fb_session): ?>
            <a href="<?php echo $fb_login_url; ?>" class="btn btn-block btn-social btn-facebook btn-small">
                <i class="fa fa-facebook"></i> Sign in to share your post Facebook
            </a>
        <?php endif; ?>
        <?php if (!$tw_session): ?>
            <a href="<?php echo site_url('auth_other/twitter_signin'); ?>"  class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-twitter"></i> Sign in to share your post Twitter
            </a>
        <?php endif; ?>

        <?php if (isset($tw_account)): ?>
            <?php
            $data = array(
                'name' => 'twaccount[]',
                'id' => '',
                'value' => $tw_account->id,
                'checked' => FALSE,
                'style' => 'margin:10px',
            );
            echo form_checkbox($data);
            ?>
            <?php
            echo 'Twitter - ' . $tw_account->name;
            ?>
        <?php endif; ?>

        <?php if (isset($fb_user['id'])): ?>
            <?php
            $data = array(
                'name' => 'fbaccount[]',
                'id' => '',
                'value' => 'me',
                'checked' => FALSE,
                'style' => 'margin:10px',
            );
            echo form_checkbox($data);
            ?>
            <?php
            echo 'Facebook - ' . $fb_user['name'];
            ?>
        <?php endif; ?>

        <?php if (isset($fb_account['data'])): ?>    
            <?php foreach ($fb_account['data'] as $account): ?>
                <?php
                $data = array(
                    'name' => 'fbaccount[]',
                    'id' => '',
                    'value' => $account->id,
                    'checked' => FALSE,
                    'style' => 'margin:10px',
                );
                echo form_checkbox($data);
                ?>
                <?php
                echo $account->name;
                ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <?php echo form_close(); ?>

</div>