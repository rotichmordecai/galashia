<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Users <small>Edit User</small></h1>
        <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
    </div>
</div><!-- /.row -->
<?php foreach ($users as $user): ?>
    <div class="row">
        <div class="col-lg-6">
            <?php
            $attributes = array('class' => '', 'id' => 'user-form', 'role' => 'form');
            echo form_open(current_url(), $attributes);
            ?>

            <div class="form-group <?php echo strlen(form_error('username')) > 0 ? 'has-error' : '' ?>">
                <label>username</label>
                <input class="form-control" id="username" name="username" value="<?php echo set_value('username', $user->username); ?>"  placeholder="<?php echo set_value('username'); ?>">
                <p class="help-block"><?php echo form_error('username'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('email')) > 0 ? 'has-error' : '' ?>">
                <label>email</label>
                <input class="form-control" id="email" name="email" value="<?php echo set_value('email', $user->email); ?>"  placeholder="<?php echo set_value('email'); ?>">
                <p class="help-block"><?php echo form_error('email'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('password')) > 0 ? 'has-error' : '' ?>">
                <label>password</label>
                <input class="form-control" id="password" name="password" value="<?php echo set_value('password'); ?>"  placeholder="<?php echo set_value('password'); ?>">
                <p class="help-block"><?php echo form_error('password'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('confirm_password')) > 0 ? 'has-error' : '' ?>">
                <label>password</label>
                <input class="form-control" id="password" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>"  placeholder="<?php echo set_value('confirm_password'); ?>">
                <p class="help-block"><?php echo form_error('confirm_password'); ?></p>
            </div>

            <button type="submit" class="btn btn-default">Submit Button</button> 
            <button type="reset" class="btn btn-default">Reset Button</button>

            <?php echo form_close(); ?>

        </div>
    </div>
<?php endforeach; ?>


