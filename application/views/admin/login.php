<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container" style="margin-top:150px">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><strong>Sign in </strong></h3></div>
            <div class="panel-body">
                <form role="form" action="<?php echo site_url('auth/login?admin=true'); ?>" method="post">
                    <div class="form-group">
                        <label for="login">Username or Email</label>
                        <input type="email" class="form-control" style="border-radius:0px" id="login" name="login" placeholder="Enter Username or Email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" style="border-radius:0px" id="password" name="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-sm btn-default">Sign in</button>
                </form>
            </div>
        </div>
    </div>
</div>