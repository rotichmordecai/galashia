<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Articles <small>Scroll Through</small></h1>
        <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
        <a href="<?php echo site_url('admin/add_article'); ?>" class="btn btn-primary pull-right">Add New</a>
        <br /><br />
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover tablesorter">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Id <i class="fa fa-sort"></i></th>
                    <th width="200">Title <i class="fa fa-sort"></i></th>
                    <th>Content <i class="fa fa-sort"></i></th> 
                    <th class="col-lg-3">Actions <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php if (is_array($results)): ?>
                    <?php foreach ($results as $result): ?>
                        <tr>
                            <td><img src="<?php echo show_post_image($result->image); ?>" alt="" width="150"/></td>
                            <td><?php echo $result->id; ?></td>
                            <td><?php echo $result->title; ?></td>
                            <td>
                                <?php echo substr(strip_tags(htmlspecialchars_decode(html_entity_decode($result->content))), 0, 250) . ' ... '; ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a href="<?php echo site_url('admin/edit_article/' . $result->id); ?>" class="btn btn-default">Edit</a>
                                    <a href="<?php echo site_url('article/view/' . $result->id); ?>" class="btn btn-default" target="_blank">View</a>
                                    <a href="<?php echo site_url('admin/delete_article/' . $result->id); ?>" class="btn btn-default">Delete</a>
                                    <?php if ($result->status): ?>
                                        <a href="<?php echo site_url('admin/block_article/' . $result->id); ?>" class="btn btn-default">Block</a>
                                    <?php else: ?>
                                        <a href="<?php echo site_url('admin/activate_article/' . $result->id); ?>" class="btn btn-default">Activate</a>
                                    <?php endif; ?>                                   
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>

        <?php echo $pagination; ?>

    </div>
</div>


