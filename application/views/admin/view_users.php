<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h1>Users <small>Scroll Through</small></h1>
        <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
        <a href="<?php echo site_url('admin/add_user'); ?>" class="btn btn-primary pull-right">Add New</a>
        <br /><br />
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover tablesorter">
            <thead>
                <tr>
                    <th>Username <i class="fa fa-sort"></i></th>
                    <th>Email <i class="fa fa-sort"></i></th>
                    <th>Last Login <i class="fa fa-sort"></i></th>
                    <th class="col-lg-3">Actions <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php if (is_array($results)): ?>
                    <?php foreach ($results as $result): ?>
                        <tr>
                            <td><?php echo $result->username; ?></td>
                            <td><?php echo $result->email; ?></td>
                            <td><?php echo $result->last_login; ?></td>
                            <td>
                                <div class="btn-group">
                                    <a href="<?php echo site_url('admin/edit_user/' . $result->id); ?>" class="btn btn-default">Edit</a>
                                    <a href="<?php echo site_url('admin/view_user/' . $result->id); ?>" class="btn btn-default">View</a>
                                    <a href="<?php echo site_url('admin/delete_user/' . $result->id); ?>" class="btn btn-default">Delete</a>
                                    <?php if ($result->activated != 1 or $result->banned == 1): ?>
                                        <a href="<?php echo site_url('admin/activate_user/' . $result->id); ?>" class="btn btn-default">Activate</a>
                                    <?php else: ?>
                                        <a href="<?php echo site_url('admin/block_user/' . $result->id); ?>" class="btn btn-default">Block</a>
                                    <?php endif; ?>   
                                        <?php if ($result->is_admin == 1): ?>
                                        <a href="<?php echo site_url('admin/set_user_admin/' . $result->id.'/0'); ?>" class="btn btn-default">User</a>
                                    <?php else: ?>
                                        <a href="<?php echo site_url('admin/set_user_admin/' . $result->id .'/1'); ?>" class="btn btn-default">Admin</a>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>

        <?php echo $pagination; ?>

    </div>
</div>

