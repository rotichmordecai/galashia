<div class="row">
    <div class="col-lg-12">
        <h1>Settings <small>All site settings</small></h1>
        <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
    </div>
</div><!-- /.row -->
<?php foreach ($settings as $setting): ?>
    <div class="row">
        <div class="col-lg-6">
            <?php
            $attributes = array('class' => '', 'id' => 'article-form', 'role' => 'form');
            echo form_open_multipart(current_url(), $attributes);
            ?>

            <input type="hidden" id="id" name="id" value="<?php echo set_value('id', $setting->id); ?>" />

            <div class="form-group <?php echo strlen(form_error('name')) > 0 ? 'has-error' : '' ?>">
                <label>Name</label>
                <input class="form-control" id="name" name="name" value="<?php echo set_value('name', $setting->name); ?>"  placeholder="<?php echo set_value('name'); ?>">
                <p class="help-block"><?php echo form_error('name'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
                <label>Meta Title</label>
                <input class="form-control" id="title" name="title" value="<?php echo set_value('title', $setting->title); ?>"  placeholder="<?php echo set_value('title'); ?>">
                <p class="help-block"><?php echo form_error('title'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('tagline')) > 0 ? 'has-error' : '' ?>">
                <label>Meta Tag Line</label>
                <input class="form-control" id="tagline" name="tagline" value="<?php echo set_value('tagline', $setting->tagline); ?>"  placeholder="<?php echo set_value('tagline'); ?>">
                <p class="help-block"><?php echo form_error('tagline'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('description')) > 0 ? 'has-error' : '' ?>">
                <label>Meta Description</label>
                <textarea class="form-control" id="description" name="description" rows="5"><?php echo set_value('description', htmlspecialchars_decode(html_entity_decode($setting->description))); ?></textarea>
                <p class="help-block"><?php echo form_error('description'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('keywords')) > 0 ? 'has-error' : '' ?>">
                <label>SEO Author</label>
                <input class="form-control" id="keywords" name="keywords" value="<?php echo set_value('keywords', $setting->keywords); ?>"  placeholder="<?php echo set_value('keywords'); ?>">
                <p class="help-block"><?php echo form_error('keywords'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('author')) > 0 ? 'has-error' : '' ?>">
                <label>SEO Author</label>
                <input class="form-control" id="author" name="author" value="<?php echo set_value('author', $setting->author); ?>"  placeholder="<?php echo set_value('author'); ?>">
                <p class="help-block"><?php echo form_error('author'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                <label>Status</label>
                <?php $addition = 'id="status" class="form-control" onChange=""'; ?>
                <?php echo form_dropdown('status', $status_options, set_value('status', $setting->status), $addition); ?>
                <p class="help-block"><?php echo form_error('status'); ?></p>
            </div>

            <button type="submit" class="btn btn-default">Submit Button</button> 
            <button type="reset" class="btn btn-default">Reset Button</button>

            <?php echo form_close(); ?>

        </div>
    </div>
<?php endforeach; ?>