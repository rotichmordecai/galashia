<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h1>Categories <small>Scroll Through</small></h1>
        <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
        <a href="<?php echo site_url('admin/add_category'); ?>" class="btn btn-primary pull-right">Add New</a>
        <br /><br />
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover tablesorter">
            <thead>
                <tr>
                    <th>Id <i class="fa fa-sort"></i></th>
                    <th>Title <i class="fa fa-sort"></i></th>
                    <th>Parent <i class="fa fa-sort"></i></th>
                    <th class="col-lg-3">Actions <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php if (is_array($results)): ?>
                    <?php foreach ($results as $result): ?>
                        <tr>
                            <td><?php echo $result->id; ?></td>
                            <td><?php echo $result->title; ?></td>
                            <td><?php echo $result->parent_title; ?></td>
                            <td>
                                <div class="btn-group">
                                    <a href="<?php echo site_url('admin/edit_category/' . $result->id); ?>" class="btn btn-default">Edit</a>
                                    <a href="<?php echo site_url('admin/view_category/' . $result->id); ?>" class="btn btn-default">View</a>
                                    <a href="<?php echo site_url('admin/delete_category/' . $result->id); ?>" class="btn btn-default">Delete</a>
                                    <?php if ($result->status): ?>
                                        <a href="<?php echo site_url('admin/block_category/' . $result->id); ?>" class="btn btn-default">Block</a>
                                    <?php else: ?>
                                        <a href="<?php echo site_url('admin/activate_category/' . $result->id); ?>" class="btn btn-default">Activate</a>
                                    <?php endif; ?>                                    
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>

        <?php echo $pagination; ?>

    </div>
</div>

