<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Client <small>Edit Client</small></h1>
        <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
    </div>
</div><!-- /.row -->

<?php foreach ($clients as $client): ?>
    <div class="row">
        <div class="col-lg-6">
            <?php
            $attributes = array('class' => '', 'id' => 'client-form', 'role' => 'form');
            echo form_open_multipart(current_url(), $attributes);
            ?>

            <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
                <label>title</label>
                <input class="form-control" id="title" name="title" value="<?php echo set_value('title', $client->title); ?>"  placeholder="<?php echo set_value('title'); ?>">
                <p class="help-block"><?php echo form_error('title'); ?></p>
            </div>

            <div class="form-group  <?php echo strlen(form_error('logo')) > 0 ? 'has-error' : '' ?>">
                <label>Image</label>
                <input type="file" id="logo" name="logo" />
                <p class="help-block"><?php echo form_error('logo'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('description')) > 0 ? 'has-error' : '' ?>">
                <label>Content</label>
                <textarea class="form-control" id="description" name="description" rows="5"><?php echo set_value('description', htmlspecialchars_decode(html_entity_decode($client->description))); ?></textarea>
                <p class="help-block"><?php echo form_error('description'); ?></p>
            </div>

            <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                <label>Status</label>
                <?php $addition = 'id="status" class="form-control" onChange=""'; ?>
                <?php echo form_dropdown('status', $status_options, set_value('status', $client->status), $addition); ?>
                <p class="help-block"><?php echo form_error('status'); ?></p>
            </div>

            <button type="submit" class="btn btn-default">Submit Button</button> 
            <button type="reset" class="btn btn-default">Reset Button</button>

            <?php echo form_close(); ?>

        </div>
    </div>
<?php endforeach; ?>
