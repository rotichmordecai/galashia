<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Categories <small>Add new Category</small></h1>
        <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">

        <?php
        $attributes = array('class' => '', 'id' => 'category-form', 'role' => 'form');
        echo form_open(current_url(), $attributes);
        ?>

        <div class="form-group <?php echo strlen(form_error('category_id')) > 0 ? 'has-error' : '' ?>">
            <label>Parent</label>
            <?php $addition = 'id="category_id" class="form-control" onChange=""'; ?>
            <?php echo form_dropdown('category_id', $category_options, set_value('category_id'), $addition); ?>
            <p class="help-block"><?php echo form_error('category_id'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
            <label>Title</label>
            <input class="form-control" id="title" name="title" value="<?php echo set_value('title'); ?>"  placeholder="<?php echo set_value('title'); ?>">
            <p class="help-block"><?php echo form_error('title'); ?></p>
        </div>

        <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
            <label>Status</label>
            <?php $addition = 'id="status" class="form-control" onChange=""'; ?>
            <?php echo form_dropdown('status', $status_options, set_value('status'), $addition); ?>
            <p class="help-block"><?php echo form_error('status'); ?></p>
        </div>

        <button type="submit" class="btn btn-default">Submit Button</button> 
        <button type="reset" class="btn btn-default">Reset Button</button>

        <?php echo form_close(); ?>

    </div>
</div>
