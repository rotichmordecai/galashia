<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--=========================================
                                Bread crumb
  =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>



<!--===========================
       Blog
 ==============================-->   

<section id="shortcode">
    <div class="container">

        <div class="contact_form">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
                    <?php
                    $attributes = array('class' => '', 'id' => 'article-form', 'role' => 'form');
                    echo form_open_multipart(current_url(), $attributes);
                    ?>

                    <div class="form-group <?php echo strlen(form_error('category_id')) > 0 ? 'has-error' : '' ?>">
                        <label>Parent</label>
                        <?php $addition = 'id="category_id" class="form-control" onChange=""'; ?>
                        <?php echo form_dropdown('category_id', $category_options, set_value('category_id'), $addition); ?>
                        <p class="help-block"><?php echo form_error('category_id'); ?></p>
                    </div>

                    <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
                        <label>title</label>
                        <input class="form-control" id="title" name="title" value="<?php echo set_value('title'); ?>"  placeholder="<?php echo set_value('title'); ?>">
                        <p class="help-block"><?php echo form_error('title'); ?></p>
                    </div>

                    <div class="form-group  <?php echo strlen(form_error('image')) > 0 ? 'has-error' : '' ?>">
                        <label>Image</label>
                        <input type="file" id="image" name="image" />
                        <p class="help-block"><?php echo form_error('image'); ?></p>
                    </div>


                    <div class="form-group <?php echo strlen(form_error('content')) > 0 ? 'has-error' : '' ?>">
                        <label>Content</label>
                        <textarea class="form-control" id="content" name="content" rows="5"><?php echo set_value('content'); ?></textarea>
                        <p class="help-block"><?php echo form_error('content'); ?></p>
                    </div>
                    
                    <div id="grammer_response">                        
                    </div>

                    <button type="button" id="grammer" class="btn btn-primary">Check Grammer</button>
                    <button type="submit" class="btn btn-default">Save Post</button> 

                    <?php echo form_close(); ?>
                </div>
            </div>       	
        </div><!--contact_form-->    
    </div><!--container-->
</section> <!--contact-->

<script type="text/javascript">
    $("#grammer").click(function() {

        var content = $('#content').tinymce().getContent();

        var request = $.ajax({
            url: "<?php echo site_url('article/grammer'); ?>",
            type: "POST",
            data: {content: content},
            dataType: "html"
        });

        request.done(function(msg) {
            $("#grammer_response").html(msg);
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });

    });
</script>
