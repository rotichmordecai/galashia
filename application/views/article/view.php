<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--=========================================
                                Bread crumb
  =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>


<!--===========================
        Blog
  ==============================-->   
<section id="blog-single">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8">

                <?php foreach ($articles as $result): ?>
                    <article class="blog-post style-2">
                        <div class="post-detail">
                            <div class="post-visual">
                                <img src="<?php echo show_post_image($result->image); ?>" alt="" class="post-image2"/>
                                <div class="clearfix"></div>
                                <div class="post-quick">

                                    <div class="post-icon">
                                        <span class=" icon-picture"></span>
                                    </div>

                                    <div class="post-date">
                                        <span class="date"><?php echo date("d", strtotime($result->updated)); ?></span>
                                        <span class="month"><?php echo date("M", strtotime($result->updated)); ?></span>
                                    </div>
                                </div><!--post-quick-->
                            </div><!--post-detail-->
                            <div class="post">
                                <div class="post-heading"><a href="<?php echo current_url(); ?>"><?php echo $result->title; ?></a></div>
                                <ul class="blog-info">
                                    <li class="icon-time"><span><?php echo date("M jS, Y", strtotime($result->updated)); ?></span></li>
                                    <li class="icon-user"><a href="#">Admin</a></li>
                                    <li class="icon-align-left"><a href="#"><?php echo isset($analytics->view) ? $analytics->view . ' Views' : NULL; ?></a></li>
                                    <li class="icon-comments"><a href="#"><?php echo isset($comments) ? count($comments) : NULL; ?> Comments</a></li>
                                </ul>
                                <?php echo htmlspecialchars_decode(html_entity_decode($result->content)); ?>
                            </div><!--\\post-->
                        </div><!--\\post-detail-->
                    </article> 
                    <div>
                        <span class='st_sharethis_large' displayText='ShareThis'></span>
                        <span class='st_facebook_large' displayText='Facebook'></span>
                        <span class='st_twitter_large' displayText='Tweet'></span>
                        <span class='st_linkedin_large' displayText='LinkedIn'></span>
                        <span class='st_pinterest_large' displayText='Pinterest'></span>
                        <span class='st_email_large' displayText='Email'></span>
                    </div>
                    <h3>Comments</h3>
                    <?php foreach ($comments as $comment): ?>
                        <div class="comments">                            
                            <div class="comment">
                                <div class="user-pic">
                                    <img src="<?php echo show_user_image(); ?>" />
                                </div>
                                <div class="post-user-detail">
                                    <span class="author"><?php echo $comment->id; ?></span> -
                                    <span class="date-stamp"><?php echo date("M jS, Y h:i:s A", strtotime($comment->updated)); ?></span>
                                    <p>
                                        <?php echo htmlspecialchars_decode(html_entity_decode($comment->textcomment)); ?>
                                    </p>
                                </div>
                            </div>
                        </div>   
                        <br /><br />
                    <?php endforeach; ?>

                    <?php
                    $attributes = array('class' => '', 'id' => 'comment-form', 'role' => 'form');
                    echo form_open(current_url(), $attributes);
                    ?>
                    <input type="hidden" id="article_id" name="article_id" value="<?php echo $result->id; ?>" >
                    <input type="hidden" id="user_id" name="user_id" value="<?php echo $this->tank_auth->get_user_id(); ?>" >

                    <div class="form-group <?php echo strlen(form_error('textcomment')) > 0 ? 'has-error' : '' ?>">
                        <label>Post your comments</label>
                        <textarea class="form-control" id="content" name="textcomment" rows="5"><?php echo set_value('textcomment'); ?></textarea>
                        <p class="help-block"><?php echo form_error('textcomment'); ?></p>
                    </div>

                    <button type="submit" class="btn btn-default">Submit Button</button> 
                    <button type="reset" class="btn btn-default">Reset Button</button>

                    <?php echo form_close(); ?>
                <?php endforeach; ?>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="sidebar">
                    <?php $this->load->view('partial/search_form'); ?>
                    <h5>Categories</h5>
                    <?php show_tree_view($categories, 0); ?>
                </div><!--sidebar-->

            </div>

        </div>
    </div>
</section>
