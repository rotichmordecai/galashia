<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

print_r($project_image);

?>

<style type="text/css">

    .well {
        margin-top:-20px;
        background-color:#007FBD;
        border:2px solid #0077B2;
        text-align:center;
        cursor:pointer;
        font-size: 25px;
        padding: 15px;
        border-radius: 0px !important;
        color: white;
    }

    .well:hover {
        margin-top:-20px;
        background-color:#0077B2;
        border:2px solid #0077B2;
        text-align:center;
        cursor:pointer;
        font-size: 25px;
        padding: 15px;
        border-radius: 0px !important;
        border-bottom : 2px solid rgba(97, 203, 255, 0.65);
        color: white;
    }


    .bg_blur
    {
        background-image:url('http://data2.whicdn.com/images/139218968/large.jpg');
        height: 300px;
        background-size: cover;
    }

    .follow_btn {
        text-decoration: none;
        position: absolute;
        left: 35%;
        top: 42.5%;
        width: 35%;
        height: 15%;
        background-color: #007FBE;
        padding: 10px;
        padding-top: 6px;
        color: #fff;
        text-align: center;
        font-size: 20px;
        border: 4px solid #007FBE;
    }

    .follow_btn:hover {
        text-decoration: none;
        position: absolute;
        left: 35%;
        top: 42.5%;
        width: 35%;
        height: 15%;
        background-color: #007FBE;
        padding: 10px;
        padding-top: 6px;
        color: #fff;
        text-align: center;
        font-size: 20px;
        border: 4px solid rgba(255, 255, 255, 0.8);
    }

    .header{
        color : #808080;
        margin-left:10%;
        margin-top:70px;
    }

    .picture{
        height:150px;
        width:150px;
        position:absolute;
        top: 75px;
        left:-75px;
    }

    .picture_mob{
        position: absolute;
        width: 35%;
        left: 35%;
        bottom: 70%;
    }

    .btn-style{
        color: #fff;
        background-color: #007FBE;
        border-color: #adadad;
        width: 33.3%;
    }

    .btn-style:hover {
        color: #333;
        background-color: #3D5DE0;
        border-color: #adadad;
        width: 33.3%;

    }


    @media (max-width: 767px) {
        .header{
            text-align : center;
        }



        .nav{
            margin-top : 30px;
        }
    }


    .portfolio img:hover {
        cursor: pointer;
        -webkit-transform: scale(1.4, 1.4);
        -ms-transform: scale(1.4, 1.4);
        transform: scale(1.4, 1.4);
        transition-duration: 0.5s;
        -webkit-transition-duration: 0.5s; /* Safari */
        z-index: 1;
    }

</style>

<!--=========================================
                                Bread crumb
  =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>


<!--===========================
        Blog
  ==============================-->   

<section id="blog-medium">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

                <div class="container" style="margin-top: 20px; margin-bottom: 20px;">
                    <div class="row panel">
                        <div class="col-md-4 bg_blur ">
                            <a href="#" class="follow_btn hidden-xs">Like</a>
                        </div>
                        <div class="col-md-8  col-xs-12">
                            <div class="header">
                                <h1><?php echo $project->title; ?></h1>
                                <h4>John Doe</h4>
                                <span><?php echo $project->description; ?></span>
                            </div>
                        </div>
                    </div>   

                    <div class="row nav">    
                        <div class="col-md-4"></div>
                        <div class="col-md-8 col-xs-12" style="margin: 0px;padding: 0px;">
                            <div class="col-md-6 col-xs-6 well"><i class="fa fa-weixin fa-lg"></i> 16</div>                          
                            <div class="col-md-6 col-xs-6 well"><i class="fa fa-thumbs-o-up fa-lg"></i> 26</div>
                        </div>
                    </div>
                </div>

                <h3>Project Images</h3>

                <div class="row portfolio">
                    <?php foreach ($project_image as $key => $value): ?>
                    <img src="<?php echo site_url('uploads/project/'.$value->thumb); ?>" alt="" class="img-rounded">
                    <?php endforeach; ?>                   
                </div>


            </div>
        </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {



    });
</script>
