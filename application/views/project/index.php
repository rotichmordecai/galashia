<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php

function string_limit($string, $result_id, $limit) {
    $string = strip_tags($string);
    if (strlen($string) > $limit) {
        $stringCut = substr($string, 0, $limit);
        $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '... <a href="' . site_url('project/view/' . $result_id) . '">Read More</a>';
    }
    return $string;
}
?>  

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>

<section id="blog-medium">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8">

                <?php if (count($results)): ?>
                    <div class="row">
                    <?php endif; ?>

                    <?php foreach ($results as $key => $result): ?>
                        <div class="col-md-4 portfolio-item">
                            <a href="<?php echo site_url('project/view/' . $result->id); ?>">
                                <img class="img-responsive" src="http://placehold.it/700x400" alt="">
                            </a>
                            <h3>
                                <a href="<?php echo site_url('project/view/' . $result->id); ?>"><?php echo $result->title; ?></a>
                            </h3>
                            <p>
                                <?php echo string_limit($result->description, $result->id, 100); ?>
                            </p>
                        </div>
                    <?php endforeach; ?>

                    <?php if (count($results)): ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

</script>
