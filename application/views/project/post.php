<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--=========================================
                                Bread crumb
  =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>


<!--===========================
        Blog
  ==============================-->   

<section id="blog-medium">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8">

                <?php
                $attributes = array('class' => '', 'id' => 'project-form', 'role' => 'form');
                echo form_open(current_url(), $attributes);
                ?>

                <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
                    <label>Title</label>
                    <input class="form-control" id="title" name="title" value="<?php echo set_value('title'); ?>"  placeholder="<?php echo set_value('title'); ?>">
                    <p class="help-block"><?php echo form_error('title'); ?></p>
                </div>

                <div class="form-group <?php echo strlen(form_error('description')) > 0 ? 'has-error' : '' ?>">
                    <label>Description</label>
                    <input class="form-control" id="description" name="description" value="<?php echo set_value('description'); ?>"  placeholder="<?php echo set_value('description'); ?>">
                    <p class="help-block"><?php echo form_error('description'); ?></p>
                </div>

                <div class="form-group <?php echo strlen(form_error('start_date')) > 0 ? 'has-error' : '' ?>">
                    <label>Start date</label>
                    <input class="form-control" id="start_date" name="start_date" value="<?php echo set_value('start_date'); ?>"  placeholder="<?php echo set_value('start_date'); ?>">
                    <p class="help-block"><?php echo form_error('start_date'); ?></p>
                </div>

                <div class="form-group <?php echo strlen(form_error('end_date')) > 0 ? 'has-error' : '' ?>">
                    <label>End date</label>
                    <input class="form-control" id="end_date" name="end_date" value="<?php echo set_value('end_date'); ?>"  placeholder="<?php echo set_value('end_date'); ?>">
                    <p class="help-block"><?php echo form_error('end_date'); ?></p>
                </div>

                <button type="submit" class="btn btn-default">Submit Button</button> 
                <button type="reset" class="btn btn-default">Reset Button</button>

                <?php echo form_close(); ?>

            </div>
        </div>
</section>