<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--=========================================
                                Bread crumb
  =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>



<!--===========================
       Blog
 ==============================-->   
<section id="blog-medium">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8">
                <?php if (is_array($results)): ?>
                    <?php foreach ($results as $result): ?>
                        <?php
                        $analytics = $this->analytics_model->select(array('article_id' => $result->id));
                        $analytics = isset($analytics[0]) ? $analytics[0] : NULL;
                        $comments = $this->comment_model->select(array('article_id' => $result->id));
                        $comments = isset($comments[0]) ? $comments[0] : NULL;
                        ?>
                        <article class="blog-post">
                            <div class="post-detail">
                                <div class="col-lg-5 col-md-6 col-sm-6">
                                    <div class="post-visual">

                                        <img src="<?php echo show_post_image($result->image); ?>" alt=""/>
                                        <div class="clearfix"></div>
                                        <div class="post-quick">

                                            <div class="post-icon">
                                                <span class=" icon-picture"></span>
                                            </div>

                                            <div class="post-date">
                                                <span class="date"><?php echo date("d", strtotime($result->updated)); ?></span>
                                                <span class="month"><?php echo date("M", strtotime($result->updated)); ?></span>
                                            </div>
                                        </div><!--post-quick-->

                                    </div><!--post-visual-->
                                </div>
                                <div class="col-lg-7 col-md-6 col-sm-6 ">
                                    <div class="post">
                                        <div class="post-heading"><a href="<?php echo site_url('article/view/' . $result->id .'_'.url_title($result->title)); ?>"><?php echo $result->title; ?></a></div>
                                        <ul class="blog-info">
                                            <li class="icon-time"><span><?php echo date("M jS, Y", strtotime($result->updated)); ?></span></li>
                                            <li class="icon-user"><a href="#">Admin</a></li>
                                            <li class="icon-align-left"><a href="#"><?php echo isset($analytics->view) ? $analytics->view . ' Views' : NULL; ?></a></li>
                                            <li class="icon-comments"><a href="#"><?php echo isset($comments) ? count($comments) : NULL; ?> Comments</a></li>
                                            <?php if (count($this->article_model->select(array('id' => $result->id, 'user_id' => $this->tank_auth->get_user_id()))) == 1): ?>
                                                <li class="icon-align-left"><a href="<?php echo site_url('article/edit/' . $result->id); ?>">Update Article</a></li>
                                            <?php endif; ?>
                                        </ul>
                                        <?php echo substr(strip_tags(htmlspecialchars_decode(html_entity_decode($result->content))), 0, 550) . ' ... '; ?>
                                        <br />
                                        <a class="readmore" href="<?php echo site_url('article/view/' . $result->id .'_'.url_title($result->title)); ?>">Read more →</a>
                                    </div><!--\\post-->
                                </div>
                                <div class="clearfix"></div>
                            </div><!--\\post-detail-->
                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>
                <!--===========================
                    Pagination
                  ==============================-->  

                <?php echo isset($pagination) ? $pagination : NULL; ?>  


            </div>

            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="sidebar">
                    <?php $this->load->view('partial/search_form'); ?>
                    <h5>Categories</h5>

                    <?php show_tree_view($categories, 0); ?>

                </div><!--sidebar-->

            </div>

        </div>
    </div>
</section>