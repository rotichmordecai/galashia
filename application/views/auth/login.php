<?php
$login = array(
    'name' => 'login',
    'id' => 'login',
    'value' => set_value('login'),
    'maxlength' => 80,
    'size' => 30,
);
if ($login_by_username AND $login_by_email) {
    $login_label = 'Email or login';
} else if ($login_by_username) {
    $login_label = 'Login';
} else {
    $login_label = 'Email';
}
$password = array(
    'name' => 'password',
    'id' => 'password',
    'size' => 30,
);
$remember = array(
    'name' => 'remember',
    'id' => 'remember',
    'value' => 1,
    'checked' => set_value('remember'),
    'style' => 'margin:0;padding:0',
);
$captcha = array(
    'name' => 'captcha',
    'id' => 'captcha',
    'maxlength' => 8,
);
?>

<!-- google friend connect -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">google.load('friendconnect', '0.8');</script>
<script type="text/javascript">
    google.friendconnect.container.setParentUrl('/' /* location of rpc_relay.html and canvas.html */);
    google.friendconnect.container.initOpenSocialApi({
        site: '<?php echo $this->config->item('google_app_id'); ?>',
        onload: function (securityToken) {
            initAllData();
        }
    });

    // main initialization function for google friend connect
    function initAllData()
    {
        var req = opensocial.newDataRequest();
        req.add(req.newFetchPersonRequest("OWNER"), "owner_data");
        req.add(req.newFetchPersonRequest("VIEWER"), "viewer_data");
        var idspec = new opensocial.IdSpec({
            'userId': 'OWNER',
            'groupId': 'FRIENDS'
        });
        req.add(req.newFetchPeopleRequest(idspec), 'site_friends');
        req.send(onData);
    }
    ;

    // main function for handling user data
    function onData(data)
    {
        // getting the site data, we don't need this for now
        //if (!data.get("owner_data").hadError()) 
        //{
        //	var owner_data = data.get("owner_data").getData();
        //	document.getElementById("site-name").innerHTML = owner_data.getDisplayName();
        //alert('user is logging in');
        //}

        var viewer_info = document.getElementById("viewer-info");
        if (data.get("viewer_data").hadError())
        {
            google.friendconnect.renderSignInButton({'id': 'gfc-button', 'text': 'Click here to join', 'style': 'long'});
            document.getElementById('gfc-button').style.display = 'block';
            viewer_info.innerHTML = '';
            //alert('there has been an error here');
        }
        else
        {
            document.getElementById('gfc-button').style.display = 'none';
            var viewer = data.get("viewer_data").getData();
            //viewer_info.innerHTML = "Hello, " + viewer.getDisplayName() + " " +
            //						"<a href='#' onclick='google.friendconnect.requestSettings()'>Settings</a> | " +
            //				        "<a href='#' onclick='google.friendconnect.requestInvite()'>Invite</a> | " +
            //				        "<a href='#' onclick='google.friendconnect.requestSignOut()'>Sign out</a>";
            //alert('user has been loaded');
            //alert(viewer.getDisplayName());
            //alert(viewer.getId());

            // let's redirect the user to our login_google action in auth_other controller
            window.location = "<?php echo site_url('auth_other/gfc_signin/'); ?>" + "/" + viewer.getId();
        }

        // for displaying friends, but we don't need this for now
        //if (!data.get("site_friends").hadError()) 
        //{
        //	var site_friends = data.get("site_friends").getData();
        //	var list = document.getElementById("friends-list");
        //	list.innerHTML = "";
        //	site_friends.each(function(friend) 
        //	{
        // 		list.innerHTML += "<li>" + friend.getDisplayName() + "</li>";
        //	});
        //}
    }
    ;
</script>

<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!--=========================================
                               Bread crumb
 =========================================-->   

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>



<!--===========================
       contact
 ==============================-->   
<section id="contact">

    <div class="container">
        <?php echo $this->message->display(); ?>
        <div class="clearfix"></div>
        <div class="contact_form">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">                        
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">  

                    <?php echo form_open($this->uri->uri_string()); ?>
                    <?php echo form_label($login_label, $login['id']); ?>
                    <?php echo form_input($login); ?>
                    <?php echo form_error($login['name']); ?>
                    <?php echo isset($errors[$login['name']]) ? $errors[$login['name']] : ''; ?>

                    <?php echo form_label('Password', $password['id']); ?>
                    <?php echo form_password($password); ?>
                    <?php echo form_error($password['name']); ?>
                    <?php echo isset($errors[$password['name']]) ? $errors[$password['name']] : ''; ?>


                    <?php
                    if ($show_captcha):
                        if ($use_recaptcha):
                            ?>
                            <div id="recaptcha_image"></div>
                            <a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
                            <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                            <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
                            <div class="recaptcha_only_if_image">Enter the words above</div>
                            <div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
                            <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
                            <?php echo form_error('recaptcha_response_field'); ?>
                            <?php echo $recaptcha_html; ?>

                        <?php else: ?>

                            <p>Enter the code exactly as it appears:</p>
                            <?php echo $captcha_html; ?>
                            <?php echo form_label('Confirmation Code', $captcha['id']); ?>
                            <?php echo form_input($captcha); ?>
                            <?php echo form_error($captcha['name']); ?>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php echo anchor('/auth/register/', 'Register?'); ?>
                    <br />
                    <?php echo anchor('/auth/forgot_password/', 'Forgot password?'); ?>
                    <br /><br />
                    <?php echo form_submit('submit', 'Let me in'); ?>
                    <?php echo form_close(); ?>

                    <a href="<?php echo $login_url; ?>" class="btn btn-block btn-social btn-facebook btn-small">
                        <i class="fa fa-facebook"></i> Sign in with Facebook
                    </a>
                    
                    <a href="<?php echo site_url('auth_other/twitter_signin'); ?>" class="btn btn-block btn-social btn-twitter btn-small">
                        <i class="fa fa-facebook"></i> Sign in with Twitter
                    </a>

                    <!--
                    <table>
                        <tr>
                            <td>
                                <a href="<?php echo $login_url; ?>" class="btn btn-block btn-social btn-facebook btn-small">
                                    <i class="fa fa-facebook"></i> Sign in with Facebook
                                </a>
                            </td>
                            <td>
                                <a class="twitter" href="<?php echo site_url('auth_other/twitter_signin'); ?>">
                                    <img style="margin-top:5px;" src="<?php echo base_url(); ?>assets/front/assets/img/social//twitter_login_button.gif" alt="twitter login" border="0"/>
                                </a>
                            </td>
                            <td>
                                <div id="gfc-button"></div>
                                (Google Friend Connect)
                            </td>				
                            <td>
                                <a href="<?php echo site_url('auth_other/google_openid_signin'); ?>">
                                    <img style="margin-top:5px;" src="<?php echo base_url(); ?>assets/front/assets/img/social//google_connect_button.png" alt="google open id" border="0"/>
                                </a>
                            </td>
                            <td>
                                <a href="<?php echo site_url('auth_other/yahoo_openid_signin'); ?>">
                                    <img style="margin-top:5px;" src="<?php echo base_url(); ?>assets/front/assets/img/social//yahoo_openid_connect.png" alt="yahoo open id" border="0"/>
                                </a>
                            </td>				
                        </tr>
                    </table>
                    -->
                </div>
            </div>       	
            <!--contact form-->

        </div><!--contact_form-->

    </div><!--container-->
</section> <!--contact-->
<p id="viewer-info"></p>
<div id="fb-root"></div>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
    FB.init({appId: "<?php echo $this->config->item('facebook_app_id'); ?>", status: true, cookie: true, xfbml: true});
    FB.Event.subscribe('auth.sessionChange', function (response) {
        if (response.session)
        {
            // A user has logged in, and a new cookie has been saved
            //window.location.reload(true);
        }
        else
        {
            // The user has logged out, and the cookie has been cleared
        }
    });
</script>