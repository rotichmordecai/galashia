<?php
$login = array(
    'name' => 'login',
    'id' => 'login',
    'value' => set_value('login'),
    'maxlength' => 80,
    'size' => 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
    $login_label = 'Email or login';
} else {
    $login_label = 'Email';
}
?>

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo isset($title) ? $title : NULL; ?></h1>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php echo isset($breadcrumbs) ? $breadcrumbs : NULL; ?>

            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>



<!--===========================
       contact
 ==============================-->   
<section id="contact">

    <div class="container">
        <?php echo $this->message->display(); ?>
        <div class="clearfix"></div>
        <div class="contact_form">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">                        
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">  
                    <?php echo form_open($this->uri->uri_string()); ?>
                    <?php echo form_label($login_label, $login['id']); ?>
                    <?php echo form_input($login); ?>
                    <p style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']]) ? $errors[$login['name']] : ''; ?></p>
                    <?php echo form_submit('reset', 'Get a new password'); ?>
                    <?php echo form_close(); ?>
                </div>
            </div>       	
            <!--contact form-->

        </div><!--contact_form-->

    </div><!--container-->


</section> <!--contact-->