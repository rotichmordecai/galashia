<div id="home">
    <div class="container">
        <div class="row slider-band"> 
            <!-- START REVOLUTION SLIDER 2.3.91 -->
            <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;max-height:373px;">
                <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:373px;height:373;">
                    <ul>  
                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" >
                            <div class="tp-caption fade"  
                                 data-x="0" 
                                 data-y="0" 
                                 data-speed="300" 
                                 data-start="500" 
                                 data-easing="easeOutExpo"  ><img src="<?php echo site_url('assets/front/assets/img/slider/img8.png'); ?>" alt="Image 2" style="width: 293px;"></div>
                            <div class="tp-caption fade"  
                                 data-x="293" 
                                 data-y="0" 
                                 data-speed="300" 
                                 data-start="800" 
                                 data-easing="easeOutExpo"  ><img src="<?php echo site_url('assets/front/assets/img/slider/img9.png'); ?>" alt="Image 3"></div>
                            <div class="tp-caption sixguin fade"  
                                 data-x="45" 
                                 data-y="38" 
                                 data-speed="300" 
                                 data-start="1400" 
                                 data-easing="easeOutExpo" 
                                 style="line-height: 25px;">Your Professional <br /> Writing Partner</div>
                            <div class="tp-caption sixguinsecond fade"  
                                 data-x="45" 
                                 data-y="100" 
                                 data-speed="300" 
                                 data-start="1700" 
                                 data-easing="easeOutExpo"  >
                                Galashia offers full-service writing and <br /> 
                                editorial for print media, corporations <br />
                                and the Web. Our writers' background is <br />
                                in trade journalism with a focus on <br />
                                social media, technology, business, <br />
                                entertainment, finance, lifestyle, strategy, <br />
                                education, healthcare and numerous <br />
                                other industries...
                            </div>
                            <div class="tp-caption fade"  
                                 data-x="85" 
                                 data-y="270" 
                                 data-speed="1000" 
                                 data-start="2300" 
                                 data-easing="easeOutBack"  ><a href="#"><img src="<?php echo site_url('assets/front/assets/img/slider/img6.png'); ?>" alt="Image 8"></a></div>
                            <div class="tp-caption sixguinsecond fade"  
                                 data-x="104" 
                                 data-y="280" 
                                 data-speed="300" 
                                 data-start="2600" 
                                 data-easing="easeOutExpo"  >
                                <a href="<?php echo site_url('page/aboutus#socialmedia'); ?>" style="color: white;">Learn More →</a> 
                            </div>
                        </li>
                        <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-fstransition="fade" data-fsmasterspeed="300" data-fsslotamount="7"> 
                            <div class="tp-caption fade"  
                                 data-x="0" 
                                 data-y="0" 
                                 data-speed="300" 
                                 data-start="500" 
                                 data-easing="easeOutExpo"  ><img src="<?php echo site_url('assets/front/assets/img/slider/img8.png'); ?>" alt="Image 2" style="width: 293px;"></div>
                            <div class="tp-caption fade"  
                                 data-x="293" 
                                 data-y="0" 
                                 data-speed="300" 
                                 data-start="800" 
                                 data-easing="easeOutExpo"  ><img src="<?php echo site_url('assets/front/assets/img/slider/bg_2.png'); ?>" alt="Image 3"></div>
                            <div class="tp-caption sixguin fade"  
                                 data-x="45" 
                                 data-y="38" 
                                 data-speed="300" 
                                 data-start="1400" 
                                 data-easing="easeOutExpo" 
                                 style="line-height: 25px;">Your Professional <br /> Writing Partner</div>
                            <div class="tp-caption sixguinsecond fade"  
                                 data-x="45" 
                                 data-y="100"  
                                 data-speed="300" 
                                 data-start="1700" 
                                 data-easing="easeOutExpo"  >
                                Galashia offers full-service writing and <br /> 
                                editorial for print media, corporations <br />
                                and the Web. Our writers' background is <br />
                                in trade journalism with a focus on <br />
                                social media, technology, business, <br />
                                entertainment, finance, lifestyle, strategy, <br />
                                education, healthcare and numerous <br />
                                other industries...
                            </div>
                            <div class="tp-caption fade"  
                                 data-x="85" 
                                 data-y="270" 
                                 data-speed="1000" 
                                 data-start="2300" 
                                 data-easing="easeOutBack"  ><a href="#"><img src="<?php echo site_url('assets/front/assets/img/slider/img6.png'); ?>" alt="Image 8"></a></div>
                            <div class="tp-caption sixguinsecond fade"  
                                 data-x="104" 
                                 data-y="280" 
                                 data-speed="300" 
                                 data-start="2600" 
                                 data-easing="easeOutExpo"  >
                                <a href="<?php echo site_url('page/aboutus#socialmedia'); ?>" style="color: white;">Learn More →</a> 
                            </div>
                        </li>
                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" >                        
                            <div class="tp-caption fade"  
                                 data-x="0" 
                                 data-y="0" 
                                 data-speed="300" 
                                 data-start="500" 
                                 data-easing="easeOutExpo"  ><img src="<?php echo site_url('assets/front/assets/img/slider/img8.png'); ?>" alt="Image 2" style="width: 293px;"></div>
                            <div class="tp-caption fade"  
                                 data-x="293" 
                                 data-y="0" 
                                 data-speed="300" 
                                 data-start="800" 
                                 data-easing="easeOutExpo"  ><img src="<?php echo site_url('assets/front/assets/img/slider/img1.png'); ?>" alt="Image 3"></div>
                            <div class="tp-caption sixguin fade"  
                                 data-x="45" 
                                 data-y="38" 
                                 data-speed="300" 
                                 data-start="1400" 
                                 data-easing="easeOutExpo" 
                                 style="line-height: 25px;">Your Professional <br /> Writing Partner</div>
                            <div class="tp-caption sixguinsecond fade"  
                                 data-x="45" 
                                 data-y="100"  
                                 data-speed="300" 
                                 data-start="1700" 
                                 data-easing="easeOutExpo"  >
                                Galashia offers full-service writing and <br /> 
                                editorial for print media, corporations <br />
                                and the Web. Our writers' background is <br />
                                in trade journalism with a focus on <br />
                                social media, technology, business, <br />
                                entertainment, finance, lifestyle, strategy, <br />
                                education, healthcare and numerous <br />
                                other industries...
                            </div>
                            <div class="tp-caption fade"  
                                 data-x="85" 
                                 data-y="270" 
                                 data-speed="1000" 
                                 data-start="2300" 
                                 data-easing="easeOutBack"  ><a href="#"><img src="<?php echo site_url('assets/front/assets/img/slider/img6.png'); ?>" alt="Image 8"></a></div>
                            <div class="tp-caption sixguinsecond fade"  
                                 data-x="104" 
                                 data-y="280" 
                                 data-speed="300" 
                                 data-start="2600" 
                                 data-easing="easeOutExpo"  >
                                <a href="<?php echo site_url('page/aboutus#socialmedia'); ?>" style="color: white;">Learn More →</a> 
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>   
    </div>
</div>
<!--static-wrapper ends--> 



<div class="clearfix"></div>




<!--=========================================
                                Services
  =========================================-->   
<section id="services" class="style5">
    <div class="container">
        <div class="row">
            <div class="service">
                <span class="icon-pencil"></span>
                <h3>Writing</h3>
                <p>
                    We write superb articles, blog posts, magazine and newspaper content, reports or eBooks on the pressing social media issues of the day, e-learning and education materials and any other topic of your specification. Our writing is tailored exactly to your needs and is relevant to your brand and ... 
                </p>
                <a href="<?php echo site_url('page/about#writing'); ?>">Learn More →</a> 
            </div>  

            <div class="service">
                <span class="icon-wrench"></span>
                <h3>Social Media Management</h3>
                <p>
                    We have experience in social media management and content creation for businesses, companies, individuals and co-operations. We are trusted with monitoring, contributing to, and filtering, measuring and otherwise guiding the social media presence of a brand, product, individual or corporation.
                </p>
                <a href="<?php echo site_url('page/about#socialmedia'); ?>">Learn More →</a> 
            </div>   

            <div class="service">
                <span class="icon-globe"></span>
                <h3>Content Marketing</h3>
                <p>
                    It’s clear that traditional marketing is becoming less and less effective by the minute, and that there has to be a better way. That’s where content marketing comes in, it’s a technique of creating and distributing valuable, relevant and consistent content to attract and acquire a clearly defined ...
                </p>
                <a href="<?php echo site_url('page/about#contentmarketing'); ?>">Learn More →</a> 
            </div>   

        </div><!--row-->

    </div>  <!--container ends--> 
</section><!--services ends-->
<?php if (isset($results) && is_array($results)): ?>
    <div class="from-blog">
        <div class="container">
            <h3>from Our Recent Articles</h3>
            <div class="row">
                <?php foreach ($results as $result): ?>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <article class="post-short">
                            <div class="from-blog-wrapper">
                                <span class=" icon-align-center"></span>
                                <h4><?php echo date("d", strtotime($result->updated)); ?><span><?php echo date("M", strtotime($result->updated)); ?></span></h4>
                            </div>

                            <h5><?php echo substr(strip_tags(htmlspecialchars_decode(html_entity_decode($result->title))), 0, 19) . (strlen($result->title) > 19 ? ' ... ' : ''); ?></h5>
                            <h6> <?php echo $result->category_title; ?></h6>
                            <p>
                                <?php echo substr(strip_tags(htmlspecialchars_decode(html_entity_decode($result->content))), 0, 60) . ' ... '; ?>
                            </p>
                            <a class="readmore" href="<?php echo site_url('article/view/' . $result->id . '_' . url_title($result->title)); ?>">Read more →</a>
                        </article>
                        <div class="clearfix"></div>    
                    </div>
                <?php endforeach; ?>
            </div><!--row-->
        </div><!--container ends-->
    </div><!--from blog ends-->
<?php endif; ?>
<!--=================================
Clients
=================================-->
<!--
<div class="clearfix"></div>
<section id="clients-container">
    <div class="container">

        <h3>Clients</h3> 
        <div class="clients">

            <a href="#" class="client">
                <img src="<?php echo site_url('assets/front/assets/img/clients/1.png'); ?>" alt="" />
            </a>

            <a href="#" class="client">
                <img src="<?php echo site_url('assets/front/assets/img/clients/2.png'); ?>" alt="" />
            </a>

            <a href="#" class="client">
                <img src="<?php echo site_url('assets/front/assets/img/clients/3.png'); ?>" alt="" />
            </a>

            <a href="#" class="client">
                <img src="<?php echo site_url('assets/front/assets/img/clients/4.png'); ?>" alt="" />
            </a>

            <a href="#" class="client">
                <img src="<?php echo site_url('assets/front/assets/img/clients/5.png'); ?>" alt="" />
            </a>

        </div>
        <div class="clearfix"></div>
    </div>
</section>
-->
<div class="clearfix"></div>


<script type="text/javascript">

    var tpj = jQuery;
    tpj.noConflict();
    var revapi1;
    tpj(document).ready(function() {
        if (tpj.fn.cssOriginal != undefined)
            tpj.fn.css = tpj.fn.cssOriginal;
        if (tpj('#rev_slider_1_1').revolution == undefined)
            revslider_showDoubleJqueryError('#rev_slider_1_1');
        else
            revapi1 = tpj('#rev_slider_1_1').show().revolution(
                    {
                        delay: 9000,
                        startwidth: 960,
                        startheight: 373,
                        hideThumbs: 200,
                        thumbWidth: 100,
                        thumbHeight: 50,
                        thumbAmount: 3,
                        navigationType: "none",
                        navigationArrows: "solo",
                        navigationStyle: "round",
                        touchenabled: "on",
                        onHoverStop: "on",
                        navigationHAlign: "center",
                        navigationVAlign: "bottom",
                        navigationHOffset: 0,
                        navigationVOffset: 20,
                        soloArrowLeftHalign: "right",
                        soloArrowLeftValign: "bottom",
                        soloArrowLeftHOffset: 39,
                        soloArrowLeftVOffset: 0,
                        soloArrowRightHalign: "right",
                        soloArrowRightValign: "bottom",
                        soloArrowRightHOffset: 0,
                        soloArrowRightVOffset: 0,
                        shadow: 0,
                        fullWidth: "on",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        startWithSlide: 0
                    });

    });	//ready

</script> 

<!-- END REVOLUTION SLIDER --> 