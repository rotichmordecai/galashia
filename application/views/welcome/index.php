
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Galashia - Your professional content development partner</title>
        <meta name="description" content="Galashia offers professional writing service for individuals, print media, businesses, companies, corporations and the Web. We also do social media management">
        <meta name="keywords" content="Galashia,Galashia.com,galashia limited,content,writing,freelance,web content
              development,content marketing,writing service,web content,social media
              content,social media management,content development,social media content
              writing,creative writing,professional content writing,dissertation writing,essay
              writing,academic papers,term papers,assignments">
        <meta name="author" content="Galashia.com">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- Web Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo site_url('assets/homepage/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">

        <!-- Font Awesome CSS -->
        <link href="<?php echo site_url('assets/homepage/fonts/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">

        <!-- Plugins -->
        <link href="<?php echo site_url('assets/homepage/css/animations.css'); ?>" rel="stylesheet">

        <!-- Worthy core CSS file -->
        <link href="<?php echo site_url('assets/homepage/css/style.css'); ?>" rel="stylesheet">

        <!-- Custom css --> 
        <link href="<?php echo site_url('assets/homepage/css/custom.css'); ?>" rel="stylesheet">
    </head>

    <body class="no-trans">
        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop"><i class="icon-up-open-big"></i></div>

        <!-- header start -->
        <!-- ================ --> 
        <header class="header fixed clearfix navbar navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">

                        <!-- header-left start -->
                        <!-- ================ -->
                        <div class="header-left clearfix">

                            <!-- logo -->

                            <div class="logo smooth-scroll">
                                <a href="#banner"><img id="logo" src="assets/homepage/images/logo.png" alt="Worthy"></a>
                            </div>

                            <!-- name-and-slogan -->
                            <div class="site-name-and-slogan smooth-scroll">
                                <div class="site-name"><a href="#banner" style="color: red;">Galashia</a></div>
                                <div class="site-slogan">Content Development</div>
                            </div>

                        </div>
                        <!-- header-left end -->

                    </div>
                    <div class="col-md-8">

                        <!-- header-right start -->
                        <!-- ================ -->
                        <div class="header-right clearfix">

                            <!-- main-navigation start -->
                            <!-- ================ -->
                            <div class="main-navigation animated">

                                <!-- navbar start -->
                                <!-- ================ -->
                                <nav class="navbar navbar-default" role="navigation">
                                    <div class="container-fluid">

                                        <!-- Toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
                                            <ul class="nav navbar-nav navbar-right">
                                                <li class="active"><a href="<?php echo site_url(); ?>">Home <i class="icon-caret-down"></i></a></li>
                                                <li><a href="<?php echo site_url('page/about'); ?>" > About Us <i class="icon-caret-down"></i></a></li>
                                                <li><a href="<?php echo site_url('articles'); ?>" >Articles <i class="icon-caret-down"></i></a>
                                                    <?php show_menu_view($categories, 0); ?>
                                                </li>
                                                <li><a href="<?php echo site_url('page/contactus'); ?>" >Contact Us <i class="icon-caret-down"></i></a></li>
                                                <?php if ($this->tank_auth->is_logged_in()): ?>
                                                    <li>
                                                        <a href="<?php echo site_url('profile/update'); ?>" >My Account <i class="icon-caret-down"></i></a>
                                                    </li>
                                                <?php else: ?>
                                                    <li><a href="<?php echo site_url('auth/login'); ?>" >Login <i class="icon-caret-down"></i></a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>

                                    </div>
                                </nav>
                                <!-- navbar end -->

                            </div>
                            <!-- main-navigation end -->

                        </div>
                        <!-- header-right end -->

                    </div>
                </div>
            </div>
        </header>
        <!-- header end -->

        <!-- banner start -->
        <!-- ================ -->
        <div id="banner" class="banner">
            <div class="banner-image"></div>
            <div class="banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 object-non-visible" data-animation-effect="fadeIn">
                            <h1 class="text-center">Society of  <span>Content developers</span></h1>
                            <p class="lead text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos debitis provident nulla illum minus enim praesentium repellendus ullam cupiditate reiciendis optio voluptatem, recusandae nobis quis aperiam, sapiente libero ut at.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->

        <!-- .subfooter start -->
        <!-- ================ -->
        <div class="subfooter">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">Copyright © 2015 Galashia.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- .subfooter end -->

    </footer>
    <!-- footer end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster
    ================================================== -->
    <!-- Jquery and Bootstap core js files -->
    <script type="text/javascript" src="<?php echo site_url('assets/homepage/plugins/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/homepage/bootstrap/js/bootstrap.min.js') ?>"></script>

    <!-- Modernizr javascript -->
    <script type="text/javascript" src="<?php echo site_url('assets/homepage/plugins/modernizr.js') ?>"></script>

    <!-- Isotope javascript -->
    <script type="text/javascript" src="<?php echo site_url('assets/homepage/plugins/isotope/isotope.pkgd.min.js') ?>"></script>

    <!-- Backstretch javascript -->
    <script type="text/javascript" src="<?php echo site_url('assets/homepage/plugins/jquery.backstretch.min.js') ?>"></script>

    <!-- Appear javascript -->
    <script type="text/javascript" src="<?php echo site_url('assets/homepage/plugins/jquery.appear.js') ?>"></script>

    <!-- Initialization of Plugins -->
    <script type="text/javascript" src="<?php echo site_url('assets/homepage/js/template.js') ?>"></script>

    <!-- Custom Scripts -->
    <script type="text/javascript" src="<?php echo site_url('assets/homepage/js/custom.js') ?>"></script>

</body>
</html>
