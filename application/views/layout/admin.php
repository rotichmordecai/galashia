<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Dashboard - Galashia</title>

        <?php echo $_styles; ?>
        <?php echo $_scripts; ?>

        <script type="text/javascript" src="<?php echo site_url('assets/tinymce/tinymce.min.js'); ?>"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "textarea"
            });
        </script>
        <!-- Page Specific CSS -->
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <link href="<?php echo site_url('assets/fa/css/font-awesome.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/front/assets/css/bootstrap-social.css'); ?>" rel="stylesheet">
    </head>

    <body>

        <div id="wrapper">

            <!-- Sidebar -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo site_url('admin'); ?>">Galashia Admin</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active"><a href="<?php echo site_url('admin/index'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="dropdown">
                            <a href="<?php echo site_url('admin/users'); ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Users <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('admin/add_user'); ?>">Add New</a></li>
                                <li><a href="<?php echo site_url('admin/view_users'); ?>">Browse</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="<?php echo site_url('admin/categories'); ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Categories <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('admin/add_category'); ?>">Add New</a></li>
                                <li><a href="<?php echo site_url('admin/view_categories'); ?>">Browse</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="<?php echo site_url('admin/articles'); ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Articles <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('admin/add_article'); ?>">Add New</a></li>
                                <li><a href="<?php echo site_url('admin/view_articles'); ?>">Browse</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="<?php echo site_url('admin/clients'); ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Clients <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('admin/add_client'); ?>">Add New</a></li>
                                <li><a href="<?php echo site_url('admin/view_clients'); ?>">Browse</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo site_url('admin/settings'); ?>"><i class="fa fa-bar-chart-o"></i> Settings</a></li>                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right navbar-user">
                        <li class="dropdown user-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->tank_auth->get_username(); ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('admin/settings'); ?>"><i class="fa fa-gear"></i> Settings</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo site_url('auth/logout?admin=true'); ?>"><i class="fa fa-power-off"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <?php echo $this->message->display(); ?>
                <?php echo $content; ?>
            </div><!-- /#page-wrapper -->

        </div><!-- /#wrapper -->        

        <!-- Page Specific Plugins -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>

    </body>
</html>


