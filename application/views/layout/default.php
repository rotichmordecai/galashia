<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html ng-app="galashia" lang="en" class="no-js>
      <!--<![endif]-->
      <head>
      <meta charset="utf-8">
      <title>Galashia - Your professional content development partner</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo site_url('assets/front/assets/img/basic/favicon.ico'); ?>"/>
    <!--
    =================================
    Meta tags
    =================================
    -->
    <meta name="description" content="Galashia offers professional writing service for individuals, print media, businesses, companies, corporations and the Web. We also do social media management">
    <meta name="keywords" content="Galashia,Galashia.com,galashia limited,content,writing,freelance,web content
          development,content marketing,writing service,web content,social media
          content,social media management,content development,social media content
          writing,creative writing,professional content writing,dissertation writing,essay
          writing,academic papers,term papers,assignments">
    <meta name="author" content="Galashia.com">
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=1, user-scalable=no" />
    <!--
    =================================
    Style Sheets
    =================================
    -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    <?php echo $_styles; ?>
    <?php echo $_scripts; ?>

    <script type="text/javascript" src="<?php echo site_url('assets/tinymce/tinymce.min.js'); ?>"></script>
    <script type="text/javascript">

        function site_url(segment) {
            return location.protocol + "//" + location.host + "/" + segment;
        }

        tinymce.init({
            selector: "textarea",
        });

    </script>

    <script type="text/javascript">
        var galashia = angular.module('galashia', []);
    </script>

    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "0f8fc789-06fc-4091-964b-794538c5b165", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

    <link href="<?php echo site_url('assets/fa/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo site_url('assets/front/assets/css/bootstrap-social.css'); ?>" rel="stylesheet">

</head>
<body>
    <!--
    =========================================
                                    Navigation
    =========================================
    -->   
    <header> 
        <div class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">

                    <a class="navbar-brand" href="<?php echo site_url(''); ?>"><img src="<?php echo site_url('assets/front/assets/img/basic/logo.png'); ?>" alt="" /></a>
                </div>
                <div class="nav-links">
                    <ul class="nav navbar-nav">

                        <li class=" dropdown"><a href="<?php echo site_url(); ?>">Home <i class="icon-caret-down"></i></a></li>
                        <li class="dropdown"><a href="<?php echo site_url('page/about'); ?>" > About Us <i class="icon-caret-down"></i></a></li>
                        <li class="dropdown"><a href="<?php echo site_url('page/contactus'); ?>" >Contact Us <i class="icon-caret-down"></i></a></li>

                        <?php if ($this->tank_auth->is_logged_in()): ?>
                            <li class="dropdown"><a href="#" >My Account <i class="icon-caret-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#"><?php echo $this->tank_auth->get_username(); ?></a></li>
                                    <li><a href="<?php echo site_url('p/' . $this->tank_auth->get_username()); ?>">My Profile</a></li>
                                    <li><a href="<?php echo site_url('profile/update'); ?>">Update Profile</a></li>
                                    <li><a href="<?php echo site_url('profile/articles'); ?>">My Articles</a></li>
                                    <li><a href="<?php echo site_url('article/post'); ?>">Post Article</a></li>
                                    <li><a href="<?php echo site_url('auth/logout'); ?>"> Sign Out!</a></li>
                                </ul>
                            </li>

                        <?php else: ?>
                            <li class="dropdown"><a href="<?php echo site_url('auth/login'); ?>" >Login <i class="icon-caret-down"></i></a></li>
                        <?php endif; ?>

                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

    </header>  
    <!--=========================================
                                   Static-Head	
     =========================================-->   
    <?php echo $content; ?>

    <!--=========================================================
#
#							Footer
#==========================================================-->
    <div class="clearfix"></div>

    <footer class="layout-footer">


        <ul class="container copyrights row-fluid">

            <li>
                <span class="detail">&copy; <?php echo date('Y'); ?> Galashia. All rights reserved</span>
            </li>

            <li>
                <span class="detail"><span class="icon-home detail-icon" ></span>Daystar University. Nairobi, Kenya</span>
            </li>

            <li>
                <span class="detail"><span class="icon-phone detail-icon" ></span> +(254) 714-384 705 </span>
            </li>

            <li>
                <span class="detail"><span class="icon-envelope-alt detail-icon"></span> customercare@galashia.com</span>
            </li>

            <li>
                <span class="detail"><span class="icon-bell detail-icon" ></span><a href="https://plus.google.com/110182962990447954736" rel="publisher" target="_blank">Google+</a></span>

            </li>
        </ul> 
    </footer><!--footer ends-->
</body>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-51665040-1', 'galashia.com');
    ga('send', 'pageview');

</script>
</html>
