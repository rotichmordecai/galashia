<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        Category_model
 * Creation date:  24.05.2014
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Category_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $category_id;   // DataType: int(11)
    private $title;   // DataType: varchar(75)
    private $updated;   // DataType: timestamp
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_category_id() {
        return $this->category_id;
    }

    function get_title() {
        return $this->title;
    }

    function get_updated() {
        return $this->updated;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_category_id($value) {
        $this->category_id = $value;
    }

    function set_title($value) {
        $this->title = $value;
    }

    function set_updated($value) {
        $this->updated = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->category_id = $row->category_id;
        $this->title = $row->title;
        $this->updated = $row->updated;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all category
    // **********************

    function select($criteria = null) {

        $this->db->select('category.*');
        $this->db->select('parent.title as parent_title');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $this->db->join('category as parent', 'parent.id = category.category_id', 'left');
        $query = $this->db->get('category');
        return $query->result();
    }

    // **********************
    // Get category by id
    // **********************

    function get_category($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('category');

        foreach ($query->result() as $category) {
            $this->init($category);
            return $category;
        }
    }

    // **********************
    // Delete category
    // **********************

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete('category');
        return $this->db->affected_rows();
    }

    // **********************
    // Insert category
    // **********************

    function insert() {

        if (isset($this->category_id) && !empty($this->category_id))
            $data['category_id'] = $this->category_id;
        if (isset($this->title) && !empty($this->title))
            $data['title'] = $this->title;
        if (isset($this->updated) && !empty($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status) && !empty($this->status))
            $data['status'] = $this->status;

        $this->db->insert('category', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update category
    // **********************

    function update() {

        $data = array();

        if (isset($this->category_id) && !empty($this->category_id))
            $data['category_id'] = $this->category_id;
        if (isset($this->title) && !empty($this->title))
            $data['title'] = $this->title;
        if (isset($this->updated) && !empty($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->where('id', $this->id);
        $this->db->update('category', $data);
        return $this->db->affected_rows();
    }

    // **********************
    // Fetch records
    //
    
    function fetch($limit, $start) {

        $this->db->select('category.*');
        $this->db->select('parent.title as parent_title');
        $this->db->limit($limit, $start);
        $this->db->join('category as parent', 'parent.id = category.category_id', 'left');
        $query = $this->db->get("category");

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function count($criteria = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            $this->db->where($criteria);
        }
        $this->db->from('category');
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>