<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        article_model
 * Creation date:  01.08.2014
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class article_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $user_id;   // DataType: int(11)
    private $category_id;   // DataType: int(11)
    private $client_id;   // DataType: int(11)
    private $title;   // DataType: varchar(75)
    private $image;   // DataType: varchar(75)
    private $content;   // DataType: text
    private $fb_msg;   // DataType: varchar(45)
    private $tw_msg;   // DataType: varchar(255)
    private $li_msg;   // DataType: varchar(255)
    private $gp_msg;   // DataType: varchar(255)
    private $tagline;   // DataType: varchar(45)
    private $description;   // DataType: varchar(255)
    private $keywords;   // DataType: varchar(125)
    private $author;   // DataType: varchar(75)
    private $updated;   // DataType: timestamp
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_user_id() {
        return $this->user_id;
    }

    function get_category_id() {
        return $this->category_id;
    }

    function get_client_id() {
        return $this->client_id;
    }

    function get_title() {
        return $this->title;
    }

    function get_image() {
        return $this->image;
    }

    function get_content() {
        return $this->content;
    }

    function get_fb_msg() {
        return $this->fb_msg;
    }

    function get_tw_msg() {
        return $this->tw_msg;
    }

    function get_li_msg() {
        return $this->li_msg;
    }

    function get_gp_msg() {
        return $this->gp_msg;
    }

    function get_tagline() {
        return $this->tagline;
    }

    function get_description() {
        return $this->description;
    }

    function get_keywords() {
        return $this->keywords;
    }

    function get_author() {
        return $this->author;
    }

    function get_updated() {
        return $this->updated;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_user_id($value) {
        $this->user_id = $value;
    }

    function set_category_id($value) {
        $this->category_id = $value;
    }

    function set_client_id($value) {
        $this->client_id = $value;
    }

    function set_title($value) {
        $this->title = $value;
    }

    function set_image($value) {
        $this->image = $value;
    }

    function set_content($value) {
        $this->content = $value;
    }

    function set_fb_msg($value) {
        $this->fb_msg = $value;
    }

    function set_tw_msg($value) {
        $this->tw_msg = $value;
    }

    function set_li_msg($value) {
        $this->li_msg = $value;
    }

    function set_gp_msg($value) {
        $this->gp_msg = $value;
    }

    function set_tagline($value) {
        $this->tagline = $value;
    }

    function set_description($value) {
        $this->description = $value;
    }

    function set_keywords($value) {
        $this->keywords = $value;
    }

    function set_author($value) {
        $this->author = $value;
    }

    function set_updated($value) {
        $this->updated = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->user_id = $row->user_id;
        $this->category_id = $row->category_id;
        $this->client_id = $row->client_id;
        $this->title = $row->title;
        $this->image = $row->image;
        $this->content = $row->content;
        $this->fb_msg = $row->fb_msg;
        $this->tw_msg = $row->tw_msg;
        $this->li_msg = $row->li_msg;
        $this->gp_msg = $row->gp_msg;
        $this->tagline = $row->tagline;
        $this->description = $row->description;
        $this->keywords = $row->keywords;
        $this->author = $row->author;
        $this->updated = $row->updated;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all article
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('article');
        return $query->result();
    }

    // **********************
    // Get article by id
    // **********************

    function get_article($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('article');

        foreach ($query->result() as $article) {
            $this->init($article);
            return $article;
        }
    }

    // **********************
    // Delete article
    // **********************

    public function delete($criteria = null) {
        if ($criteria != null) {
            $this->db->delete('article', array($criteria));
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('article');
            return $this->db->affected_rows();
        }
        return 0;
    }

    // **********************
    // Insert article
    // **********************

    function insert() {
        if (isset($this->user_id))
            $data['user_id'] = $this->user_id;
        if (isset($this->category_id))
            $data['category_id'] = $this->category_id;
        if (isset($this->client_id))
            $data['client_id'] = $this->client_id;
        if (isset($this->title))
            $data['title'] = $this->title;
        if (isset($this->image))
            $data['image'] = $this->image;
        if (isset($this->content))
            $data['content'] = $this->content;
        if (isset($this->fb_msg))
            $data['fb_msg'] = $this->fb_msg;
        if (isset($this->tw_msg))
            $data['tw_msg'] = $this->tw_msg;
        if (isset($this->li_msg))
            $data['li_msg'] = $this->li_msg;
        if (isset($this->gp_msg))
            $data['gp_msg'] = $this->gp_msg;
        if (isset($this->tagline))
            $data['tagline'] = $this->tagline;
        if (isset($this->description))
            $data['description'] = $this->description;
        if (isset($this->keywords))
            $data['keywords'] = $this->keywords;
        if (isset($this->author))
            $data['author'] = $this->author;
        if (isset($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('article', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update article
    // **********************

    function update($criteria = null) {
        if (isset($this->id))
            $data['id'] = $this->id;
        if (isset($this->user_id))
            $data['user_id'] = $this->user_id;
        if (isset($this->category_id))
            $data['category_id'] = $this->category_id;
        if (isset($this->client_id))
            $data['client_id'] = $this->client_id;
        if (isset($this->title))
            $data['title'] = $this->title;
        if (isset($this->image))
            $data['image'] = $this->image;
        if (isset($this->content))
            $data['content'] = $this->content;
        if (isset($this->fb_msg))
            $data['fb_msg'] = $this->fb_msg;
        if (isset($this->tw_msg))
            $data['tw_msg'] = $this->tw_msg;
        if (isset($this->li_msg))
            $data['li_msg'] = $this->li_msg;
        if (isset($this->gp_msg))
            $data['gp_msg'] = $this->gp_msg;
        if (isset($this->tagline))
            $data['tagline'] = $this->tagline;
        if (isset($this->description))
            $data['description'] = $this->description;
        if (isset($this->keywords))
            $data['keywords'] = $this->keywords;
        if (isset($this->author))
            $data['author'] = $this->author;
        if (isset($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('article', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('article', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {
        $this->db->select('*');
        $this->db->from('article');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('article.*, user.username');
        $this->db->select('category.title as category_title');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('article');
        $this->db->join('category', 'category.id = article.category_id');
        $this->db->join('user', 'user.id = article.user_id');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL && is_array($order_by)) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('article');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL && is_array($order_by)) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id', $search);
        $this->db->or_like('category_id', $search);
        $this->db->or_like('client_id', $search);
        $this->db->or_like('title', $search);
        $this->db->or_like('image', $search);
        $this->db->or_like('content', $search);
        $this->db->or_like('fb_msg', $search);
        $this->db->or_like('tw_msg', $search);
        $this->db->or_like('li_msg', $search);
        $this->db->or_like('gp_msg', $search);
        $this->db->or_like('tagline', $search);
        $this->db->or_like('description', $search);
        $this->db->or_like('keywords', $search);
        $this->db->or_like('author', $search);
        $this->db->or_like('updated', $search);
        $this->db->or_like('status', $search);
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('article');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL && is_array($order_by)) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        $this->db->like('user_id', $search);
        $this->db->or_like('category_id', $search);
        $this->db->or_like('client_id', $search);
        $this->db->or_like('title', $search);
        $this->db->or_like('image', $search);
        $this->db->or_like('content', $search);
        $this->db->or_like('fb_msg', $search);
        $this->db->or_like('tw_msg', $search);
        $this->db->or_like('li_msg', $search);
        $this->db->or_like('gp_msg', $search);
        $this->db->or_like('tagline', $search);
        $this->db->or_like('description', $search);
        $this->db->or_like('keywords', $search);
        $this->db->or_like('author', $search);
        $this->db->or_like('updated', $search);
        $this->db->or_like('status', $search);
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('article');
        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL && is_array($order_by)) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>