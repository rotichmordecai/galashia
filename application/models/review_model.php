<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        review_model
 * Creation date:  09.01.2015
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Review_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $user_id1;   // DataType: int(11)
    private $user_id2;   // DataType: int(11)
    private $rating;   // DataType: int(11)
    private $review;   // DataType: varchar(255)
    private $modified;   // DataType: timestamp
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_user_id1() {
        return $this->user_id1;
    }

    function get_user_id2() {
        return $this->user_id2;
    }

    function get_rating() {
        return $this->rating;
    }

    function get_modified() {
        return $this->modified;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_user_id1($value) {
        $this->user_id1 = $value;
    }

    function set_user_id2($value) {
        $this->user_id2 = $value;
    }

    function set_rating($value) {
        $this->rating = $value;
    }

    function set_review($value) {
        $this->review = $value;
    }

    function set_modified($value) {
        $this->modified = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {

        $this->id = $row->id;
        $this->user_id1 = $row->user_id1;
        $this->user_id2 = $row->user_id2;
        $this->rating = $row->rating;
        $this->review = $row->review;
        $this->modified = $row->modified;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all review
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('review');
        return $query->result();
    }

    // **********************
    // Get review by id
    // **********************

    function get_review($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('review');

        foreach ($query->result() as $review) {
            $this->init($review);
            return $review;
        }
    }

    // **********************
    // Delete review
    // **********************

    public function delete($criteria = null) {

        if ($criteria != null) {
            $this->db->delete('review', array($criteria));
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('review');
            return $this->db->affected_rows();
        }
        return 0;
    }

    // **********************
    // Insert review
    // **********************

    function insert() {

        if (isset($this->user_id1))
            $data['user_id1'] = $this->user_id1;
        if (isset($this->user_id2))
            $data['user_id2'] = $this->user_id2;
        if (isset($this->rating))
            $data['rating'] = $this->rating;
        if (isset($this->review))
            $data['review'] = $this->review;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('review', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update review
    // **********************

    function update($criteria = null) {

        if (isset($this->id))
            $data['id'] = $this->id;
        if (isset($this->user_id1))
            $data['user_id1'] = $this->user_id1;
        if (isset($this->user_id2))
            $data['user_id2'] = $this->user_id2;
        if (isset($this->rating))
            $data['rating'] = $this->rating;
        if (isset($this->review))
            $data['review'] = $this->review;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('review', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('review', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {

        $this->db->select('*');
        $this->db->from('review');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('review');
        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('review');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id1', $search);
        $this->db->or_like('user_id2', $search);
        $this->db->or_like('rating', $search);
        $this->db->or_like('review', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('review');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id1', $search);
        $this->db->or_like('user_id2', $search);
        $this->db->or_like('rating', $search);
        $this->db->or_like('review', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('review');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>