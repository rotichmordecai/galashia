<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        message_thread_model
 * Creation date:  09.01.2015
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Message_thread_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $title;   // DataType: varchar(75)
    private $modified;   // DataType: timestamp
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_title() {
        return $this->title;
    }

    function get_modified() {
        return $this->modified;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_title($value) {
        $this->title = $value;
    }

    function set_modified($value) {
        $this->modified = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->title = $row->title;
        $this->modified = $row->modified;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all message_thread
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('message_thread');
        return $query->result();
    }

    // **********************
    // Get message_thread by id
    // **********************

    function get_message_thread($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('message_thread');

        foreach ($query->result() as $message_thread) {
            $this->init($message_thread);
            return $message_thread;
        }
    }

    // **********************
    // Delete message_thread
    // **********************

    public function delete($criteria = null) {

        if ($criteria != null) {
            $this->db->delete('message_thread', array($criteria));
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('message_thread');
            return $this->db->affected_rows();
        }

        return 0;
    }

    // **********************
    // Insert message_thread
    // **********************

    function insert() {

        if (isset($this->title))
            $data['title'] = $this->title;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('message_thread', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update message_thread
    // **********************

    function update($criteria = null) {

        if (isset($this->id))
            $data['id'] = $this->id;
        if (isset($this->title))
            $data['title'] = $this->title;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('message_thread', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('message_thread', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {
        $this->db->select('*');
        $this->db->from('message_thread');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('message_thread');
        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('message_thread');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('title', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('message_thread');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('title', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('message_thread');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>