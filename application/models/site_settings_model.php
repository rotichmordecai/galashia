<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        site_settings_model
 * Creation date:  16.07.2014
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Site_settings_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $name;   // DataType: varchar(45)
    private $title;   // DataType: varchar(45)
    private $tagline;   // DataType: varchar(45)
    private $description;   // DataType: varchar(255)
    private $keywords;   // DataType: varchar(125)
    private $author;   // DataType: varchar(75)
    private $updated;   // DataType: timestamp
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_name() {
        return $this->name;
    }

    function get_title() {
        return $this->title;
    }

    function get_tagline() {
        return $this->tagline;
    }

    function get_description() {
        return $this->description;
    }

    function get_keywords() {
        return $this->keywords;
    }

    function get_author() {
        return $this->author;
    }

    function get_updated() {
        return $this->updated;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_name($value) {
        $this->name = $value;
    }

    function set_title($value) {
        $this->title = $value;
    }

    function set_tagline($value) {
        $this->tagline = $value;
    }

    function set_description($value) {
        $this->description = $value;
    }

    function set_keywords($value) {
        $this->keywords = $value;
    }

    function set_author($value) {
        $this->author = $value;
    }

    function set_updated($value) {
        $this->updated = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->name = $row->name;
        $this->title = $row->title;
        $this->tagline = $row->tagline;
        $this->description = $row->description;
        $this->keywords = $row->keywords;
        $this->author = $row->author;
        $this->updated = $row->updated;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all site_settings
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('site_settings');
        return $query->result();
    }

    // **********************
    // Get site_settings by id
    // **********************

    function get_site_settings($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('site_settings');

        foreach ($query->result() as $site_settings) {
            $this->init($site_settings);
            return $site_settings;
        }
    }

    // **********************
    // Delete site_settings
    // **********************

    public function delete($criteria = null) {
        if ($criteria != null) {
            $this->db->delete('site_settings', array($criteria));
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('site_settings');
            return $this->db->affected_rows();
        }
        return 0;
    }

    // **********************
    // Insert site_settings
    // **********************

    function insert() {
        if (isset($this->name))
            $data['name'] = $this->name;
        if (isset($this->title))
            $data['title'] = $this->title;
        if (isset($this->tagline))
            $data['tagline'] = $this->tagline;
        if (isset($this->description))
            $data['description'] = $this->description;
        if (isset($this->keywords))
            $data['keywords'] = $this->keywords;
        if (isset($this->author))
            $data['author'] = $this->author;
        if (isset($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('site_settings', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update site_settings
    // **********************

    function update($criteria = null) {
        if (isset($this->id))
            $data['id'] = $this->id;
        if (isset($this->name))
            $data['name'] = $this->name;
        if (isset($this->title))
            $data['title'] = $this->title;
        if (isset($this->tagline))
            $data['tagline'] = $this->tagline;
        if (isset($this->description))
            $data['description'] = $this->description;
        if (isset($this->keywords))
            $data['keywords'] = $this->keywords;
        if (isset($this->author))
            $data['author'] = $this->author;
        if (isset($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('site_settings', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('site_settings', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {
        $this->db->select('*');
        $this->db->from('site_settings');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('site_settings');
        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('site_settings');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        $this->db->like('name', $search);
        $this->db->or_like('title', $search);
        $this->db->or_like('tagline', $search);
        $this->db->or_like('description', $search);
        $this->db->or_like('keywords', $search);
        $this->db->or_like('author', $search);
        $this->db->or_like('updated', $search);
        $this->db->or_like('status', $search);
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('site_settings');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        $this->db->like('name', $search);
        $this->db->or_like('title', $search);
        $this->db->or_like('tagline', $search);
        $this->db->or_like('description', $search);
        $this->db->or_like('keywords', $search);
        $this->db->or_like('author', $search);
        $this->db->or_like('updated', $search);
        $this->db->or_like('status', $search);
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('site_settings');
        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>