<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        Comment_model
 * Creation date:  27.05.2014
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Comment_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $article_id;   // DataType: int(11)
    private $user_id;   // DataType: int(11)
    private $textcomment;   // DataType: varchar(255)
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_article_id() {
        return $this->article_id;
    }

    function get_user_id() {
        return $this->user_id;
    }

    function get_textcomment() {
        return $this->textcomment;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_article_id($value) {
        $this->article_id = $value;
    }

    function set_user_id($value) {
        $this->user_id = $value;
    }

    function set_textcomment($value) {
        $this->textcomment = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->article_id = $row->article_id;
        $this->user_id = $row->user_id;
        $this->textcomment = $row->textcomment;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all comment
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('comment');
        return $query->result();
    }

    // **********************
    // Get comment by id
    // **********************

    function get_comment($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('comment');

        foreach ($query->result() as $comment) {
            $this->init($comment);
            return $comment;
        }
    }

    // **********************
    // Delete comment
    // **********************

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete('comment');
        return $this->db->affected_rows();
    }

    // **********************
    // Insert comment
    // **********************

    function insert() {

        if (isset($this->article_id) && !empty($this->article_id))
            $data['article_id'] = $this->article_id;
        if (isset($this->user_id) && !empty($this->user_id))
            $data['user_id'] = $this->user_id;
        if (isset($this->textcomment) && !empty($this->textcomment))
            $data['textcomment'] = $this->textcomment;
        if (isset($this->status) && !empty($this->status))
            $data['status'] = $this->status;

        $this->db->insert('comment', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update comment
    // **********************

    function update() {

        if (isset($this->article_id) && !empty($this->article_id))
            $data['article_id'] = $this->article_id;
        if (isset($this->user_id) && !empty($this->user_id))
            $data['user_id'] = $this->user_id;
        if (isset($this->textcomment) && !empty($this->textcomment))
            $data['textcomment'] = $this->textcomment;
        if (isset($this->status) && !empty($this->status))
            $data['status'] = $this->status;

        $this->db->where('id', $this->id);
        $this->db->update('comment', $data);
        return $this->db->affected_rows();
    }

    // **********************
    // Count records
    // **********************

    function count($criteria = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            $this->db->where($criteria);
        }
        $this->db->from('comment');
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>