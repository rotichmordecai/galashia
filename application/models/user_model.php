<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        User_model
 * Creation date:  24.05.2014
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class User_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $username;   // DataType: varchar(50)
    private $password;   // DataType: varchar(255)
    private $email;   // DataType: varchar(100)
    private $activated;   // DataType: tinyint(1)
    private $banned;   // DataType: tinyint(1)
    private $ban_reason;   // DataType: varchar(255)
    private $new_password_key;   // DataType: varchar(50)
    private $new_password_requested;   // DataType: datetime
    private $new_email;   // DataType: varchar(100)
    private $new_email_key;   // DataType: varchar(50)
    private $last_ip;   // DataType: varchar(40)
    private $last_login;   // DataType: datetime
    private $created;   // DataType: datetime
    private $modified;   // DataType: timestamp
    private $is_admin;   // DataType: tinyint(1)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_username() {
        return $this->username;
    }

    function get_password() {
        return $this->password;
    }

    function get_email() {
        return $this->email;
    }

    function get_activated() {
        return $this->activated;
    }

    function get_banned() {
        return $this->banned;
    }

    function get_ban_reason() {
        return $this->ban_reason;
    }

    function get_new_password_key() {
        return $this->new_password_key;
    }

    function get_new_password_requested() {
        return $this->new_password_requested;
    }

    function get_new_email() {
        return $this->new_email;
    }

    function get_new_email_key() {
        return $this->new_email_key;
    }

    function get_last_ip() {
        return $this->last_ip;
    }

    function get_last_login() {
        return $this->last_login;
    }

    function get_created() {
        return $this->created;
    }

    function get_modified() {
        return $this->modified;
    }

    function get_is_admin() {
        return $this->is_admin;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_username($value) {
        $this->username = $value;
    }

    function set_password($value) {
        $this->password = $value;
    }

    function set_email($value) {
        $this->email = $value;
    }

    function set_activated($value) {
        $this->activated = $value;
    }

    function set_banned($value) {
        $this->banned = $value;
    }

    function set_ban_reason($value) {
        $this->ban_reason = $value;
    }

    function set_new_password_key($value) {
        $this->new_password_key = $value;
    }

    function set_new_password_requested($value) {
        $this->new_password_requested = $value;
    }

    function set_new_email($value) {
        $this->new_email = $value;
    }

    function set_new_email_key($value) {
        $this->new_email_key = $value;
    }

    function set_last_ip($value) {
        $this->last_ip = $value;
    }

    function set_last_login($value) {
        $this->last_login = $value;
    }

    function set_created($value) {
        $this->created = $value;
    }

    function set_modified($value) {
        $this->modified = $value;
    }

    function set_is_admin($value) {
        $this->is_admin = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->username = $row->username;
        $this->password = $row->password;
        $this->email = $row->email;
        $this->activated = $row->activated;
        $this->banned = $row->banned;
        $this->ban_reason = $row->ban_reason;
        $this->new_password_key = $row->new_password_key;
        $this->new_password_requested = $row->new_password_requested;
        $this->new_email = $row->new_email;
        $this->new_email_key = $row->new_email_key;
        $this->last_ip = $row->last_ip;
        $this->last_login = $row->last_login;
        $this->created = $row->created;
        $this->modified = $row->modified;
        $this->is_admin = $row->is_admin;
    }

    // **********************
    // Select / Get all user
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        $this->db->from('user');

        if (is_array($criteria)) {
            $this->db->where($criteria);
        }

        $this->db->join('user_profile', 'user_profile.user_id = user.id');
        $query = $this->db->get();

        return $query->result();
    }

    // **********************
    // Get user by id
    // **********************

    function get_user($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('user');

        foreach ($query->result() as $user) {
            $this->init($user);
            return $user;
        }
    }

    // **********************
    // Delete user
    // **********************

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete('user');
        return $this->db->affected_rows();
    }

    // **********************
    // Insert user
    // **********************

    function insert() {

        if (isset($this->username) && !empty($this->username))
            $data['username'] = $this->username;
        if (isset($this->password) && !empty($this->password))
            $data['password'] = $this->password;
        if (isset($this->email) && !empty($this->email))
            $data['email'] = $this->email;
        if (isset($this->activated) && !empty($this->activated))
            $data['activated'] = $this->activated;
        if (isset($this->banned) && !empty($this->banned))
            $data['banned'] = $this->banned;
        if (isset($this->ban_reason) && !empty($this->ban_reason))
            $data['ban_reason'] = $this->ban_reason;
        if (isset($this->new_password_key) && !empty($this->new_password_key))
            $data['new_password_key'] = $this->new_password_key;
        if (isset($this->new_password_requested) && !empty($this->new_password_requested))
            $data['new_password_requested'] = $this->new_password_requested;
        if (isset($this->new_email) && !empty($this->new_email))
            $data['new_email'] = $this->new_email;
        if (isset($this->new_email_key) && !empty($this->new_email_key))
            $data['new_email_key'] = $this->new_email_key;
        if (isset($this->last_ip) && !empty($this->last_ip))
            $data['last_ip'] = $this->last_ip;
        if (isset($this->last_login) && !empty($this->last_login))
            $data['last_login'] = $this->last_login;
        if (isset($this->created) && !empty($this->created))
            $data['created'] = $this->created;
        if (isset($this->modified) && !empty($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->is_admin) && !empty($this->is_admin))
            $data['is_admin'] = $this->is_admin;

        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update user
    // **********************

    function update() {

        $data = array();

        if (isset($this->username) && !empty($this->username))
            $data['username'] = $this->username;
        if (isset($this->password) && !empty($this->password))
            $data['password'] = $this->password;
        if (isset($this->email) && !empty($this->email))
            $data['email'] = $this->email;
        if (isset($this->activated))
            $data['activated'] = $this->activated;
        if (isset($this->banned))
            $data['banned'] = $this->banned;
        if (isset($this->ban_reason) && !empty($this->ban_reason))
            $data['ban_reason'] = $this->ban_reason;
        if (isset($this->new_password_key) && !empty($this->new_password_key))
            $data['new_password_key'] = $this->new_password_key;
        if (isset($this->new_password_requested) && !empty($this->new_password_requested))
            $data['new_password_requested'] = $this->new_password_requested;
        if (isset($this->new_email) && !empty($this->new_email))
            $data['new_email'] = $this->new_email;
        if (isset($this->new_email_key) && !empty($this->new_email_key))
            $data['new_email_key'] = $this->new_email_key;
        if (isset($this->last_ip) && !empty($this->last_ip))
            $data['last_ip'] = $this->last_ip;
        if (isset($this->last_login) && !empty($this->last_login))
            $data['last_login'] = $this->last_login;
        if (isset($this->created) && !empty($this->created))
            $data['created'] = $this->created;
        if (isset($this->modified) && !empty($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->is_admin))
            $data['is_admin'] = $this->is_admin;

        $this->db->where('id', $this->id);
        $this->db->update('user', $data);
        return $this->db->affected_rows();
    }

    // **********************
    // Fetch records
    //
    
    function fetch($limit, $start) {

        $this->db->limit($limit, $start);
        $query = $this->db->get("user");

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function count($criteria = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            $this->db->where($criteria);
        }
        $this->db->from('user');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // get user by their social media id
    function get_user_by_sm($data, $sm_id) {
        $this->db->select("u.*, up." . $sm_id);
        $this->db->from("user AS u");
        $this->db->join("user_profile AS up", "u.id=up.user_id");
        $this->db->where($data);
        $query = $this->db->get();
        return $query->result();
    }

    // Returns user by its email
    function get_user_by_email($email) {
        $query = $this->db->query("SELECT * FROM user u, user_profile up WHERE u.email='$email' and u.id = up.user_id");
        return $query->result();
    }

    function get_user_by_username($username) {
        $query = $this->db->query("SELECT * FROM user u, user_profile up WHERE u.username='$username' and u.id = up.user_id");
        return $query->result();
    }

    // a generic update method for user profile
    function update_user_profile($user_id, $data) {
        $this->db->where('user_id', $user_id);
        $this->db->update('user_profile', $data);
    }

    // return the user given the id
    function get_user_data($user_id) {
        $query = $this->db->query("SELECT user.*, user_profile.* FROM user, user_profile WHERE user.id='$user_id' AND user_profile.user_id='$user_id'");
        return $query->result();
    }

}

?>