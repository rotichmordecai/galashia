<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        project_image_model
 * Creation date:  11.01.2015
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Project_image_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $project_id;   // DataType: int(11)
    private $image;   // DataType: varchar(255)
    private $thumb;   // DataType: varchar(255)
    private $modified;   // DataType: timestamp
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_project_id() {
        return $this->project_id;
    }

    function get_image() {
        return $this->image;
    }

    function get_thumb() {
        return $this->thumb;
    }

    function get_modified() {
        return $this->modified;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_project_id($value) {
        $this->project_id = $value;
    }

    function set_image($value) {
        $this->image = $value;
    }

    function set_thumb($value) {
        $this->thumb = $value;
    }

    function set_modified($value) {
        $this->modified = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->project_id = $row->project_id;
        $this->image = $row->image;
        $this->thumb = $row->thumb;
        $this->modified = $row->modified;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all project_image
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('project_image');
        return $query->result();
    }

    // **********************
    // Get project_image by id
    // **********************

    function get_project_image($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('project_image');

        foreach ($query->result() as $project_image) {
            $this->init($project_image);
            return $project_image;
        }
    }

    // **********************
    // Delete project_image
    // **********************

    public function delete($criteria = null) {
        if ($criteria != null) {
            $this->db->where($criteria);
            $this->db->delete('project_image');
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('project_image');
            return $this->db->affected_rows();
        }
        return 0;
    }

    // **********************
    // Insert project_image
    // **********************

    function insert() {
        if (isset($this->project_id))
            $data['project_id'] = $this->project_id;
        if (isset($this->image))
            $data['image'] = $this->image;
        if (isset($this->thumb))
            $data['thumb'] = $this->thumb;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('project_image', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update project_image
    // **********************

    function update($criteria = null) {

        if (isset($this->id))
            $data['id'] = $this->id;
        if (isset($this->project_id))
            $data['project_id'] = $this->project_id;
        if (isset($this->image))
            $data['image'] = $this->image;
        if (isset($this->thumb))
            $data['thumb'] = $this->thumb;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('project_image', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('project_image', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {
        $this->db->select('*');
        $this->db->from('project_image');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('project_image');
        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('project_image');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('project_id', $search);
        $this->db->or_like('image', $search);
        $this->db->or_like('thumb', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('project_image');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('project_id', $search);
        $this->db->or_like('image', $search);
        $this->db->or_like('thumb', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('project_image');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>