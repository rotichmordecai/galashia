<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        Client_model
 * Creation date:  30.05.2014
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Client_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $title;   // DataType: varchar(45)
    private $logo;   // DataType: varchar(255)
    private $description;   // DataType: text
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_title() {
        return $this->title;
    }

    function get_logo() {
        return $this->logo;
    }

    function get_description() {
        return $this->description;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_title($value) {
        $this->title = $value;
    }

    function set_logo($value) {
        $this->logo = $value;
    }

    function set_description($value) {
        $this->description = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->title = $row->title;
        $this->logo = $row->logo;
        $this->description = $row->description;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all client
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('client');
        return $query->result();
    }

    // **********************
    // Get client by id
    // **********************

    function get_client($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('client');

        foreach ($query->result() as $client) {
            $this->init($client);
            return $client;
        }
    }

    // **********************
    // Delete client
    // **********************

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete('client');
        return $this->db->affected_rows();
    }

    // **********************
    // Insert client
    // **********************

    function insert() {

        if (isset($this->title) && !empty($this->title))
            $data['title'] = $this->title;
        if (isset($this->logo) && !empty($this->logo))
            $data['logo'] = $this->logo;
        if (isset($this->description) && !empty($this->description))
            $data['description'] = $this->description;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('client', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update client
    // **********************

    function update() {

        if (isset($this->title) && !empty($this->title))
            $data['title'] = $this->title;
        if (isset($this->logo) && !empty($this->logo))
            $data['logo'] = $this->logo;
        if (isset($this->description) && !empty($this->description))
            $data['description'] = $this->description;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->where('id', $this->id);
        $this->db->update('client', $data);
        return $this->db->affected_rows();
    }

    // **********************
    // Fetch records
    //
    
    function fetch($criteria, $limit, $start) {

        if ($criteria != null && is_array($criteria)) {
            $this->db->where($criteria);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get("client");

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function count($criteria = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            $this->db->where($criteria);
        }
        $this->db->from('client');
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>