<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        message_model
 * Creation date:  09.01.2015
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Message_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $user_id1;   // DataType: int(11)
    private $user_id2;   // DataType: int(11)
    private $message_thread_id;   // DataType: int(11)
    private $message_title;   // DataType: varchar(75)
    private $message_text;   // DataType: varchar(255)
    private $modified;   // DataType: timestamp
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_user_id1() {
        return $this->user_id1;
    }

    function get_user_id2() {
        return $this->user_id2;
    }

    function get_message_thread_id() {
        return $this->message_thread_id;
    }

    function get_message_title() {
        return $this->message_title;
    }

    function get_message_text() {
        return $this->message_text;
    }

    function get_modified() {
        return $this->modified;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_user_id1($value) {
        $this->user_id1 = $value;
    }

    function set_user_id2($value) {
        $this->user_id2 = $value;
    }

    function set_message_thread_id($value) {
        $this->message_thread_id = $value;
    }

    function set_message_title($value) {
        $this->message_title = $value;
    }

    function set_message_text($value) {
        $this->message_text = $value;
    }

    function set_modified($value) {
        $this->modified = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {

        $this->id = $row->id;
        $this->user_id1 = $row->user_id1;
        $this->user_id2 = $row->user_id2;
        $this->message_thread_id = $row->message_thread_id;
        $this->message_title = $row->message_title;
        $this->message_text = $row->message_text;
        $this->modified = $row->modified;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all message
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('message');
        return $query->result();
    }

    // **********************
    // Get message by id
    // **********************

    function get_message($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('message');

        foreach ($query->result() as $message) {
            $this->init($message);
            return $message;
        }
    }

    // **********************
    // Delete message
    // **********************

    public function delete($criteria = null) {
        if ($criteria != null) {
            $this->db->delete('message', array($criteria));
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('message');
            return $this->db->affected_rows();
        }
        return 0;
    }

    // **********************
    // Insert message
    // **********************

    function insert() {

        if (isset($this->user_id1))
            $data['user_id1'] = $this->user_id1;
        if (isset($this->user_id2))
            $data['user_id2'] = $this->user_id2;
        if (isset($this->message_thread_id))
            $data['message_thread_id'] = $this->message_thread_id;
        if (isset($this->message_title))
            $data['message_title'] = $this->message_title;
        if (isset($this->message_text))
            $data['message_text'] = $this->message_text;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('message', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update message
    // **********************

    function update($criteria = null) {

        if (isset($this->id))
            $data['id'] = $this->id;
        if (isset($this->user_id1))
            $data['user_id1'] = $this->user_id1;
        if (isset($this->user_id2))
            $data['user_id2'] = $this->user_id2;
        if (isset($this->message_thread_id))
            $data['message_thread_id'] = $this->message_thread_id;
        if (isset($this->message_title))
            $data['message_title'] = $this->message_title;
        if (isset($this->message_text))
            $data['message_text'] = $this->message_text;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('message', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('message', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {
        $this->db->select('*');
        $this->db->from('message');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('message');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('message');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id1', $search);
        $this->db->or_like('user_id2', $search);
        $this->db->or_like('message_thread_id', $search);
        $this->db->or_like('message_title', $search);
        $this->db->or_like('message_text', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('message');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id1', $search);
        $this->db->or_like('user_id2', $search);
        $this->db->or_like('message_thread_id', $search);
        $this->db->or_like('message_title', $search);
        $this->db->or_like('message_text', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('message');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>