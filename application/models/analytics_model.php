<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        Analytics_model
 * Creation date:  28.05.2014
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Analytics_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $article_id;   // DataType: int(11)
    private $view;   // DataType: bigint(20)
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_article_id() {
        return $this->article_id;
    }

    function get_view() {
        return $this->view;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_article_id($value) {
        $this->article_id = $value;
    }

    function set_view($value) {
        $this->view = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->article_id = $row->article_id;
        $this->view = $row->view;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all analytics
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('analytics');
        return $query->result();
    }

    // **********************
    // Get analytics by id
    // **********************

    function get_analytics($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('analytics');

        foreach ($query->result() as $analytics) {
            $this->init($analytics);
            return $analytics;
        }
    }

    // **********************
    // Delete analytics
    // **********************

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete('analytics');
        return $this->db->affected_rows();
    }

    // **********************
    // Insert analytics
    // **********************

    function insert() {

        if (isset($this->article_id) && !empty($this->article_id))
            $data['article_id'] = $this->article_id;
        if (isset($this->view) && !empty($this->view))
            $data['view'] = $this->view;
        if (isset($this->status) && !empty($this->status))
            $data['status'] = $this->status;

        $this->db->insert('analytics', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update analytics
    // **********************

    function update() {

        if (isset($this->article_id) && !empty($this->article_id))
            $data['article_id'] = $this->article_id;
        if (isset($this->view) && !empty($this->view))
            $data['view'] = $this->view;
        if (isset($this->status) && !empty($this->status))
            $data['status'] = $this->status;

        $this->db->where('id', $this->id);
        $this->db->update('analytics', $data);
        return $this->db->affected_rows();
    }

    // **********************
    // Count records
    // **********************

    function count($criteria = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            $this->db->where($criteria);
        }
        $this->db->from('analytics');
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>