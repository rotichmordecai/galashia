<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        project_model
 * Creation date:  11.01.2015
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Project_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $user_id;   // DataType: int(11)
    private $title;   // DataType: varchar(75)
    private $description;   // DataType: varchar(1000)
    private $start_date;   // DataType: datetime
    private $end_date;   // DataType: datetime
    private $modified;   // DataType: timestamp
    private $status;   // DataType: tinyint(4)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_user_id() {
        return $this->user_id;
    }

    function get_title() {
        return $this->title;
    }

    function get_description() {
        return $this->description;
    }

    function get_start_date() {
        return $this->start_date;
    }

    function get_end_date() {
        return $this->end_date;
    }

    function get_modified() {
        return $this->modified;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_user_id($value) {
        $this->user_id = $value;
    }

    function set_title($value) {
        $this->title = $value;
    }

    function set_description($value) {
        $this->description = $value;
    }

    function set_start_date($value) {
        $this->start_date = $value;
    }

    function set_end_date($value) {
        $this->end_date = $value;
    }

    function set_modified($value) {
        $this->modified = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->user_id = $row->user_id;
        $this->title = $row->title;
        $this->description = $row->description;
        $this->start_date = $row->start_date;
        $this->end_date = $row->end_date;
        $this->modified = $row->modified;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all project
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('project');
        return $query->result();
    }

    // **********************
    // Get project by id
    // **********************

    function get_project($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('project');

        foreach ($query->result() as $project) {
            $this->init($project);
            return $project;
        }
    }

    // **********************
    // Delete project
    // **********************

    public function delete($criteria = null) {
        if ($criteria != null) {
            $this->db->delete('project', array($criteria));
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('project');
            return $this->db->affected_rows();
        }
        return 0;
    }

    // **********************
    // Insert project
    // **********************

    function insert() {
        if (isset($this->user_id))
            $data['user_id'] = $this->user_id;
        if (isset($this->title))
            $data['title'] = $this->title;
        if (isset($this->description))
            $data['description'] = $this->description;
        if (isset($this->start_date))
            $data['start_date'] = $this->start_date;
        if (isset($this->end_date))
            $data['end_date'] = $this->end_date;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('project', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update project
    // **********************

    function update($criteria = null) {
        if (isset($this->id))
            $data['id'] = $this->id;
        if (isset($this->user_id))
            $data['user_id'] = $this->user_id;
        if (isset($this->title))
            $data['title'] = $this->title;
        if (isset($this->description))
            $data['description'] = $this->description;
        if (isset($this->start_date))
            $data['start_date'] = $this->start_date;
        if (isset($this->end_date))
            $data['end_date'] = $this->end_date;
        if (isset($this->modified))
            $data['modified'] = $this->modified;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('project', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('project', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {
        $this->db->select('*');
        $this->db->from('project');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('project');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('project');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id', $search);
        $this->db->or_like('title', $search);
        $this->db->or_like('description', $search);
        $this->db->or_like('start_date', $search);
        $this->db->or_like('end_date', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('project');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id', $search);
        $this->db->or_like('title', $search);
        $this->db->or_like('description', $search);
        $this->db->or_like('start_date', $search);
        $this->db->or_like('end_date', $search);
        $this->db->or_like('modified', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('project');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>