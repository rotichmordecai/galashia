<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        user_profile_model
 * Creation date:  22.09.2014
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class user_profile_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $id;   // Key Attribute
    private $user_id;   // DataType: int(11)
    private $user_name;   // DataType: varchar(255)
    private $country;   // DataType: varchar(20)
    private $website;   // DataType: varchar(255)
    private $facebook_id;   // DataType: varchar(255)
    private $twitter_id;   // DataType: varchar(255)
    private $gfc_id;   // DataType: varchar(255)
    private $google_open_id;   // DataType: varchar(255)
    private $yahoo_open_id;   // DataType: varchar(255)
    private $user_bio;   // DataType: text
    private $facebook;   // DataType: varchar(255)
    private $twitter;   // DataType: varchar(255)
    private $googleplus;   // DataType: varchar(255)
    private $linkedin;   // DataType: varchar(255)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_user_id() {
        return $this->user_id;
    }

    function get_user_name() {
        return $this->user_name;
    }

    function get_country() {
        return $this->country;
    }

    function get_website() {
        return $this->website;
    }

    function get_facebook_id() {
        return $this->facebook_id;
    }

    function get_twitter_id() {
        return $this->twitter_id;
    }

    function get_gfc_id() {
        return $this->gfc_id;
    }

    function get_google_open_id() {
        return $this->google_open_id;
    }

    function get_yahoo_open_id() {
        return $this->yahoo_open_id;
    }

    function get_user_bio() {
        return $this->user_bio;
    }

    function get_facebook() {
        return $this->facebook;
    }

    function get_twitter() {
        return $this->twitter;
    }

    function get_googleplus() {
        return $this->googleplus;
    }

    function get_linkedin() {
        return $this->linkedin;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_user_id($value) {
        $this->user_id = $value;
    }

    function set_user_name($value) {
        $this->user_name = $value;
    }

    function set_country($value) {
        $this->country = $value;
    }

    function set_website($value) {
        $this->website = $value;
    }

    function set_facebook_id($value) {
        $this->facebook_id = $value;
    }

    function set_twitter_id($value) {
        $this->twitter_id = $value;
    }

    function set_gfc_id($value) {
        $this->gfc_id = $value;
    }

    function set_google_open_id($value) {
        $this->google_open_id = $value;
    }

    function set_yahoo_open_id($value) {
        $this->yahoo_open_id = $value;
    }

    function set_user_bio($value) {
        $this->user_bio = $value;
    }

    function set_facebook($value) {
        $this->facebook = $value;
    }

    function set_twitter($value) {
        $this->twitter = $value;
    }

    function set_googleplus($value) {
        $this->googleplus = $value;
    }

    function set_linkedin($value) {
        $this->linkedin = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {
        $this->id = $row->id;
        $this->user_id = $row->user_id;
        $this->user_name = $row->user_name;
        $this->country = $row->country;
        $this->website = $row->website;
        $this->facebook_id = $row->facebook_id;
        $this->twitter_id = $row->twitter_id;
        $this->gfc_id = $row->gfc_id;
        $this->google_open_id = $row->google_open_id;
        $this->yahoo_open_id = $row->yahoo_open_id;
        $this->user_bio = $row->user_bio;
        $this->facebook = $row->facebook;
        $this->twitter = $row->twitter;
        $this->googleplus = $row->googleplus;
        $this->linkedin = $row->linkedin;
    }

    // **********************
    // Select / Get all user_profile
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('user_profile');
        return $query->result();
    }

    // **********************
    // Get user_profile by id
    // **********************

    function get_user_profile($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('user_profile');

        foreach ($query->result() as $user_profile) {
            $this->init($user_profile);
            return $user_profile;
        }
    }

    // **********************
    // Delete user_profile
    // **********************

    public function delete($criteria = null) {
        if ($criteria != null) {
            $this->db->delete('user_profile', array($criteria));
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('user_profile');
            return $this->db->affected_rows();
        }
        return 0;
    }

    // **********************
    // Insert user_profile
    // **********************

    function insert() {
        if (isset($this->user_id))
            $data['user_id'] = $this->user_id;
        if (isset($this->user_name))
            $data['user_name'] = $this->user_name;
        if (isset($this->country))
            $data['country'] = $this->country;
        if (isset($this->website))
            $data['website'] = $this->website;
        if (isset($this->facebook_id))
            $data['facebook_id'] = $this->facebook_id;
        if (isset($this->twitter_id))
            $data['twitter_id'] = $this->twitter_id;
        if (isset($this->gfc_id))
            $data['gfc_id'] = $this->gfc_id;
        if (isset($this->google_open_id))
            $data['google_open_id'] = $this->google_open_id;
        if (isset($this->yahoo_open_id))
            $data['yahoo_open_id'] = $this->yahoo_open_id;
        if (isset($this->user_bio))
            $data['user_bio'] = $this->user_bio;
        if (isset($this->facebook))
            $data['facebook'] = $this->facebook;
        if (isset($this->twitter))
            $data['twitter'] = $this->twitter;
        if (isset($this->googleplus))
            $data['googleplus'] = $this->googleplus;
        if (isset($this->linkedin))
            $data['linkedin'] = $this->linkedin;

        $this->db->insert('user_profile', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Update user_profile
    // **********************

    function update($criteria = null) {

        if (isset($this->id))
            $data['id'] = $this->id;
        if (isset($this->user_id))
            $data['user_id'] = $this->user_id;
        if (isset($this->user_name))
            $data['user_name'] = $this->user_name;
        if (isset($this->country))
            $data['country'] = $this->country;
        if (isset($this->website))
            $data['website'] = $this->website;
        if (isset($this->facebook_id))
            $data['facebook_id'] = $this->facebook_id;
        if (isset($this->twitter_id))
            $data['twitter_id'] = $this->twitter_id;
        if (isset($this->gfc_id))
            $data['gfc_id'] = $this->gfc_id;
        if (isset($this->google_open_id))
            $data['google_open_id'] = $this->google_open_id;
        if (isset($this->yahoo_open_id))
            $data['yahoo_open_id'] = $this->yahoo_open_id;
        if (isset($this->user_bio))
            $data['user_bio'] = $this->user_bio;
        if (isset($this->facebook))
            $data['facebook'] = $this->facebook;
        if (isset($this->twitter))
            $data['twitter'] = $this->twitter;
        if (isset($this->googleplus))
            $data['googleplus'] = $this->googleplus;
        if (isset($this->linkedin))
            $data['linkedin'] = $this->linkedin;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('user_profile', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('user_profile', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {
        $this->db->select('*');
        $this->db->from('user_profile');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('user_profile');
        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {
        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('user_profile');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id', $search);
        $this->db->or_like('user_name', $search);
        $this->db->or_like('country', $search);
        $this->db->or_like('website', $search);
        $this->db->or_like('facebook_id', $search);
        $this->db->or_like('twitter_id', $search);
        $this->db->or_like('gfc_id', $search);
        $this->db->or_like('google_open_id', $search);
        $this->db->or_like('yahoo_open_id', $search);
        $this->db->or_like('user_bio', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('user_profile');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id', $search);
        $this->db->or_like('user_name', $search);
        $this->db->or_like('country', $search);
        $this->db->or_like('website', $search);
        $this->db->or_like('facebook_id', $search);
        $this->db->or_like('twitter_id', $search);
        $this->db->or_like('gfc_id', $search);
        $this->db->or_like('google_open_id', $search);
        $this->db->or_like('yahoo_open_id', $search);
        $this->db->or_like('user_bio', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('user_profile');
        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>