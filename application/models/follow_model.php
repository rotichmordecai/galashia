<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        follow_model
 * Creation date:  09.01.2015
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************

class Follow_model extends CI_Model {

    // **********************
    // Attribute Declaration
    // **********************

    private $user_id_1;   // DataType: int(11)
    private $user_id_2;   // DataType: int(11)
    private $updated;   // DataType: timestamp
    private $status;   // DataType: int(11)

    // **********************
    // Constructor Method
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter Methods
    // **********************

    function get_user_id_1() {
        return $this->user_id_1;
    }

    function get_user_id_2() {
        return $this->user_id_2;
    }

    function get_updated() {
        return $this->updated;
    }

    function get_status() {
        return $this->status;
    }

    // **********************
    // Setter Methods
    // **********************

    function set_user_id_1($value) {
        $this->user_id_1 = $value;
    }

    function set_user_id_2($value) {
        $this->user_id_2 = $value;
    }

    function set_updated($value) {
        $this->updated = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    // **********************
    // Init Method
    // **********************

    function init($row) {

        $this->user_id_1 = $row->user_id_1;
        $this->user_id_2 = $row->user_id_2;
        $this->updated = $row->updated;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all follow
    // **********************

    function select($criteria = null) {

        $this->db->select('*');
        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        $query = $this->db->get('follow');
        return $query->result();
    }

    // **********************
    // Get follow by 
    // **********************

    function get_follow($id) {

        $this->db->select('*');
        $this->db->where('', $id);
        $query = $this->db->get('follow');

        foreach ($query->result() as $follow) {
            $this->init($follow);
            return $follow;
        }
    }

    // **********************
    // Delete follow
    // **********************

    public function delete($criteria = null) {

        if ($criteria != null) {
            $this->db->where($criteria);
            $this->db->delete('follow');
            return $this->db->affected_rows();
        }
        return 0;
    }

    // **********************
    // Insert follow
    // **********************

    function insert() {

        if (isset($this->user_id_1))
            $data['user_id_1'] = $this->user_id_1;
        if (isset($this->user_id_2))
            $data['user_id_2'] = $this->user_id_2;
        if (isset($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('follow', $data);
        return $this->db->affected_rows();
    }

    // **********************
    // Update follow
    // **********************

    function update($criteria = null) {

        if (isset($this->user_id_1))
            $data['user_id_1'] = $this->user_id_1;
        if (isset($this->user_id_2))
            $data['user_id_2'] = $this->user_id_2;
        if (isset($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('follow', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Count records
    // **********************

    function count() {

        $this->db->select('*');
        $this->db->from('follow');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('follow');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $this->db->limit($limit, $start);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    // **********************
    // Count records
    // **********************

    function fetch_count($criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('follow');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Search records
    // **********************

    function search($search, $limit, $start, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id_1', $search);
        $this->db->or_like('user_id_2', $search);
        $this->db->or_like('updated', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('follow');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function search_count($search, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        $this->db->like('user_id_1', $search);
        $this->db->or_like('user_id_2', $search);
        $this->db->or_like('updated', $search);
        $this->db->or_like('status', $search);

        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('follow');

        if ($group_by != NULL) {
            $this->db->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>