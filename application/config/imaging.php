<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['image_path'] = './uploads/';
$config['thumb_path'] = './uploads/thumb/';

$config['project_image_path'] = './uploads/project/';
$config['project_thumb_path'] = './uploads/project/thumb/';

$config['user_image_path'] = './uploads/user/';
$config['user_thumb_path'] = './uploads/user/thumb/';

$config['thumb_size'] = 200;


