<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['facebook']['api_id'] = '606648989440077';
$config['facebook']['app_secret'] = 'cf91f541fbbddce9fab018cb9b341488';

$config['facebook']['redirect_url'] = 'http://www.galashia.com/auth_other/facebook_signin';

if (strpos($_SERVER['HTTP_HOST'], 'local') !== false) {
    $config['facebook']['redirect_url'] = 'http://local.galashia.com/auth_other/facebook_signin';
}

$config['facebook']['permissions'] = array(
    'email',
    'user_location',
    'user_birthday',
    'user_likes',
    'public_profile',
    'user_friends',
    'user_about_me',
    'manage_pages',
    'publish_actions'
);
