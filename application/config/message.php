<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['message_prefix'] = '<p>';
$config['message_suffix'] = '</p>';
$config['message_folder'] = 'message/';
$config['message_view']   = 'message'; // without the _view suffix
$config['wrapper_prefix'] = '<div class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button>';
$config['wrapper_suffix'] = '</div>';
