<?php

error_reporting(E_ALL);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author rotichmordecai
 */
class MY_Controller extends CI_Controller {

    /**
     * TwitterOauth class instance.
     */
    protected $connection;

    function MY_Controller() {
        parent::__construct();

        if ($this->session->userdata('access_token') && $this->session->userdata('access_token_secret')) {
            // If user already logged in
            $this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('access_token'), $this->session->userdata('access_token_secret'));
        } elseif ($this->session->userdata('request_token') && $this->session->userdata('request_token_secret')) {
            // If user in process of authentication
            $this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
        } else {
            // Unknown user
            $this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
        }
    }

    function front_assets() {

        $this->template->add_css('assets/front/assets/css/bootstrap.min.css');
        $this->template->add_css('assets/front/assets/css/font-awesome.css');
        $this->template->add_css('assets/front/assets/css/flexslider.css');
        $this->template->add_css('assets/front/assets/css/prettyPhoto.css');
        $this->template->add_css('assets/front/assets/rs-plugin/css/settings.css');
        $this->template->add_css('assets/front/assets/css/dropzone.css');
        $this->template->add_css('assets/front/assets/css/main.css');

        $this->template->add_js('assets/front/assets/js/modernizr-2.6.2-respond-1.1.0.min.js');
        $this->template->add_js('assets/front/assets/js/jquery-1.10.1.min.js');
        $this->template->add_js('assets/front/assets/js/angular.min.js');
        $this->template->add_js('assets/front/assets/js/bootstrap.min.js');
        $this->template->add_js('assets/front/assets/js/jquery.flexslider-min.js');
        $this->template->add_js('assets/front/assets/js/isotope.js');
        $this->template->add_js('assets/front/assets/js/jquery.carouFredSel-6.2.1-packed.js');
        $this->template->add_js('assets/front/assets/js/tweetable.jquery.min.js');
        $this->template->add_js('assets/front/assets/js/jquery.mobilemenu.js');
        $this->template->add_js('assets/front/assets/js/jquery.prettyPhoto.js');
        $this->template->add_js('assets/front/assets/js/jquery.cookie.js');
        $this->template->add_js('assets/front/assets/js/jquery.resizecrop-1.0.3.js');
        $this->template->add_js('assets/front/assets/rs-plugin/js/jquery.themepunch.plugins.min.js');
        $this->template->add_js('assets/front/assets/rs-plugin/js/jquery.themepunch.revolution.min.js');
        $this->template->add_js('assets/front/assets/js/bootstrap-rating-input.js');
        $this->template->add_js('assets/front/assets/js/jquery.form-validator.js');
        $this->template->add_js('assets/front/assets/js/bootbox.js');
        $this->template->add_js('assets/front/assets/js/dropzone.min.js');
        $this->template->add_js('assets/front/assets/js/main.js');
    }

    /**
     * Reset session data
     * @access	private
     * @return	void
     */
    protected function reset_session() {
        $this->session->unset_userdata('access_token');
        $this->session->unset_userdata('access_token_secret');
        $this->session->unset_userdata('request_token');
        $this->session->unset_userdata('request_token_secret');
        $this->session->unset_userdata('twitter_user_id');
        $this->session->unset_userdata('twitter_screen_name');
    }

}

class Administrator extends MY_Controller {

    function MY_Controller() {
        parent::__construct();
    }

    function initialize() {
        
    }

}
